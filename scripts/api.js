// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

//Se carga el codigo de la api
function cargar_codigo()
{
    id_tipo = $("#cb_tipo").val();
    $(".codigo").hide();
    $("#ejemplo_" + id_tipo).show();    
}

//Se realiza la ejecucion de la api
function ejecutar_api(_base_url)
{
    validar = true;
    metodo = '';
    
    if($.trim($("#usuario").val()) == "")
    {
        alert("Debe completar el campo Usuario");
        validar = false;
    }
    
    if((validar == true) && ($.trim($("#usuario").val()) == ""))
    {
        alert("Debe completar el campo Token");
        validar = false;
    }
    
    if((validar == true) && (($("#cb_tipo").val() == "1") || ($("#cb_tipo").val() == "2") || ($("#cb_tipo").val() == "3")) && ($.trim($("#id_registro").val()) == ""))
    {
        alert("Debe completar el campo Id Registro");
        validar = false;
    }
    
    if ($("#cb_tipo").val() == "1") {
        metodo = 'cultores/datos/' + $("#id_registro").val();
    }
    else if($("#cb_tipo").val() == "2")
    {
        metodo = 'comunas/datos/' + $("#id_registro").val();   
    }
    else if($("#cb_tipo").val() == "3")
    {
        metodo = 'dominio_especifico/datos/' + $("#id_registro").val();   
    }
    else if($("#cb_tipo").val() == "4")
    {
        metodo = 'perfil_dominio/datos/' + $("#id_registro").val();   
    }
    else if($("#cb_tipo").val() == "5")
    {
        metodo = 'categoria_unesco/datos/' + $("#id_registro").val();   
    }
    
    if ($("#cb_tipo").val() == "6") {
        metodo = 'cultores/datos_cultor_colectivo/' + $("#id_registro").val();
    }

    if (validar)
    {
        /*
        url = _base_url + "api/" + metodo;
        $.extend(
        {
            redirectPost: function(location, args)
            {
                var form = '';
                $.each( args, function( key, value ) {
                        //value = value.split('"').join('\"');
                        form += '<input type="hidden" name="'+key+'" value="'+value+'">';
                });
                $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
            }
        });

        $.redirectPost(url, {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value});
             */
        html = "<h3>Resultado</h3>";
        $.ajax({
		type: "POST",
		url: _base_url + "api/" + metodo,                
		data: {TOKEN_LOGIN: Base64.encode($("#usuario").val()),
                       TOKEN: Base64.encode($("#token").val())
		       },
		
		success: function (response)
                {
                    respuesta = JSON.stringify(response);

                    html += "<pre>"  + respuesta + "</pre>";
		    $("#dv_resultado").html(html);				    
		},
		error: function (xhr, ajaxOptions, thrownError) {
                    html += "Error obteniendo los datos";
		    $("#dv_resultado").html(html);
		} 
	    });
    }
}

//Se realiza la validacion del login
function validar_login()
{
    resultado = true;
    $("#dv_error").html("");
    $("#dv_error").hide();
    $('#username').addClass('js-username-field email-input').removeClass('signup_error');
    $('#password').addClass('js-password-field').removeClass('signup_error');
    
    if ($.trim($('#username').val()) == "")
    {
        $('#username').removeClass().addClass('signup_error');
        $('#username').attr('title', 'Correo electrónico es obligatorio.');            
        $('#username').tooltip();
        resultado = false;
    }

    if ($.trim($('#password').val()) == "")
    {
        $('#password').removeClass().addClass('signup_error');
        $('#password').attr('title', 'Debe ingresar la contraseña.'); 
        $('#password').tooltip();
        resultado = false;
    }
    
    if (resultado){        
        $.ajax({
            type: "POST",
            url: "/api/login/",
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    username: $('#username').val(),
                    password: $('#password').val()
                    },
            success : function(respuesta){
                obj = JSON.parse(respuesta);
               
                if (obj.status == "ok")
		{
                    parent.window.location = "/usuario:" + obj.usuario.slug + ".html";
                }
                else{                
                    $("#dv_error").html(obj.message);
		    $("#dv_error").show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status  + " " + xhr.responseText);
                alert(thrownError);
                }
            });
    }    
}

//Se realiza la validacion del formulario de registro
function validar_registro()
{
    resultado = true;
    $("#dv_error").html("");
    $("#dv_error").hide();
    $('#registrese').attr('style', '');
    $('#nombres').attr('style', '');
    $('#apellidos').attr('style', '');
    $('#email').attr('style', '');
    $('#pass1').attr('style', '');
    $('#pass2').attr('style', '');
    //$('#dv_avatar').attr('style', '');
    $('#chk_acepto').attr('style', '');
    
    if ($.trim($('#nombres').val()) == "")
    {
        $('#nombres').attr('style', 'border: 1px solid red;');
        $('#nombres').attr('title', 'Debe ingresar sus Nombres.');            
        $('#nombres').tooltip();
        resultado = false;
    }

    if ($.trim($('#apellidos').val()) == "")
    {
        $('#apellidos').attr('style', 'border: 1px solid red;');
        $('#apellidos').attr('title', 'Debe ingresar sus Apellidos.'); 
        $('#apellidos').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#email').val()) == "") || (validar_email($.trim($('#email').val())) == false))
    {
        $('#email').attr('style', 'border: 1px solid red;');
        $('#email').attr('title', 'Debe ingresar su Correo electrónico, revise el formato.'); 
        $('#email').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass1').val()) == "")
    {
        $('#pass1').attr('style', 'border: 1px solid red;');
        $('#pass1').attr('title', 'Debe ingresar su contraseña.'); 
        $('#pass1').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass2').val()) == "")
    {
        $('#pass2').attr('style', 'border: 1px solid red;');
        $('#pass2').attr('title', 'Debe re-ingresar la contraseña.'); 
        $('#pass2').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass1').val()) != $.trim($('#pass2').val()))
    {
        $('#pass1, #pass2').attr('style', 'border: 1px solid red;');
        $('#pass1, #pass2').attr('title', 'Las contraseñas no coinciden.'); 
        $('#pass1, #pass2').tooltip();
        resultado = false;
    }
    
    if (!$('#acepto').is(':checked'))
    {
        $('#chk_acepto').attr('style', 'border: 1px solid red;');
        $('#chk_acepto').attr('title', 'Debe Aceptar las condiciones.'); 
        $('#chk_acepto').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $.ajax({
            type: "POST",
            url: "/api/registro/",
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    nombres : $('#nombres').val(),
		    apellidos: $('#apellidos').val(),
		    email : $('#email').val(),
		    pass1: $('#pass1').val(),
		    pass2: $('#pass2').val(),
		    organizacion: $('#organizacion').val()		    
                    },
            success : function(respuesta){
                obj = JSON.parse(respuesta);
               
                if (obj.status == "ok")
		{
                    parent.window.location = "/usuario:" + obj.usuario.slug + ".html";
                }
                else{                
                    $("#dv_error").html(obj.message);
		    if (obj.campo != ''){
			$('#' + obj.campo).attr('style', 'border: 1px solid red;');
			if (obj.campo == 'nombres')
			{
			    $('#' +  obj.campo).attr('title', 'Nombre ya se encuentra registrado, intente con otro');
			    $('#' +  obj.campo).tooltip();		    
			}
			else if (obj.campo == 'email'){
			    $('#' +  obj.campo).attr('title', 'Correo electrónico ya se encuentra registrado, intente con otro.');
			    $('#' +  obj.campo).tooltip();		    
			}			
		    }
		
		    $("#dv_error").show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
		    $("#dv_error").html("Error realizando el registro, intente nuevamente.");
		    $("#dv_error").show();
                }
            });
    }   
}

//Se abre la ventana para recuperar contraseña
function olvide_contrasena(_base_url) {
 $.fancybox({
        'href' : _base_url + "sigpa/olvide-password",
         'width' : '70%',
         'height' : '50%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }          
     });   
}
