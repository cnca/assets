//Se inicializan los clic en "VER COMO LISTADO" o "VER COMO GALERIA"
function iniciar_listado() {
	//section.ci
	$('.ci .option span').click(function(){

		if($(".ci .item").hasClass('list')){
			$(".ci .thv").removeClass("small");    
			$(".ci .item").removeClass('list');
			$(this).html('ver como listado');
			
		}else{  
			$(".ci .item").addClass('list');
			$(".ci .thv").addClass("small"); 
			$(this).html('ver como galería');
		}
	});

	//section.cc
	$('.cc .option span').click(function(){

		if($(".cc .item").hasClass('list')){
			$(".cc .thv").removeClass("small");  
			$(".cc .item").removeClass('list');
			$(this).html('ver como listado');  
		}else{  
			$(".cc .thv").addClass("small"); 
			$(".cc .item").addClass('list');
			$(this).html('ver como galería');
		}
	});
	//section.fp
	$('.fp .option span').click(function(){

		if($(".fp .item").hasClass('list')){  
			$(".fp .item").removeClass('list');
			$(this).html('ver como listado');  
		}else{  
			$(".fp .item").addClass('list');
			$(this).html('ver como galería');
		}

	});

	//section.fp
	$('.lg .option span').click(function(){

		if($(".lg .item").hasClass('list')){  
			$(".lg .item").removeClass('list');
			$(this).html('ver como listado');  

		}else{  

			$(".lg .item").addClass('list');
			$(this).html('ver como galería');

		}

	});
}
// Can also be used with $(document).ready()
$(window).load(function() {
	$('.flexslider').flexslider({
		animation: "easing"
	});
});

$(document).ready(function() {	
	iniciar_listado();	
});

//Se realiza el cambio de pagina en la Paginacion
function cambiar_pagina_(url, pagina_actual, cantidad_registros)
{
    $("#pagina_actual").val(pagina_actual);
    $("#cantidad_registros").val(cantidad_registros);

    $.extend(
    {
      	redirectPost: function(location, args)
      	{
      	    var form = '';
      	    $.each( args, function( key, value ) {
      		    //value = value.split('"').join('\"');
      		    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
      	    });
      	    $('<form action="' + location + '" method="POST">{% csrf_token %}' + form + '</form>').appendTo($(document.body)).submit();
      	}
    });

    $.redirectPost(url, {pagina_actual: pagina_actual, cantidad_registros: cantidad_registros});

}

function mostrarMenu(nro_menu){
	$("#categoria_"+nro_menu).show();
}

function ocultarMenu(nro_menu){
	$("#categoria_"+nro_menu).hide();
}

function cambiar_encabezado(token, id_ficha, tipo_registro){

    $.ajax({
		type: "POST",
		url: "/sigpa/widget/cambiar-encabezado/",
		async: true,
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       token: token,
		       id_ficha: id_ficha,
		       tipo_registro: tipo_registro,   
		       },
		
		success: function (response) {
		    if (response != "ERROR"){
		    	$("#dv_encabezado").html(response);		
		    }
		    else{
				$("#dv_encabezado").html("");					    
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	 
		    $("#dv_encabezado").html("");
		} 
    });
}

