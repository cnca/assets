//Se realiza la validacion del paso 1
function validar_paso_1()
{
    resultado = true;
    
    $('#nombre').removeClass('requires');
    $('#fecha_inicio').removeClass('requires');
    $('#fecha_termino').removeClass('requires');
                
    if ($.trim($('#nombre').val()) == "")
    {
        $('#nombre').addClass('requires');
        $('#nombre').attr('title', 'Debe ingresar el Nombre');
        $('#nombre').tooltip();
        resultado = false;
    }   
    
    if ($.trim($('#fecha_inicio').val()) == "")
    {
        $('#fecha_inicio').addClass('requires');
        $('#fecha_inicio').attr('title', 'Debe ingresar la Fecha de Inicio');
        $('#fecha_inicio').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#fecha_termino').val()) == "")
    {
        $('#fecha_termino').addClass('requires');
        $('#fecha_termino').attr('title', 'Debe ingresar la Fecha de Termino');
        $('#fecha_termino').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $.ajax({
            type: "POST",
            url: "/sigpa/mi-cuenta/registro/validar-ficha-existe/",
            async: false,
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    tipo_registro : 'FR',
                    operacion : $('#operacion').val(),
                    id_registro : $('#id_registro').val(),
                    nombre: $('#nombre').val(),
                    },
            success : function(respuesta){   
                if (respuesta == 'OK') {
                    resultado = true;
                }
                else
                {
                    if (respuesta == 'EXISTE')
                    {                                        
                        $('#nombre').addClass('requires');
                        $('#nombre').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#nombre').tooltip();                        
                    }                    
                    else
                    {
                        $('#nombre').addClass('requires');
                        $('#nombre').attr('title', 'Revise los datos, existen problemas en la ficha');
                        $('#nombre').tooltip();                        
                    }
                    
                    resultado = false;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {                
                    resultado = false;
                }
            });
        
        return resultado;
    }
    else
    {
        return resultado;
    }    
}

//Se realiza la validacion del paso 2
function validar_paso_2()
{
    resultado = true;
    $('#antecedentes').removeClass('requires');
        
    if ($.trim($('#antecedentes').val()) == "")
    {
        $('#antecedentes').addClass('requires');
        $('#antecedentes').attr('title', 'Debe ingresar los Antecedentes de la Fiesta');
        $('#antecedentes').tooltip();
        resultado = false;
    }
    
    return resultado;    
}

//Se realiza la validacion del paso 3
function validar_paso_3()
{
    resultado = true;
    /*$('#btn_ambitos_culturales').attr('style', style_exito);  
    $('#btn_rituales').attr('style', style_exito);  
    $('#btn_agentes_unipersonales').attr('style', style_exito);  
    $('#btn_agentes_colectivos').attr('style', style_exito);  
    $('#btn_agentes_institucionales').attr('style', style_exito);  
    
    if (($.trim($('#comuna_nacimiento').val()) == "") || ($('#comuna_nacimiento').val() == "-1"))
    {
        $('#comuna_nacimiento').addClass('requires');
        $('#comuna_nacimiento').attr('title', 'Debe seleccionar la comuna');
        $('#comuna_nacimiento').tooltip();
        resultado = false;
    }

    if ($.trim($('#antecedentes').val()) == "")
    {
        $('#antecedentes').addClass('requires');
        $('#antecedentes').attr('title', 'Debe ingresar la Descripción de Cultor');
        $('#antecedentes').tooltip();
        resultado = false;
    }
        

    if (($.trim($('#especialidades').val()) == "") || ($('#especialidades').val() == "-1"))
    {
        $('#especialidades').addClass('requires');
        $('#especialidades').attr('title', 'Debe seleccionar los Perfiles de Dominio');
        $('#especialidades').tooltip();
        resultado = false;
    }
    */
    action = $("#frm_fiesta").attr("action");
    if ((resultado == true) && (action.indexOf('agregar') > 0))
    {
        mapa_inicio("map_canvas");
    }
    
    return resultado;    
}

//Se realiza la validacion del paso 4
function validar_paso_4()
{
    resultado = true;
    $('#btn_localizacion-comuna').attr('style', style_exito);    
    $('#localizacion-ciudad').removeClass('requires');
    $('#localizacion-calle_avenida').removeClass('requires');
    $('#localizacion-numero').removeClass('requires');
    $('#localizacion-otra_referencia').removeClass('requires');
    
    comuna = $("#localizacion-comuna option:selected").text();
    if (comuna == "Comuna")
    {
        $('#btn_localizacion-comuna').attr('style', style_error);
        $('#btn_localizacion-comuna').attr('title', 'Debe ingresar la comuna');
        $('#btn_localizacion-comuna').tooltip();
        resultado = false;
    }

    if ($.trim($('#localizacion-ciudad').val()) == "")
    {
        $('#localizacion-ciudad').addClass('requires');
        $('#localizacion-ciudad').attr('title', 'Debe ingresar la ciudad');
        $('#localizacion-ciudad').tooltip();
        resultado = false;
    }
        

    if ($.trim($('#localizacion-calle_avenida').val()) == "")
    {
        $('#localizacion-calle_avenida').addClass('requires');
        $('#localizacion-calle_avenida').attr('title', 'Debe ingresar la Calle');
        $('#localizacion-calle_avenida').tooltip();
        resultado = false;
    }
                
    return resultado;    
}


//Se realiza la validacion del paso 4
function validar_paso_5()
{
    resultado = true;
    $('#contacto-nombres').removeClass('requires');
    $('#contacto-apellido_paterno').removeClass('requires');
    $('#contacto-apellido_materno').removeClass('requires');
    $('#contacto-email').removeClass('requires');
    $('#contacto-sitio_web').removeClass('requires');

    if ($.trim($('#contacto-nombres').val()) == "")
    {
        $('#contacto-nombres').addClass('requires');
        $('#contacto-nombres').attr('title', 'Debe ingresar el Nombre del Contacto');
        $('#contacto-nombres').tooltip();
        resultado = false;
    }
        
    if ($.trim($('#contacto-apellido_paterno').val()) == "")
    {
        $('#contacto-apellido_paterno').addClass('requires');
        $('#contacto-apellido_paterno').attr('title', 'Debe ingresar el Apellido Paterno');
        $('#contacto-apellido_paterno').tooltip();
        resultado = false;
    }

        if ($.trim($('#contacto-apellido_materno').val()) == "")
    {
        $('#contacto-apellido_materno').addClass('requires');
        $('#contacto-apellido_materno').attr('title', 'Debe ingresar el Apellido Materno');
        $('#contacto-apellido_materno').tooltip();
        resultado = false;
    }

    if (($.trim($('#contacto-email').val()) == ""))
    {
        $('#contacto-email').addClass('requires');
        $('#contacto-email').attr('title', 'Debe ingresar el Correo Eletrónico del Contacto');
        $('#contacto-email').tooltip();
        resultado = false;
    }    
    if (($.trim($('#contacto-email').val()) != "") && (validar_email($('#contacto-email').val()) == false))
    {
        $('#contacto-email').addClass('requires');
        $('#contacto-email').attr('title', 'Debe ingresar un Correo Eletrónico válido para el Contacto');
        $('#contacto-email').tooltip();
        resultado = false;
    }

    if (($.trim($('#contacto-sitio_web').val()) != "") && (validar_url($('#contacto-sitio_web').val()) == false))
    {
        $('#contacto-sitio_web').addClass('requires');
        $('#contacto-sitio_web').attr('title', 'Debe ingresar una url válida. Ej: http://www.google.cl');
        $('#contacto-sitio_web').tooltip();
        resultado = false;
    }
    
    return resultado;        
}

//Se realiza la validacion del paso 6
function validar_paso_6()
{
    resultado = true;
    
    
    return resultado;    
}

//Se realiza la validacion del formulario de registro
function validar_formulario(paso_actual, operacion)
{
    
    //return true;
    
    if (paso_actual == 1)
    {        
        return validar_paso_1();
    }
    else if (paso_actual == 2)
    {        
        return validar_paso_2();    
    }
    else if (paso_actual == 3)
    {        
        return validar_paso_3();    
    }
    else if (paso_actual == 4)
    {        
        return validar_paso_4();    
    }
    else if (paso_actual == 5)
    {        
        return validar_paso_5();    
    }
    else if (paso_actual == 6)
    {        
        if(operacion == "add" && validar_paso_6() == true)
        {
            $('form#frm_fiesta').submit();
        }
        else
        {
            $('form#frm_fiesta').submit();
        }
    }
}