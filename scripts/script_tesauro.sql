
CREATE FULLTEXT INDEX idx_categoria_unesco ON sigpa_categoria_unesco(nombre, descripcion);

CREATE FULLTEXT INDEX idx_perfil_dominio ON sigpa_conceptos_perfildominio(nombre, descripcion, antecedentes);

CREATE FULLTEXT INDEX idx_dominio_especifico ON sigpa_conceptos_especialidad(nombre, descripcion, antecedentes);

CREATE FULLTEXT INDEX idx_cultor_individual ON sigpa_cultores_cultorindividual(nombres, apellido_paterno, apellido_materno, antecedentes, nombre_contacto); 

CREATE FULLTEXT INDEX idx_cultor_colectivo ON sigpa_cultores_cultorcolectivo(antecedentes, nombre, lugar_fundacion, nombre_contacto); 

CREATE FULLTEXT INDEX idx_fiestas_nombre ON sigpa_fiestas_fiesta(nombre); 

CREATE FULLTEXT INDEX idx_fiestas ON sigpa_fiestas_fiesta(descripcion, antecedentes); 

CREATE FULLTEXT INDEX idx_lugar_gastronomico ON sigpa_gastronomia_lugar(nombre, antecedentes); 
           
-- *******************************************************************************************************
--                                   PROCEDIMIENTOS ALMACENADOS
-- *******************************************************************************************************

-- SE REALIZA EL SPLIT DE UN ELEMENTO
DELIMITER $$

DROP FUNCTION IF EXISTS SPLIT_STRING $$

CREATE FUNCTION SPLIT_STRING(
  delim VARCHAR(12), 
  str VARCHAR(255), 
  pos INT
) 
RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
RETURN
    REPLACE(
        SUBSTRING(
            SUBSTRING_INDEX(str, delim, pos),
            LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1
        ),
        delim, ''
    )$$
DELIMITER $$

-- OBTENER LISTA DE CATEGORIAS UNESCO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_categoria_unesco $$

CREATE PROCEDURE sp_sgp_obt_categoria_unesco
(  
	 v_texto VARCHAR(250)
)
BEGIN

    SELECT id, nombre, descripcion, fecha_registro, fecha_actualizacion 
    FROM sigpa_categoria_unesco
    WHERE (v_texto = '')
    	  OR (MATCH (nombre, descripcion) AGAINST (v_texto))
    ORDER BY id ASC;               
                  
END $$

DELIMITER ;

-- OBTENER LISTA DE PERFILES DE DOMINIO A LOS QUE ESTA ASOCIADO UN DOMINIO ESPECIFICO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_perfiles_dominios_dominio_especicio $$

CREATE PROCEDURE sp_sgp_obt_perfiles_dominios_dominio_especicio
(  
     v_id_dominio INT
)
BEGIN

    /*
    SELECT PD.id, PD.nombre, PD.descripcion, PD.antecedentes, PD.categoria_unesco_id 
    FROM sigpa_conceptos_perfildominio PD
    INNER JOIN sigpa_conceptos_dominio_especifico_perfil DMP ON (PD.id = DMP.perfildominio_id)
    WHERE (DMP.especialidad_id = v_id_dominio)
    ORDER BY PD.id ASC;               
    */   

    SELECT PD.id
    FROM sigpa_conceptos_perfildominio PD
    INNER JOIN sigpa_conceptos_dominio_especifico_perfil DMP ON (PD.id = DMP.perfildominio_id)
    WHERE (DMP.especialidad_id = v_id_dominio)
    ORDER BY PD.id ASC;        

END $$

DELIMITER ;

-- OBTENER LISTA DE CATEGORIAS UNESCO QUE ESTA ASOCIADAS A VARISO PERFILES DE DOMINIO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_categorias_perfiles_dominio $$

CREATE PROCEDURE sp_sgp_obt_categorias_perfiles_dominio
(  
     v_ids TEXT
)
BEGIN

    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_perfiles;

    
    -- Tabla temporal para categorias unesco
    CREATE TEMPORARY TABLE tmp_perfiles 
    (
        id_perfil INT NOT NULL
    );
  
   
    SET posicion = 0;
    
    loop_perfil: LOOP
         SET posicion = posicion + 1;
         SET str = SPLIT_STRING(",", v_ids, posicion);
         IF str = '' THEN
            LEAVE loop_perfil;
         END IF;

         SET v_id = CONVERT(str, SIGNED INT);
         
         #Do Inserts into temp table here with str going into the row
         INSERT INTO  tmp_perfiles (id_perfil) 
         SELECT id FROM sigpa_conceptos_perfildominio
         WHERE (id = v_id);  

    END LOOP loop_perfil;

    SELECT DISTINCT CU.id
    FROM sigpa_categoria_unesco CU
    INNER JOIN sigpa_conceptos_perfildominio PD ON (CU.id = PD.categoria_unesco_id)
    INNER JOIN tmp_perfiles TP ON (PD.id = TP.id_perfil)
    ORDER BY CU.id ASC;           

    DROP TABLE tmp_perfiles;
END $$

DELIMITER ;


-- OBTENER LISTA DE DOMINIOS PREFERIDOS A PARTIR DE UN DOMINIO ESPECIFICO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominios_preferidos $$

CREATE PROCEDURE sp_sgp_obt_dominios_preferidos
(  
   IN v_id TEXT
)
BEGIN
  
    SELECT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador
    FROM sigpa_conceptos_especialidad E
    INNER JOIN sigpa_conceptos_termino_especifico TE ON (E.id = TE.to_especialidad_id)
    WHERE (TE.from_especialidad_id = v_id)     
          AND (E.id != v_id);           
        
END $$

DELIMITER ;

-- OBTENER LISTA DE DOMINIOS RELACIONADOS A PARTIR DE UN DOMINIO ESPECIFICO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominios_relacionados $$

CREATE PROCEDURE sp_sgp_obt_dominios_relacionados
(  
   IN v_id INT
)
BEGIN
        
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominios;

    
    -- Tabla temporal para categorias unesco
    CREATE TEMPORARY TABLE tmp_dominios 
    (
        id_dominio INT NOT NULL
    );
  
  
    INSERT INTO tmp_dominios(id_dominio)
    SELECT E.id
    FROM sigpa_conceptos_especialidad E
    INNER JOIN sigpa_conceptos_termino_relacionado TR ON (E.id = TR.to_especialidad_id)
    WHERE (TR.from_especialidad_id = v_id)     
          AND (E.id != v_id);

    INSERT INTO tmp_dominios(id_dominio)
    SELECT E.id
    FROM sigpa_conceptos_especialidad E
    INNER JOIN sigpa_conceptos_termino_relacionado TR ON (E.id = TR.from_especialidad_id)
    WHERE (TR.to_especialidad_id = v_id)     
          AND (E.id != v_id);

    SELECT DISTINCT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador
    FROM sigpa_conceptos_especialidad E
    INNER JOIN tmp_dominios TD ON (E.id = TD.id_dominio)
    WHERE (E.id != v_id)
    ORDER BY E.nombre ASC;           
           
    DROP TABLE IF EXISTS tmp_dominios;
END $$

DELIMITER ;

-- OBTENER LISTA DE DOMINIOS USAR PARA A PARTIR DE UN DOMINIO ESPECIFICO
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominios_usar_para $$

CREATE PROCEDURE sp_sgp_obt_dominios_usar_para
(  
   IN v_id INT
)
BEGIN
        
    SELECT DISTINCT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador
    FROM sigpa_conceptos_especialidad E
    INNER JOIN sigpa_conceptos_usar_para UP ON (E.id = UP.from_especialidad_id)
    WHERE (UP.to_especialidad_id = v_id)     
          AND (E.id != v_id);

END $$

DELIMITER ;

-- OBTENER EL DETALLE DE UN DOMINIO PARTICULAR
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominio_particular $$

CREATE PROCEDURE sp_sgp_obt_dominio_particular
(  
   IN v_id INT
)
BEGIN
  
    DECLARE v_id_origen INT;
    
    SET v_id_origen = 0;

    SELECT from_especialidad_id INTO v_id_origen 
    FROM sigpa_conceptos_usar_para 
    WHERE (to_especialidad_id = v_id) LIMIT 1; 

    IF (v_id_origen > 0) THEN
      
      SELECT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador 
      FROM sigpa_conceptos_especialidad E
      WHERE (E.id = v_id_origen);
      
    ELSE
      
      SELECT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador 
      FROM sigpa_conceptos_especialidad E
      WHERE (E.id = v_id);
                    
    END IF;

END $$

DELIMITER ;

-- OBTENER LISTA DE DOMINIOS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominios $$

CREATE PROCEDURE sp_sgp_obt_dominios
(  
   IN v_ids TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominios;

    
    -- Tabla temporal para categorias unesco
    CREATE TEMPORARY TABLE tmp_dominios 
    (
        id_dominio INT NOT NULL
    );
  
   
    SET posicion = 0;
    
    loop_dominio: LOOP
         SET posicion = posicion + 1;
         SET str = SPLIT_STRING(",", v_ids, posicion);
         IF str = '' THEN
            LEAVE loop_dominio;
         END IF;

         SET v_id = CONVERT(str, SIGNED INT);
         
         #Do Inserts into temp table here with str going into the row
         INSERT INTO  tmp_dominios (id_dominio) 
         SELECT id FROM sigpa_conceptos_especialidad
         WHERE (id = v_id);  

    END LOOP loop_dominio;

    SELECT E.id, E.nombre, E.descripcion, E.antecedentes, E.es_agrupador 
    FROM sigpa_conceptos_especialidad E
    INNER JOIN tmp_dominios TD ON (E.id = TD.id_dominio);
                  
    DROP TABLE IF EXISTS tmp_dominios;
        
END $$

DELIMITER ;

-- OBTENER LISTA DE COMUNAS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_comunas $$

CREATE PROCEDURE sp_sgp_obt_comunas
(  
   IN v_ids TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_comunas;

    
    -- Tabla temporal para categorias unesco
    CREATE TEMPORARY TABLE tmp_comunas 
    (
        id_comuna INT NOT NULL
    );
  
   
    SET posicion = 0;
    
    loop_comunas: LOOP
         SET posicion = posicion + 1;
         SET str = SPLIT_STRING(",", v_ids, posicion);
         IF str = '' THEN
            LEAVE loop_comunas;
         END IF;

         SET v_id = CONVERT(str, SIGNED INT);
         
         #Do Inserts into temp table here with str going into the row
         INSERT INTO  tmp_comunas (id_comuna) 
         SELECT id FROM sigpa_geo_comuna
         WHERE (id = v_id);  

    END LOOP loop_comunas;

    SELECT C.id, C.nombre, C.provincia_id 
    FROM sigpa_geo_comuna C
    INNER JOIN tmp_comunas TC ON (C.id = TC.id_comuna);
                  
    DROP TABLE IF EXISTS tmp_comunas;
        
END $$

DELIMITER ;

-- OBTENER LISTA DE CULTORES INDIVIDUALES
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_cultor_individual $$

CREATE PROCEDURE sp_sgp_obt_cultor_individual
(  
   IN v_texto VARCHAR(250),
   IN v_dominios_especificos TEXT,
   IN v_comunas TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominio;
    
    DROP TABLE IF EXISTS tmp_comuna;

    -- Tabla temporal para Dominios Especificos
    CREATE TEMPORARY TABLE tmp_dominio 
    (
        id_dominio INT NOT NULL
    );

    -- Tabla temporal para Comunas
    CREATE TEMPORARY TABLE tmp_comuna 
    (
        id_comuna INT NOT NULL
    );

    /*
    *     SE OBTIENEN LOS DOMINIOS ESPECIFICOS A CONSIDERAR EN LA BUSQUEDA
    */
    IF (v_texto != '') AND (v_dominios_especificos = '') THEN
    
        INSERT INTO tmp_dominio (id_dominio)
        SELECT id FROM sigpa_conceptos_especialidad
        WHERE (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto IN NATURAL LANGUAGE MODE));
    
    END IF;

    IF (v_dominios_especificos != '') THEN
        SET posicion = 0;
        
        loop_dominio: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_dominios_especificos, posicion);
             IF str = '' THEN
                LEAVE loop_dominio;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_dominio (id_dominio) 
             SELECT id FROM sigpa_conceptos_especialidad
             WHERE (id = v_id)
                   AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));  

             INSERT INTO tmp_dominio(id_dominio)
             SELECT E.id FROM sigpa_conceptos_especialidad E
             INNER JOIN sigpa_conceptos_termino_especifico TE ON (E.id = TE.to_especialidad_id)
             WHERE (TE.from_especialidad_id = v_id)
                    AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));

        END LOOP loop_dominio;

    END IF;
    
    IF (v_comunas != '') THEN
    
        SET posicion = 0;
        
        loop_comunas: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_comunas, posicion);
             IF str = '' THEN
                LEAVE loop_comunas;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_comuna (id_comuna) 
             SELECT id FROM sigpa_geo_comuna
             WHERE (id = v_id);  

        END LOOP loop_comunas;

    END IF;

    SELECT F.id, F.slug, F.fecha_creacion, F.fecha_modificacion, F.publicado, F.documentador_id, 
           F.localizacion_id, F.vistas, F.eliminado, F.origen, F.ficha, CI.ficha_ptr_id, CI.rut, 
           CI.antecedentes, CI.nombre_contacto, 
           CI.email, CI.sitio_web, CI.telefono1, 
           CI.telefono2, CI.nombres, CI.apellido_paterno, 
           CI.apellido_materno, CI.fecha_nacimiento, 
           CI.fecha_defuncion, CI.vivo_muerto, 
           CI.genero, CI.comuna_nacimiento_id, 
           CI.lugar_nacimiento, CI.proveniencia_cultural_id, 
           CI.es_thv, CI.anio_thv, 
           CI.anios_postulaciones_thv, CI.postulacion_thv_destacada 
    FROM sigpa_cultores_cultorindividual CI
    INNER JOIN sigpa_cultores_ficha F ON (CI.ficha_ptr_id = F.id) 
    INNER JOIN sigpa_geo_localizacion L ON (F.localizacion_id = L.id)
    INNER JOIN sigpa_geo_comuna C ON (L.comuna_id = C.id)
    INNER JOIN sigpa_geo_provincia P ON (C.provincia_id = P.id)
    LEFT JOIN tmp_comuna TC ON (C.id = TC.id_comuna)
    WHERE (F.publicado = 1) 
          AND (F.eliminado = 0)
          AND ((v_comunas = '') OR (C.id = TC.id_comuna))          
          AND ((v_texto = '') OR (MATCH (nombres, apellido_paterno, apellido_materno, antecedentes, nombre_contacto) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)))                         
          AND ((v_dominios_especificos = '') 
                OR ((SELECT count(1) FROM sigpa_cultores_cultorindividual_especialidades CDE
                      INNER JOIN tmp_dominio T ON (CDE.especialidad_id = T.id_dominio)
                      WHERE (CDE.cultorindividual_id = F.id)) > 0))                                 
    ORDER BY F.id DESC;             
    
    DROP TABLE IF EXISTS tmp_dominio;

    DROP TABLE IF EXISTS tmp_comuna;    
END $$

DELIMITER ;


-- OBTENER LISTA DE CULTORES COLECTIVOS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_cultor_colectivo $$

CREATE PROCEDURE sp_sgp_obt_cultor_colectivo
(  
   IN v_texto VARCHAR(250),
   IN v_dominios_especificos TEXT,
   IN v_comunas TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominio;
    
    DROP TABLE IF EXISTS tmp_comuna;

    -- Tabla temporal para Dominios Especificos
    CREATE TEMPORARY TABLE tmp_dominio 
    (
        id_dominio INT NOT NULL
    );

    -- Tabla temporal para Comunas
    CREATE TEMPORARY TABLE tmp_comuna 
    (
        id_comuna INT NOT NULL
    );

    /*
    *     SE OBTIENEN LOS DOMINIOS ESPECIFICOS A CONSIDERAR EN LA BUSQUEDA
    */
    IF (v_texto != '') AND (v_dominios_especificos = '') THEN
    
        INSERT INTO tmp_dominio (id_dominio)
        SELECT id FROM sigpa_conceptos_especialidad
        WHERE (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto IN NATURAL LANGUAGE MODE));
    
    END IF;

    IF (v_dominios_especificos != '') THEN
        SET posicion = 0;
        
        loop_dominio: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_dominios_especificos, posicion);
             IF str = '' THEN
                LEAVE loop_dominio;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_dominio (id_dominio) 
             SELECT id FROM sigpa_conceptos_especialidad
             WHERE (id = v_id)
                   AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));  

            INSERT INTO tmp_dominio(id_dominio)
            SELECT E.id FROM sigpa_conceptos_especialidad E
            INNER JOIN sigpa_conceptos_termino_especifico TE ON (E.id = TE.to_especialidad_id)
            WHERE (TE.from_especialidad_id = v_id)
                    AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));

        END LOOP loop_dominio;

    END IF;
    
    IF (v_comunas != '') THEN
    
        SET posicion = 0;
        
        loop_comunas: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_comunas, posicion);
             IF str = '' THEN
                LEAVE loop_comunas;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_comuna (id_comuna) 
             SELECT id FROM sigpa_geo_comuna
             WHERE (id = v_id);  

        END LOOP loop_comunas;

    END IF;

    SELECT F.id, F.slug, F.fecha_creacion, F.fecha_modificacion, F.publicado, F.documentador_id, 
           F.localizacion_id, F.vistas, F.eliminado, F.origen, F.ficha, CC.ficha_ptr_id, CC.rut,
           CC.antecedentes, CC.nombre_contacto, CC.email, CC.sitio_web, CC.telefono1, CC.telefono2,
           CC.nombre, CC.fecha_fundacion, CC.esta_activo, CC.composicion, CC.comuna_fundacion_id, CC.lugar_fundacion,
           CC.personalidad_juridica, CC.proveniencia_cultural_id, CC.es_thv, CC.anio_thv, CC.anios_postulaciones_thv,
           CC.postulacion_thv_destacada
    FROM sigpa_cultores_cultorcolectivo CC
    INNER JOIN sigpa_cultores_ficha F ON ( CC.ficha_ptr_id = F.id)
    INNER JOIN sigpa_geo_localizacion L ON (F.localizacion_id = L.id)
    INNER JOIN sigpa_geo_comuna C ON (L.comuna_id = C.id)
    INNER JOIN sigpa_geo_provincia P ON (C.provincia_id = P.id)
    LEFT JOIN tmp_comuna TC ON (C.id = TC.id_comuna) 
    WHERE (F.publicado = 1) 
          AND (F.eliminado = 0)
          AND ((v_comunas = '') OR (C.id = TC.id_comuna)) 
          AND ((v_texto = '') OR (MATCH (CC.antecedentes, CC.nombre, CC.lugar_fundacion, CC.nombre_contacto) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)))                         
          AND ((v_dominios_especificos = '')
                OR ((SELECT count(1) FROM sigpa_cultores_cultorindividual_especialidades CDE
                      INNER JOIN tmp_dominio T ON (CDE.especialidad_id = T.id_dominio)
                      WHERE (CDE.cultorindividual_id = F.id)) > 0))                            
    ORDER BY F.id DESC;        
    
    DROP TABLE IF EXISTS tmp_dominio;

    DROP TABLE IF EXISTS tmp_comuna;    
END $$

DELIMITER ;


-- OBTENER LISTA DE FIESTAS RELIGIOSAS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_fiestas $$

CREATE PROCEDURE sp_sgp_obt_fiestas
(  
   IN v_texto VARCHAR(250),
   IN v_dominios_especificos TEXT,
   IN v_comunas TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominio;
    
    DROP TABLE IF EXISTS tmp_comuna;

    -- Tabla temporal para Dominios Especificos
    CREATE TEMPORARY TABLE tmp_dominio 
    (
        id_dominio INT NOT NULL
    );

    -- Tabla temporal para Comunas
    CREATE TEMPORARY TABLE tmp_comuna 
    (
        id_comuna INT NOT NULL
    );

    /*
    *     SE OBTIENEN LOS DOMINIOS ESPECIFICOS A CONSIDERAR EN LA BUSQUEDA
    */
    IF (v_texto != '') AND (v_dominios_especificos = '') THEN
    
        INSERT INTO tmp_dominio (id_dominio)
        SELECT id FROM sigpa_conceptos_especialidad
        WHERE (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto IN NATURAL LANGUAGE MODE));
    
    END IF;

    IF (v_dominios_especificos != '') THEN
        SET posicion = 0;
        
        loop_dominio: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_dominios_especificos, posicion);
             IF str = '' THEN
                LEAVE loop_dominio;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_dominio (id_dominio) 
             SELECT id FROM sigpa_conceptos_especialidad
             WHERE (id = v_id)
                   AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));  

            INSERT INTO tmp_dominio(id_dominio)
            SELECT E.id FROM sigpa_conceptos_especialidad E
            INNER JOIN sigpa_conceptos_termino_especifico TE ON (E.id = TE.to_especialidad_id)
            WHERE (TE.from_especialidad_id = v_id)
                    AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));

        END LOOP loop_dominio;

    END IF;
    
    IF (v_comunas != '') THEN
    
        SET posicion = 0;
        
        loop_comunas: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_comunas, posicion);
             IF str = '' THEN
                LEAVE loop_comunas;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_comuna (id_comuna) 
             SELECT id FROM sigpa_geo_comuna
             WHERE (id = v_id);  

        END LOOP loop_comunas;

    END IF;

    SELECT F.id, F.slug, F.fecha_creacion, F.fecha_modificacion, F.publicado, F.documentador_id, 
           F.localizacion_id, F.vistas, F.eliminado, F.origen, F.ficha, FR.ficha_ptr_id, 
           FR.nombre, FR.fecha_inicio, FR.fecha_termino, FR.fecha_dia_principal, FR.descripcion, 
           FR.antecedentes, FR.termino_actividad, FR.fecha_termino_actividad 
    FROM sigpa_fiestas_fiesta FR
    INNER JOIN sigpa_cultores_ficha F ON (FR.ficha_ptr_id = F.id)
    INNER JOIN sigpa_geo_localizacion L ON (F.localizacion_id = L.id)
    INNER JOIN sigpa_geo_comuna C ON (L.comuna_id = C.id)
    INNER JOIN sigpa_geo_provincia P ON (C.provincia_id = P.id)
    LEFT JOIN tmp_comuna TC ON (C.id = TC.id_comuna) 
    WHERE (F.publicado = 1) 
          AND (F.eliminado = 0)
          AND ((v_comunas = '') OR (C.id = TC.id_comuna)) 
          AND ((v_texto = '') OR (MATCH (FR.nombre) AGAINST (v_texto  IN NATURAL LANGUAGE MODE))
                              OR (MATCH (FR.descripcion, FR.antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)))                         
          AND ((v_dominios_especificos = '') 
                OR ((SELECT count(1) FROM sigpa_fiestas_fiesta_dominio_especifico FDE
                      INNER JOIN tmp_dominio T ON (FDE.especialidad_id = T.id_dominio)
                      WHERE (FDE.fiesta_id = F.id)) > 0))                          
    ORDER BY F.id DESC;        
    
    DROP TABLE IF EXISTS tmp_dominio;

    DROP TABLE IF EXISTS tmp_comuna;    
END $$

DELIMITER ;


-- OBTENER LISTA DE LUGARES GASTRONOMICOS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_lugares $$

CREATE PROCEDURE sp_sgp_obt_lugares
(  
   IN v_texto VARCHAR(250),
   IN v_dominios_especificos TEXT,
   IN v_comunas TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominio;
    
    DROP TABLE IF EXISTS tmp_comuna;

    -- Tabla temporal para Dominios Especificos
    CREATE TEMPORARY TABLE tmp_dominio 
    (
        id_dominio INT NOT NULL
    );

    -- Tabla temporal para Comunas
    CREATE TEMPORARY TABLE tmp_comuna 
    (
        id_comuna INT NOT NULL
    );

    /*
    *     SE OBTIENEN LOS DOMINIOS ESPECIFICOS A CONSIDERAR EN LA BUSQUEDA
    */
    IF (v_texto != '') AND (v_dominios_especificos = '') THEN
    
        INSERT INTO tmp_dominio (id_dominio)
        SELECT id FROM sigpa_conceptos_especialidad
        WHERE (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto IN NATURAL LANGUAGE MODE));
    
    END IF;

    IF (v_dominios_especificos != '') THEN
        SET posicion = 0;
        
        loop_dominio: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_dominios_especificos, posicion);
             IF str = '' THEN
                LEAVE loop_dominio;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_dominio (id_dominio) 
             SELECT id FROM sigpa_conceptos_especialidad
             WHERE (id = v_id)
                   AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));  

            INSERT INTO tmp_dominio(id_dominio)
            SELECT E.id FROM sigpa_conceptos_especialidad E
            INNER JOIN sigpa_conceptos_termino_especifico TE ON (E.id = TE.to_especialidad_id)
            WHERE (TE.from_especialidad_id = v_id)
                    AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));

        END LOOP loop_dominio;

    END IF;
    
    IF (v_comunas != '') THEN
    
        SET posicion = 0;
        
        loop_comunas: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_comunas, posicion);
             IF str = '' THEN
                LEAVE loop_comunas;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_comuna (id_comuna) 
             SELECT id FROM sigpa_geo_comuna
             WHERE (id = v_id);  

        END LOOP loop_comunas;

    END IF;

    SELECT F.id, F.slug, F.fecha_creacion, F.fecha_modificacion, F.publicado, F.documentador_id, 
           F.localizacion_id, F.vistas, F.eliminado, F.origen, F.ficha, LG.ficha_ptr_id, 
           LG.nombre, LG.rut, LG.fecha_fundacion, LG.antecedentes, LG.categoria_id, LG.capacidad, LG.idioma, 
           LG.estacionamiento, LG.menu_almuerzo, LG.menu_ninos, LG.acceso_minusvalidos, 
           LG.carta_vinos_cervezas, LG.derecho_descorche, LG.precio_promedio_pp, LG.ley_tabaco, 
           LG.horarios_y_dias_atencion, LG.tiene_resolucion_sanitaria, LG.giro_finiquitado, LG.fecha_termino_giro
    FROM sigpa_gastronomia_lugar LG
    INNER JOIN sigpa_cultores_ficha F ON (LG.ficha_ptr_id = F.id)
    INNER JOIN sigpa_geo_localizacion L ON (F.localizacion_id = L.id)
    INNER JOIN sigpa_geo_comuna C ON (L.comuna_id = C.id)
    INNER JOIN sigpa_geo_provincia P ON (C.provincia_id = P.id)
    LEFT JOIN tmp_comuna TC ON (C.id = TC.id_comuna) 
    WHERE (F.publicado = 1) 
          AND (F.eliminado = 0)
          AND ((v_comunas = '') OR (C.id = TC.id_comuna)) 
          AND ((v_texto = '') OR (MATCH (LG.nombre,  LG.antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)))                         
          AND ((v_dominios_especificos = '') 
                OR ((SELECT count(1) FROM sigpa_gastronomia_lugar_dominio_especifico LDE
                      INNER JOIN tmp_dominio T ON (LDE.especialidad_id = T.id_dominio)
                      WHERE (LDE.lugar_id = F.id)) > 0))                           
    ORDER BY F.id DESC;        
    
    DROP TABLE IF EXISTS tmp_dominio;

    DROP TABLE IF EXISTS tmp_comuna;    
END $$

DELIMITER ;

-- OBTENER LISTA DE DOMINIOS ESPECIFICOS CON LA CANTIDAD DE REGISTROS
DELIMITER $$

DROP PROCEDURE IF EXISTS sp_sgp_obt_dominios_con_registros $$

CREATE PROCEDURE sp_sgp_obt_dominios_con_registros
(  
   IN v_texto VARCHAR(250),
   IN v_dominios_especificos TEXT,
   IN v_id_perfil INT,
   IN v_comunas TEXT
)
BEGIN
  
    DECLARE v_id INT;
    DECLARE posicion INT Default 0 ;
    DECLARE str VARCHAR(255);
    
    -- Borramos las tablas temporales
    DROP TABLE IF EXISTS tmp_dominio;
    
    DROP TABLE IF EXISTS tmp_comuna;

    -- Tabla temporal para Dominios Especificos
    CREATE TEMPORARY TABLE tmp_dominio 
    (
        id_dominio INT NOT NULL
    );

    -- Tabla temporal para Comunas
    CREATE TEMPORARY TABLE tmp_comuna 
    (
        id_comuna INT NOT NULL
    );

    /*
    *     SE OBTIENEN LOS DOMINIOS ESPECIFICOS A CONSIDERAR EN LA BUSQUEDA
    */
    IF (v_texto != '') AND (v_dominios_especificos = '') THEN
    
        INSERT INTO tmp_dominio (id_dominio)
        SELECT id FROM sigpa_conceptos_especialidad
        WHERE (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto IN NATURAL LANGUAGE MODE));
    
    END IF;

    IF (v_dominios_especificos != '') THEN
        SET posicion = 0;
        
        loop_dominio: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_dominios_especificos, posicion);
             IF str = '' THEN
                LEAVE loop_dominio;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_dominio (id_dominio) 
             SELECT id FROM sigpa_conceptos_especialidad
             WHERE (id = v_id)
                   AND ((v_texto = '') OR (MATCH (nombre, descripcion, antecedentes) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)));  

        END LOOP loop_dominio;

    END IF;
    
    IF (v_comunas != '') THEN
    
        SET posicion = 0;
        
        loop_comunas: LOOP
             SET posicion = posicion + 1;
             SET str = SPLIT_STRING(",", v_comunas, posicion);
             IF str = '' THEN
                LEAVE loop_comunas;
             END IF;

             SET v_id = CONVERT(str, SIGNED INT);
             
             #Do Inserts into temp table here with str going into the row
             INSERT INTO  tmp_comuna (id_comuna) 
             SELECT id FROM sigpa_geo_comuna
             WHERE (id = v_id);  

        END LOOP loop_comunas;

    END IF;

    SELECT F.id, F.slug, F.fecha_creacion, F.fecha_modificacion, F.publicado, F.documentador_id, 
           F.localizacion_id, F.vistas, F.eliminado, F.origen, F.ficha, CI.ficha_ptr_id, CI.rut, 
           CI.antecedentes, CI.nombre_contacto, 
           CI.email, CI.sitio_web, CI.telefono1, 
           CI.telefono2, CI.nombres, CI.apellido_paterno, 
           CI.apellido_materno, CI.fecha_nacimiento, 
           CI.fecha_defuncion, CI.vivo_muerto, 
           CI.genero, CI.comuna_nacimiento_id, 
           CI.lugar_nacimiento, CI.proveniencia_cultural_id, 
           CI.es_thv, CI.anio_thv, 
           CI.anios_postulaciones_thv, CI.postulacion_thv_destacada 
    FROM sigpa_cultores_cultorindividual CI
    INNER JOIN sigpa_cultores_ficha F ON (CI.ficha_ptr_id = F.id) 
    INNER JOIN sigpa_geo_localizacion L ON (F.localizacion_id = L.id)
    INNER JOIN sigpa_geo_comuna C ON (L.comuna_id = C.id)
    INNER JOIN sigpa_geo_provincia P ON (C.provincia_id = P.id)
    LEFT JOIN tmp_comuna TC ON (C.id = TC.id_comuna)
    WHERE (F.publicado = 1) 
          AND (F.eliminado = 0)
          AND ((v_comunas = '') OR (C.id = TC.id_comuna))          
          AND (
          ((v_texto = '') OR (MATCH (nombres, apellido_paterno, apellido_materno, antecedentes, nombre_contacto) AGAINST (v_texto  IN NATURAL LANGUAGE MODE)))                         
          OR (((v_texto = '') AND (v_dominios_especificos = '')) 
                OR ((SELECT count(1) FROM sigpa_cultores_cultorindividual_especialidades CDE
                      INNER JOIN tmp_dominio T ON (CDE.especialidad_id = T.id_dominio)
                      WHERE (CDE.cultorindividual_id = F.id)) > 0))  
          )                          
    ORDER BY F.id DESC;             
    
    DROP TABLE IF EXISTS tmp_dominio;

    DROP TABLE IF EXISTS tmp_comuna;    
END $$

DELIMITER ;
