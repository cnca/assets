//Se limpian los select de provincias y comunas
function limpiar_provincias_comunas(opcion)
{
    if (opcion == 0)
    { 	
	$("#cb_provincia").find('option').remove();
	$("#cb_provincia").append("<option value='-1'>Provincia</option>").val("-1");
    
	$("#cb_comuna").find('option').remove();
	$("#cb_comuna").append("<option value='-1'>Comuna</option>").val("-1");
    }
    else
    {
	$("#cb_comuna").find('option').remove();
	$("#cb_comuna").append("<option value='-1'>Comuna</option>").val("-1");
    }
}

//Se realiza la carga de provincias asociadas a una Region
function cargar_provincias()
{
    slug_region = $("#cb_region").val();
    if (slug_region != -1)
    {
	limpiar_provincias_comunas(0);	
	$.ajax({
	    type: "POST",
	    url: "/cargar-provincias/" + slug_region,
	    contentType: "application/json",
	    cache: false,
	    dataType: "json",
	    data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value},
	    success: function (provincias) {
		if (provincias != null) 
		{		    
		    for(i=0;i < provincias.length;i++)
		    {
			provincia = provincias[i];
			$("#cb_provincia").append("<option value='" + provincia.pk + "'>" + provincia.fields.nombre + "</option>").val("-1");
		    }
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		
	    } 
	});
    }
    else
    {
	limpiar_provincias_comunas(0);
    }
}

//Se realiza la carga de comunas asociadas a una Provincia
function cargar_comunas()
{
    id_provincia = $("#cb_provincia").val();
    if (id_provincia != -1)
    {
	limpiar_provincias_comunas(1);	
	$.ajax({
	    type: "POST",
	    url: "/cargar-comunas/" + id_provincia,
	    contentType: "application/json",
	    cache: false,
	    dataType: "json",
	    data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value},
	    success: function (comunas) {
		if (comunas != null) 
		{		    
		    for(i=0;i < comunas.length;i++)
		    {
			comuna = comunas[i];
			$("#cb_comuna").append("<option value='" + comuna.pk + "'>" + comuna.fields.nombre + "</option>").val("-1");
		    }
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
		
	    } 
	});
    }
    else
    {
	limpiar_provincias_comunas(1);
    }
}

//Se realiza la busqueda de acuerdo a los datos ingresados
function buscar_registros()
{
    nombre = $("#nombre").val();
    if (nombre == "")
    {
	nombre = "name_src";
    }
    
    slug_region = $("#cb_region").val();
    id_provincia = $("#cb_provincia").val();
    id_comuna = $("#cb_comuna").val();
    id_dominio = $("#cb_dominio").val();

    img_load = "<div style='padding-left:15%;'>";
    img_load += "   <img alt='Spinner' src='media/vendor/scripts/scroll/spinner.gif?1294888830' /> cargando...";
    img_load += "</div>";
    $("#dv_resultados").html(img_load);
    
    $.ajax({
	type: "POST",
	url: "/buscar-resultado/" + nombre + "/" + slug_region + "/" + id_provincia + "/" + id_comuna + "/" + id_dominio, 
	data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       nombre: $("#nombre").val()},
	
	success: function (response) {
	    if (response != "ERROR")
	    {		
		$("#dv_resultados").html(response);
		iniciar_listado();
		ReemplazarImagenesRotas();
	    }
	    else
	    {
		alert("Ocurrio un error");
	    }
	},
	error: function (xhr, ajaxOptions, thrownError) {
	    $("#dv_resultados").html("No se encontraron registros, intente nuevamente");
	} 
    });
}