//Se realiza la validacion del paso 1
function validar_paso_1()
{
    resultado = true;
    
    $('#rut').removeClass('requires');
    $('#apellido_paterno').removeClass('requires');
    $('#apellido_materno').removeClass('requires');
    $('#nombres').removeClass('requires');
    $('#btn_genero').attr('style', style_exito);
    // $('#btn_proveniencia_cultural').attr('style', style_exito);
    $('#btn_lenguas').attr('style', style_exito);
        
    if (($.trim($('#rut').val()) != "") && (validar_rut($('#rut').val()) == false))
    {
    //if ($.trim($('#rut').val()) == "")
    //{
        $('#rut').addClass('requires');
        $('#rut').attr('title', 'Debe ingresar un rut válido');
        $('#rut').tooltip();
        resultado = false;
    }

    if ($.trim($('#apellido_paterno').val()) == "")
    {
        $('#apellido_paterno').addClass('requires');
        $('#apellido_paterno').attr('title', 'Debe ingresar su Apellido Paterno');
        $('#apellido_paterno').tooltip();
        resultado = false;
    }

    if ($.trim($('#apellido_materno').val()) == "")
    {
        $('#apellido_materno').addClass('requires');
        $('#apellido_materno').attr('title', 'Debe ingresar su Apellido Materno');
        $('#apellido_materno').tooltip();
        resultado = false;
    }

    if ($.trim($('#nombres').val()) == "")
    {
        $('#nombres').addClass('requires');
        $('#nombres').attr('title', 'Debe ingresar su Nombre');
        $('#nombres').tooltip();
        resultado = false;
    }

    if (($('#genero').val() == "") || ($('#genero').val() == "-1"))
    {        
        $('#btn_genero').attr('style', style_error);
        $('#btn_genero').attr('title', 'Debe seleccionar el Género');
        $('#btn_genero').tooltip();
        resultado = false;
    }
    
    // if (($('#proveniencia_cultural').val() == "") || ($('#proveniencia_cultural').val() == "-1"))
    // {
    //     $('#btn_proveniencia_cultural').attr('style', style_error);
    //     $('#btn_proveniencia_cultural').attr('title', 'Debe seleccionar la Proveniencia Cultural');
    //     $('#btn_proveniencia_cultural').tooltip();
    //     resultado = false;
    // }
    
    if (($('#lenguas').val() == null) || ($('#lenguas').val() == "") || ($('#lenguas').val() == "-1"))
    {
        $('#btn_lenguas').attr('style', style_error);
        $('#btn_lenguas').attr('title', 'Debe seleccionar una lengua');
        $('#btn_lenguas').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $.ajax({
            type: "POST",
            url: "/sigpa/mi-cuenta/registro/validar-ficha-existe/",
            async: false,
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    tipo_registro : 'CI',
                    operacion : $('#operacion').val(),
                    id_registro : $('#id_registro').val(),
                    rut: $('#rut').val(),
                    nombres: $('#nombres').val(),
                    apellido_paterno: $('#apellido_paterno').val(),
                    apellido_materno: $('#apellido_materno').val(),
                    },
            success : function(respuesta){   
                if (respuesta == 'OK')
                {
                    resultado = true;
                }
                else
                {
                    if (respuesta == 'EXISTE-FICHA')
                    {                    
                        $('#apellido_paterno').addClass('requires');
                        $('#apellido_paterno').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#apellido_paterno').tooltip();
                        
                        $('#apellido_materno').addClass('requires');
                        $('#apellido_materno').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#apellido_materno').tooltip();
                        
                        $('#nombres').addClass('requires');
                        $('#nombres').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#nombres').tooltip();
                        
                    }
                    else if (respuesta == 'EXISTE-TODO')
                    {
                        $('#rut').addClass('requires');
                        $('#rut').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#rut').tooltip();
                        
                        $('#apellido_paterno').addClass('requires');
                        $('#apellido_paterno').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#apellido_paterno').tooltip();
                        
                        $('#apellido_materno').addClass('requires');
                        $('#apellido_materno').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#apellido_materno').tooltip();
                        
                        $('#nombres').addClass('requires');
                        $('#nombres').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#nombres').tooltip();
                        
                    }
                    else if (respuesta == 'EXISTE-RUT')
                    {
                        $('#rut').addClass('requires');
                        $('#rut').attr('title', 'El registro ya existe. Si requiere autoría de la ficha escribir a sigpa.contacto@gmail.com');
                        $('#rut').tooltip();                    
                    }
                    else
                    {
                        $('#rut').addClass('requires');
                        $('#rut').attr('title', 'Revise los datos, existen problemas en la ficha');
                        $('#rut').tooltip();                        
                    }
                    
                    resultado = false;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {                
                    resultado = false;
                }
            });
        
        return resultado;
    }
    else
    {
        return resultado;
    }
}

//Se realiza la validacion del paso 2
function validar_paso_2()
{
    resultado = true;
    $('#fecha_nacimiento').removeClass('requires');
    //$('#comuna_nacimiento').removeClass('requires');
    $('#btn_comuna_nacimiento').attr('style', style_exito);
    // $('#lugar_nacimiento').removeClass('requires');

    if ($.trim($('#fecha_nacimiento').val()) == "")
    {
        $('#fecha_nacimiento').addClass('requires');
        $('#fecha_nacimiento').attr('title', 'Debe ingresar la Fecha de Nacimiento');
        $('#fecha_nacimiento').tooltip();
        resultado = false;
    }

    if (($('#comuna_nacimiento').val() == "") || ($('#comuna_nacimiento').val() == "-1"))
    {
        $('#btn_comuna_nacimiento').attr('style', style_error);
        $('#btn_comuna_nacimiento').attr('title', 'Debe seleccionar lba Comuna de Nacimiento');
        $('#btn_comuna_nacimiento').tooltip();
        resultado = false;
    }
    
    // if ($.trim($('#lugar_nacimiento').val()) == "")
    // {
    //     $('#lugar_nacimiento').addClass('requires');
    //     $('#lugar_nacimiento').attr('title', 'Debe ingresar el Lugar de Nacimiento');
    //     $('#lugar_nacimiento').tooltip();
    //     resultado = false;
    // }    
    
    
    action = $("#frm_cultor").attr("action");
    if ((resultado == true) && (action.indexOf('agregar') > 0))
    {
        mapa_inicio("map_canvas");
    }
    return resultado;    
}

//Se realiza la validacion del paso 3
function validar_paso_3()
{
    resultado = true;
    $('#btn_localizacion-comuna').attr('style', style_exito);    
    $('#localizacion-ciudad').removeClass('requires');
    $('#localizacion-calle_avenida').removeClass('requires');
    $('#localizacion-numero').removeClass('requires');
    $('#localizacion-otra_referencia').removeClass('requires');
    
    comuna = $("#localizacion-comuna option:selected").text();
    if (comuna == "Comuna")
    {
        $('#btn_localizacion-comuna').attr('style', style_error);
        //$('#btn_localizacion-comuna').addClass('requires');
        $('#btn_localizacion-comuna').attr('title', 'Debe ingresar la comuna');
        $('#btn_localizacion-comuna').tooltip();
        resultado = false;
    }

    if ($.trim($('#localizacion-ciudad').val()) == "")
    {
        $('#localizacion-ciudad').addClass('requires');
        $('#localizacion-ciudad').attr('title', 'Debe ingresar la ciudad');
        $('#localizacion-ciudad').tooltip();
        resultado = false;
    }
        

    if ($.trim($('#localizacion-calle_avenida').val()) == "")
    {
        $('#localizacion-calle_avenida').addClass('requires');
        $('#localizacion-calle_avenida').attr('title', 'Debe ingresar la Calle');
        $('#localizacion-calle_avenida').tooltip();
        resultado = false;
    }
                
    return resultado;    
}


//Se realiza la validacion del paso 4
function validar_paso_4()
{
    resultado = true;
    $('#nombre_contacto').removeClass('requires');
    $('#email').removeClass('requires');
    $('#sitio_web').removeClass('requires');
    $('#telefono1').removeClass('requires');
    $('#telefono2').removeClass('requires');

    if ($.trim($('#nombre_contacto').val()) == "")
    {
        $('#nombre_contacto').addClass('requires');
        $('#nombre_contacto').attr('title', 'Debe ingresar el Nombre del Contacto');
        $('#nombre_contacto').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#email').val()) == "")
    {
        $('#email').addClass('requires');
        $('#email').attr('title', 'Debe ingresar el Correo Eletrónico del Contacto');
        $('#email').tooltip();
        resultado = false;
    }


    if (($.trim($('#email').val()) != "") && (validar_email($('#email').val()) == false))
    {
        $('#email').addClass('requires');
        $('#email').attr('title', 'Debe ingresar un Correo Eletrónico válido para el Contacto');
        $('#email').tooltip();
        resultado = false;
    }

    if (($.trim($('#sitio_web').val()) != "") && (validar_url($('#sitio_web').val()) == false))
    {
        $('#sitio_web').addClass('requires');
        $('#sitio_web').attr('title', 'Debe ingresar una url válida. Ej: http://www.google.cl');
        $('#sitio_web').tooltip();
        resultado = false;
    }
    
    return resultado;        
}

//Se realiza la validacion del paso 5
function validar_paso_5()
{
    resultado = true;
    $('#comuna_nacimiento_cultor').removeClass('requires');
    $('#antecedentes_biograficos').removeClass('requires');
    $('#antecedentes_de_la_practica_cultor').removeClass('requires');
    //$('#especialidades').removeClass('requires');
    $('#btn_especialidades').attr('style', style_exito);
    //$('#antecedentes').removeClass('requires');

    if ($.trim($('#antecedentes_biograficos').val()) == "")
    {
        $('#antecedentes_biograficos').addClass('requires');
        $('#antecedentes_biograficos').attr('title', 'Debe ingresar los antecedentes biograficos del Cultor');
        $('#antecedentes_biograficos').tooltip();
        resultado = false;
    }
    if ($.trim($('#antecedentes_de_la_practica_cultor').val()) == "")
    {
        $('#antecedentes_de_la_practica_cultor').addClass('requires');
        $('#antecedentes_de_la_practica_cultor').attr('title', 'Debe ingresar los antecedentes de la práctica del Cultor');
        $('#antecedentes_de_la_practica_cultor').tooltip();
        resultado = false;
    }
        
    if (($('#especialidades').val() == null) || ($('#especialidades').val() == "") || ($('#especialidades').val() == "-1"))
    {
        $('#btn_especialidades').attr('style', style_error);
        $('#btn_especialidades').attr('title', 'Debe seleccionar los Perfiles de Dominio');
        $('#btn_especialidades').tooltip();
        resultado = false;
    }

    return resultado;    

}

//Se realiza la validacion del paso 6
function validar_paso_6()
{
    resultado = true;
    return resultado;    
}

//Se realiza la validacion del paso 7
function validar_paso_7()
{
    resultado = true;
    return resultado;    

}

//Validar ficha
function validar_ficha()
{
    ficha = $("#fl_ficha").val();
    if ($.trim(ficha) != "")
    {
        $("#lg_ficha").html("<strong>Ficha Seleccionada:</strong> " + $("#fl_ficha").val());
    }
    else
    {
        $("#lg_ficha").html("");
    }
}

//Se realiza la validacion del formulario de registro
function validar_formulario(paso_actual, operacion)
{
    if (paso_actual == 1)
    {        
        return validar_paso_1();
    }
    else if (paso_actual == 2)
    {        
        return validar_paso_2();    
    }
    else if (paso_actual == 3)
    {        
        return validar_paso_3();    
    }
    else if (paso_actual == 4)
    {        
        return validar_paso_4();    
    }
    else if (paso_actual == 5)
    {        
        return validar_paso_5();    
    }
    else if (paso_actual == 6)
    {        
        //return true; //validar_paso_6();    
        return true; //validar_paso_6();    
    }
    else if (paso_actual == 7)
    {        
        if(operacion == "add" && validar_paso_7() == true)
        {
            $('form#frm_cultor').submit();
        }
        else if (operacion == "upd")
        {
            $('form#frm_cultor').submit();
        }
        else
        {
            return false;
        }
    }    
}