//Se realiza la validacion del formulario de contacto
function validar_contacto(args)
{
    resultado = true;
    $('#nombre').addClass('form').removeClass('form_error');
    $('#email_visitante').addClass('form').removeClass('form_error');
    $('#asunto').addClass('form').removeClass('form_error');
    $('#mensaje').addClass('form').removeClass('form_error');
    
    if ($('#nombre').val() == "")
    {
        $('#nombre').addClass('form_error').removeClass('form');
        $('#nombre').attr('title', 'Nombre es obligatorio, favor ingresar.');            
        $('#nombre').tooltip({
			track: true
		});
        resultado = false;
    }
    
    if (($('#email_visitante').val() == "") || (validar_email($('#email_visitante').val()) == false))
    {
        $('#email_visitante').addClass('form_error').removeClass('form');
        $('#email_visitante').attr('title', 'Correo eletrónico es obligatorio, revise el formato.');            
        $('#email_visitante').tooltip({
			track: true
		});
        resultado = false;
    }

    if ($('#asunto').val() == "")
    {
        $('#asunto').addClass('form_error').removeClass('form');
        $('#asunto').attr('title', 'Ingrese el asunto del correo');            
        $('#asunto').tooltip({
			track: true
		});
        resultado = false;
    }
    
    if ($('#mensaje').val() == "")
    { 
        $('#mensaje').addClass('form_error').removeClass('form');
        $('#mensaje').attr('title', 'Ingrese el texto asociado al mensaje');            
        $('#mensaje').tooltip({
			track: true
		});
        resultado = false;
    }
    
    return resultado;
}

//Se realiza la validacion del formulario de contacto
function validar_reportar_problema()
{
    resultado = true;
    $('#nombre').addClass('form').removeClass('form_error');
    $('#email_visitante').addClass('form').removeClass('form_error');
    $('#asunto').addClass('form').removeClass('form_error');
    $('#mensaje').addClass('form').removeClass('form_error');
    
    if ($('#nombre').val() == "")
    {
        $('#nombre').addClass('form_error').removeClass('form');
        $('#nombre').attr('title', 'Nombre es obligatorio, favor ingresar.');            
        $('#nombre').tooltip({
			track: true
		});
        resultado = false;
    }
    
    if (($('#email_visitante').val() == "") || (validar_email($('#email_visitante').val()) == false))
    {
        $('#email_visitante').addClass('form_error').removeClass('form');
        $('#email_visitante').attr('title', 'Correo eletrónico es obligatorio, revise el formato.');            
        $('#email_visitante').tooltip({
			track: true
		});
        resultado = false;
    }

    if ($('#asunto').val() == "")
    {
        $('#asunto').addClass('form_error').removeClass('form');
        $('#asunto').attr('title', 'Ingrese el asunto del correo');            
        $('#asunto').tooltip({
			track: true
		});
        resultado = false;
    }
    
    if ($('#mensaje').val() == "")
    { 
        $('#mensaje').addClass('form_error').removeClass('form');
        $('#mensaje').attr('title', 'Ingrese el texto asociado al mensaje');            
        $('#mensaje').tooltip({
			track: true
		});
        resultado = false;
    }
    
    return resultado;
}
