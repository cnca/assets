// ----- FUNCIONES PARA FORMULARIOS -----

function countChar1(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum1').text(max - len);
   }
}

function countChar2(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum2').text(max - len);
   }
}

function countChar3(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum3').text(max - len);
   }
}

function countChar4(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum4').text(max - len);
   }
}

function countChar5(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum5').text(max - len);
   }
}

function countChar6(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum6').text(max - len);
   }
}

function countChar7(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum7').text(max - len);
   }
}

function countChar0(val, max) {
   var len = val.value.length;
   if (len > max) {
      val.value = val.value.substring(0, max);
   } else {
      $('#charNum0').text(max - len);
   }
}

/* 2.4 Integrantes del colectivo o comunidad */
function add_integrante() {
   var nombre = $("#integrante-nombre").val();
   var rut = $("#integrante-rut").val();
   var telefono = $("#integrante-telefono").val();
   var email = $("#integrante-email").val();

   var wrapper = $("#colectivo-integrantes");
   $(wrapper).append("<div><div class='campo5 new'><input id='integrante-nombre' type='text' name='integrante-nombre[]' value='" + nombre + "' disabled></div><div class='campo5 new'><input id='integrante-rut' type='text' name='integrante-rut[]' value='" + rut + "' disabled></div><div class='campo5 new'><input id='integrante-telefono' type='text' name='integrante-telefono[]' value='" + telefono + "' disabled></div><div class='campo5 new'><input id='integrante-email[]' type='text' name='integrante-email[]' value='" + email + "' disabled></div><button type='button' class='button-delete'></button></div>");

   $("#integrante-nombre").val('');
   $("#integrante-rut").val('');
   $("#integrante-telefono").val('');
   $("#integrante-email").val('');

   $(wrapper).on("click", ".button-delete", function(e){
      e.preventDefault();
      $(this).parent('div').remove();
   });
}

/* 5. Anexos: Archivos */
function add_anexo() {
   var nombre = $("#archivo-nombre").val();
   var tipo = $("#archivo-tipo").val();
   var archivo = $("#archivo-archivo").val();

   var wrapper = $("#anexos1");
   $(wrapper).append("<div><div class='campo6 new'><input id='archivo-nombre' type='text' name='archivo-nombre[]' value='" + nombre + "' disabled></div><div class='campo6 new'><input id='archivo-tipo' type='text' name='archivo-tipo[]' value='" + tipo + "' disabled></div><div class='campo6 new'><input id='archivo-archivo' class='subirArchivo' type='file' name='archivo-archivo[]' accept='application/msword, application/pdf' value='" + archivo + "' disabled></div><button type='button' class='button-delete'/></div>");

   $("#archivo-nombre").val('');
   $("#archivo-tipo").val('0');
   $("#archivo-archivo").val('');

   $(wrapper).on("click", ".button-delete", function(e){
      e.preventDefault();
      $(this).parent('div').remove();
   });
}

/* 5. Anexos: Enlaces */
function add_anexo2() {
   var nombre = $("#enlace-nombre").val();
   var tipo = $("#enlace-tipo").val();
   var enlace = $("#enlace-enlace").val();

   var wrapper = $("#anexos2");
   $(wrapper).append("<div><div class='campo6 new'><input id='enlace-nombre' type='text' name='enlace-nombre[]' value='" + nombre + "' disabled></div><div class='campo6 new'><input id='enlace-tipo' type='text' name='enlace-tipo[]' value='" + tipo + "' disabled></div><div class='campo6 new'><input id='enlace-enlace' type='text' name='enlace-enlace[]' value='" + enlace + "' disabled></div><button type='button' class='button-delete'/></div>");

   $("#enlace-nombre").val('');
   $("#enlace-tipo").val('0');
   $("#enlace-enlace").val('');

   $(wrapper).on("click", ".button-delete", function(e){
      e.preventDefault();
      $(this).parent('div').remove();
   });
}

/* 5. Anexos: Postulaciones anteriores */
function add_aniothv() {
   var anio = $("#anterior-anio").val();
   var patrocinador = $("#anterior-nombre_patrocinador").val();

   var wrapper = $("#post-anteriores");
   $(wrapper).append("<div><div class='campo3 new'><input type='text' id='anterior-anio' name='anterior-anio[]' value='" + anio + "' disabled></div><div class='campo3 new'><input type='text' id='anterior-nombre_patrocinador' name='anterior-nombre_patrocinador[]' value='" + patrocinador + "' disabled></div><button type='button' class='button-delete'/></div>");

   $("#anterior-anio").val('');
   $("#anterior-nombre_patrocinador").val('');

   $(wrapper).on("click", ".button-delete", function(e){
      e.preventDefault();
      $(this).parent('div').remove();
   });
}


// ----- OCULTA Y MUESTRA PASOS DE LOS FORMULARIOS -----

function ir_form1(){
   $(".step1").show("fast");
   $(".step2").hide();
   $(".step3").hide();
   $(".step4").hide();
   $(".step5").hide();
   $(".step6").hide();
}

function ir_form2(){
   $(".step1").hide();
   $(".step2").show("fast");
   $(".step3").hide();
   $(".step4").hide();
   $(".step5").hide();
   $(".step6").hide();
}

function ir_form3(){
   $(".step1").hide();
   $(".step2").hide();
   $(".step3").show("fast");
   $(".step4").hide();
   $(".step5").hide();
   $(".step6").hide();
}

function ir_form4(){
   $(".step1").hide();
   $(".step2").hide();
   $(".step3").hide();
   $(".step4").show("fast");
   $(".step5").hide();
   $(".step6").hide();
}

function ir_form5(){
   $(".step1").hide();
   $(".step2").hide();
   $(".step3").hide();
   $(".step4").hide();
   $(".step5").show("fast");
   $(".step6").hide();
}

function ir_form6(){
   $(".step1").hide();
   $(".step2").hide();
   $(".step3").hide();
   $(".step4").hide();
   $(".step5").hide();
   $(".step6").show("fast");
}

// ----- VALIDACION DE PASOS -----

function validar_paso_1(){
   /* Validacion */
   ir_form2();
}

function validar_paso_2(){
   /* Validacion */
   ir_form3();
}

function validar_paso_3(){
   /* Validacion */
   ir_form4();
}

function validar_paso_4(){
   /* Validacion */
   ir_form5();
}

function validar_paso_5(){
   /* Validacion */
   ir_form6();
}

function validar_paso_6(){
   /* Validacion */
   parent.$.fancybox.close();
}