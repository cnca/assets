//Se realiza el scroll de los resultados para dejarlo alineado a la nueva seleccion
function scroll_resultados(ultima_seleccion)
{
    //if (ultima_seleccion != '')
    //{	
	var offset = 0;
	var top_final = $(window).scrollTop() +  offset;
	if (top_final > offset) { 
	    $("#dv_resultados").stop().animate({
		marginTop: (top_final - 70)
	    });
	}
    //});
    //}
}

//Se procesa un reponse y se parsean con json
function parsearJson(ajaxResponse)
{ 
    if (typeof ajaxResponse == "string")
        ajaxResponse = $.parseJSON(ajaxResponse);
    return ajaxResponse;
}

function limpiar_busqueda(base_url){

	$("#ultima_seleccion").val('');
	$('.tres_seleccionado').removeClass("tres_seleccionado");
	$('.dos_seleccionado').next().slideToggle();
	$('.dos_seleccionado').removeClass("dos_seleccionado");

	$('[id^=region_].uno.uno_seleccionado').next().slideToggle();
	$('[id^=region_].uno.uno_seleccionado').removeClass("uno_seleccionado");

	$('#ultima_seleccion').val('');
	$('#perfiles_dominio').val('');		
	$('#dominios_especificos').val('');
	$('#texto_libre').val('');
	$('#regiones').val('');
	$('#provincias').val('');
	$('#comunas').val('');
	$('#estado').val('');
	$('#descartar').val('');
	$('#pagina_actual').val('1');
	$('#cantidad_registros').val('10');
	$('#orden_seleccion_nombre').val('');
	//console.log(base_url);
	url = base_url + 'buscar/?';
	document.location.href = url;
}


//Limpiar la busqueda (respaldo original) (mantiene la ultima url)
function limpiar_busqueda_1(){

	$("#ultima_seleccion").val('');
	$('.tres_seleccionado').removeClass("tres_seleccionado");
	$('.dos_seleccionado').next().slideToggle();
	$('.dos_seleccionado').removeClass("dos_seleccionado");

	$('[id^=region_].uno.uno_seleccionado').next().slideToggle();
	$('[id^=region_].uno.uno_seleccionado').removeClass("uno_seleccionado");

	$.ajax({
	    type: "POST",
	    url: "/obtener-mensaje-inicio/", 
	    data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value, 	    
		},
	    success: function (response) {	    
		if (response != "ERROR")
		{
			div = '<h3 class="title">Buscador</h3>' + 
            		'<section class="margent">' +
                		'<p>'+response['mensaje_buscador']+'</p>' +                
            		'</section>';
			$('.resultados').html(div);	
		}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {	    
			//implementar
	    } 
	});

	$('#ultima_seleccion').val('');
	$('#perfiles_dominio').val('');		
	$('#dominios_especificos').val('');
	$('#texto_libre').val('');
	$('#regiones').val('');
	$('#provincias').val('');
	$('#comunas').val('');
	$('#estado').val('');
	$('#descartar').val('');
	$('#pagina_actual').val('1');
	$('#cantidad_registros').val('10');
}

//Se realiza la eliminacion de un Tag de Perfil a partir de la X
function eliminar_perfil(id_item){
    seleccion = "perfil;" + id_item;
    $('#eliminar_tag').val('yes');
    if ($("#ultima_seleccion").val() != seleccion)
    {
	$("#dv_tag_perfil_" + id_item).remove();

	$("#perfil_" + id_item).removeClass("dos_seleccionado");
	$("#dv_perfiles_" + id_item).slideToggle();
	concatenar_faceta('perfiles_dominio', id_item, 'del');
    }
    else
    {	
	$("#ultima_seleccion").val('');
	$("#dv_tag_" + id_item).remove();
	$("#perfil_" + id_item).removeClass("dos_seleccionado");
	$("#dv_perfiles_" + id_item).slideToggle();
	concatenar_faceta('perfiles_dominio', id_item, 'del');
	perfiles = $("#perfiles_dominio").val();
	if (perfiles != ''){
	    lista = perfiles.split(';');
	    largo = lista.length;
	    if (largo > 0)
	    {
		$("#ultima_seleccion").val('perfil;' + lista[largo - 2]);	    
	    }
	}
    }
    $("#pagina_actual").val('1');
    buscar_registros();
}

//Se realiza la eliminacion de un Tag a partir de la X
function eliminar_dominio(id_item)
{
    seleccion = "dominio;" + id_item;
    $('#eliminar_tag').val('yes');
    if ($("#ultima_seleccion").val() != seleccion)
    {
	$("#dv_tag_dominio_" + id_item).remove();
	$("p[id^='dominio_" + id_item + "_']").removeClass("tres_seleccionado");
	concatenar_faceta('dominios_especificos', id_item, 'del');
    }
    else
    {	
	$("#ultima_seleccion").val('');
	$("#dv_tag_dominio_" + id_item).remove();
	$("p[id^='dominio_" + id_item + "_']").removeClass("tres_seleccionado");
	concatenar_faceta('dominios_especificos', id_item, 'del');
	dominios = $("#dominios_especificos").val();
	if (dominios != ''){
	    lista = dominios.split(';');
	    largo = lista.length;
	    if (largo > 0)
	    {
		$("#ultima_seleccion").val('dominio;' + lista[largo - 2]);	    
	    }
	}
    }
    $("#pagina_actual").val('1');
    buscar_registros();
}

//Se limpian las facetas de la region
function limpiar_region(id_region)
{
    $("#region_" + id_region).removeClass("uno_seleccionado");
    
    actualizar_orden_seleccion('region', id_region, "del");
	    
    $('#dv_provincias_' + id_region + ' > p').each(function(index){
	id = $(this).attr("id");
	
	if (id.indexOf('provincia_') >= 0)
	{
	    data = id.split('_');
	    
	    limpiar_seleccion("provincia", data[1]);
	    
	    provincias = $("#provincias").val();
	    provincias = provincias.replace(";" + data[1] + ";", "");
	    $("#provincias").val(provincias);
	    
	    actualizar_orden_seleccion('provincia', data[1], "del");
	    
	    $("p[id^='provincia_" + data[1] + "_']").removeClass("dos_seleccionado");
	    
	    $('#dv_comunas_' + data[1] + ' > p').each(function(index){
		id = $(this).attr("id");
		    
		if (id.indexOf('comuna_') >= 0)
		{
		    data2 = id.split('_');
		    
		    $("p[id^='comuna_" + data2[1] + "_']").removeClass("tres_seleccionado");
		    limpiar_seleccion("comuna", data2[1]);
		    comunas = $("#comunas").val();
		    comunas = comunas.replace(";" + data2[1] + ";", "");
		    $("#comunas").val(comunas);
		    actualizar_orden_seleccion('comuna', data2[1], "del");	    
		}			    
	    });
	    
	    if ($('#dv_comunas_' + data[1]).is(':visible'))
	    {
		$('#dv_comunas_' + data[1]).slideToggle();
	    }
	}
    });
    
    $("#dv_tag_region_" + id_region).remove();
    if ($('#dv_provincias_' + id_region).is(':visible'))
    {
	$("#dv_provincias_" + id_region).slideToggle();
    }            
}

//Se realiza la eliminacion de un Tag de region a partir de la X
function eliminar_region(id_item)
{
    seleccion = "region;" + id_item;
    $('#eliminar_tag').val('yes');
    if ($("#ultima_seleccion").val() != seleccion)
    {
	$("#dv_tag_region_" + id_item).remove();
	concatenar_faceta('regiones', id_item, 'del');	
    }
    else
    {
	$("#ultima_seleccion").val('');
	$("#dv_tag_region_" + id_item).remove();
	concatenar_faceta('regiones', id_item, 'del');	
    }
    $("#pagina_actual").val('1');
    buscar_registros();
}

//Se reaiza la limpieza de las facetasd eprovincia
function limpiar_provincia(id_provincia)
{
    $("p[id^='provincia_" + id_provincia + "_']").removeClass("dos_seleccionado");
    
    actualizar_orden_seleccion('provincia', id_provincia, "del");

    $('#dv_comunas_' + id_provincia + ' > p').each(function(index){
	id = $(this).attr("id");		    
	if (id.indexOf('comuna_') >= 0)
	{
	    data = id.split('_');
	    
	    $("p[id^='comuna_" + data[1] + "_']").removeClass("tres_seleccionado");
	    
	    comunas = $("#comunas").val();
	    comunas = comunas.replace(";" + data[1] + ";", "");
	    $("#comunas").val(comunas);
	    
	    actualizar_orden_seleccion('comuna', data[1], "del");

	}
    });
    
    if($('#dv_comunas_' + id_provincia).is(':visible'))
    {
	$("#dv_comunas_" + id_provincia).slideToggle();
    }
    
    id_item = $("p[id^='provincia_" + id_provincia + "_']").attr("id");
    datos = id_item.split('_');    
    
    ocultar_provincia = true;
    $("#dv_provincias_" + datos[2] + " > p").each(function(index){
	id = $(this).attr("id");	
	if ($("#" + id ).hasClass("dos_seleccionado"))
	{
	    ocultar_provincia = false;
	}
    });
    
    if (ocultar_provincia)
    {		    
	$("#region_" + datos[2]).removeClass("uno_seleccionado");
	actualizar_orden_seleccion("region", datos[2], 'del');
	datos = id_item.split('_');        
	if($('#dv_provincias_' + datos[2]).is(':visible')){
	    $("#dv_provincias_" + datos[2]).slideToggle();
	}
    }
}

//Se realiza la eliminacion de un Tag de provincia a partir de la X
function eliminar_provincia(id_item)
{
    seleccion = "region;" + id_item;
    $('#eliminar_tag').val('yes');
    if ($("#ultima_seleccion").val() != seleccion)
    {
	$("#dv_tag_provincia_" + id_item).remove();
	concatenar_faceta('provincias', id_item, 'del');	
    }
    else
    {
	$("#ultima_seleccion").val('');
	$("#dv_tag_provincia_" + id_item).remove();
	concatenar_faceta('provincias', id_item, 'del');		
    }    
    $("#pagina_actual").val('1');
    buscar_registros();
}

//Se realiza la limpieza de las Facetas de comuna
function limpiar_comuna(id_comuna)
{
    
    $("p[id^='comuna_" + id_comuna + "_']").removeClass("tres_seleccionado");
    actualizar_orden_seleccion('comuna', id_comuna, "del");
    datos = $("p[id^='comuna_" + id_comuna + "_']").attr("id").split("_");
    
    ocultar_comunas = true;    
    ocultar_provincias = true;
    
    //Verificamos si hay comunas seleccionadas para la provincia de la comuna
    $('#dv_comunas_' + datos[2] + ' > p').each(function(index){
	id = $(this).attr("id");	
	if (id.indexOf('comuna_') >= 0)
	{
	    data = id.split("_");
	    if($("p[id^='comuna_" + data[1] + "_']").hasClass("tres_seleccionado")){	
		ocultar_comunas = false;
	    }		
	}
    });
    
    if (ocultar_comunas)
    {
	$("p[id^='provincia_" + datos[2] + "_']").removeClass("dos_seleccionado");
	actualizar_orden_seleccion("provincia", datos[2], 'del');
	datos = $("p[id^='comuna_" + id_comuna + "_']").attr("id").split("_");    
    
	if ($('#dv_comunas_' + datos[2]).is(':visible'))
	{	    
	    $("#dv_comunas_" + datos[2]).slideToggle();
	}	
    }
    
    datos = $("p[id^='comuna_" + id_comuna + "_']").attr("id").split("_");    
    $("#dv_provincias_" + datos[3] + " > p").each(function(index){
	id = $(this).attr("id");		    
	if (id.indexOf('provincia_') >= 0)
	{
	    data = id.split("_");
	    if($("p[id^='provincia_" + data[1] + "_']").hasClass("dos_seleccionado")){	
		ocultar_provincias = false;
	    }			
	}
    });
    
    datos = $("p[id^='comuna_" + id_comuna + "_']").attr("id").split("_");    
    if (ocultar_provincias)
    {
	$("#region_" + datos[3]).removeClass("uno_seleccionado");
	actualizar_orden_seleccion("region", datos[3], 'del');
	datos = $("p[id^='comuna_" + id_comuna + "_']").attr("id").split("_");    

	if ($('#dv_provincias_' + datos[3]).is(':visible')) {
	    $("#dv_provincias_" + datos[3]).slideToggle();
	}
    }
}
//Se realiza la eliminacion de un Tag a partir de la X
function eliminar_comuna(id_item)
{
    seleccion = "comuna;" + id_item;
    $('#eliminar_tag').val('yes');
    if ($("#ultima_seleccion").val() != seleccion)
    {
	$("#dv_tag_comuna_" + id_item).remove();
	concatenar_faceta('comunas', id_item, 'del');
    }
    else
    {
	$("#ultima_seleccion").val('');
	$("#dv_tag_comuna_" + id_item).remove();
	concatenar_faceta('comunas', id_item, 'del');		
    }
    $("#pagina_actual").val('1');
    buscar_registros();
}

function eliminar_texto(texto)
{

	$('#eliminar_tag').val('yes');
	$("#texto_libre").val("");
	$("#dv_tag_texto").remove();	
	$("#pagina_actual").val('1');
	buscar_registros();
}

//Se obtiene el valor concatenado
function concatenar_faceta(id_campo, id_item, operacion)
{
    if (operacion == "add"){
	datos = $("#" + id_campo).val();
	if (datos.indexOf(";" + id_item + ";") < 0)
	{
	    datos += ";" + id_item + ";";
	    $("#" + id_campo).val(datos);		
	}
		
	if (id_campo == "dominios_especificos")
	{
	    id = $("[id^='dominio_" + id_item + "_']").attr("id");
	    //data = id.split("_");
	    //perfiles = $("#perfiles_dominio").val();
	    //perfiles = perfiles.replace(";" + data[2] + ";", "");	    
	    //$("#perfiles_dominio").val(perfiles);	    
	
	}
	else if (id_campo == "provincias")
	{
	    id = $("[id^='provincia_" + id_item + "_']").attr("id");
	    data = id.split("_");
	    regiones = $("#regiones").val();
	    regiones = regiones.replace(";" + data[2] + ";", "");
	    $("#regiones").val(regiones);	    
	}
	else if (id_campo == "comunas")
	{
	    id = $("[id^='comuna_" + id_item + "_']").attr("id");
	    data = id.split("_");
	    provincias = $("#provincias").val();
	    provincias = provincias.replace(";" + data[2] + ";", "");
	    $("#provincias").val(provincias);	    
	}
    }
    else
    {
	datos = $("#" + id_campo).val();
	datos = datos.replace(";" + id_item + ";", "");
	$("#" + id_campo).val(datos);
	
	if (id_campo == "dominios_especificos")
	{
	    id = $("[id^='dominio_" + id_item + "_']").attr("id");
	    data = id.split("_");
	    
	    dominios = $("#dv_dominios_" + data[2] + " > .tres_seleccionado");	    
	    if (dominios != null && dominios.length == 0){
		$("#perfil_" + data[2]).removeClass("dos_seleccionado");
		actualizar_orden_seleccion("perfil", data[2], 'del');
		$("#dv_dominios_" + data[2]).slideToggle();
	    }
	}
	else if (id_campo == "regiones")
	{
	    limpiar_region(id_item);
	}
	else if (id_campo == "provincias")
	{
	    limpiar_provincia(id_item);	    
	}
	else if (id_campo == "comunas")
	{	    
	    limpiar_comuna(id_item);
	}
    }
}

//Se realiza la limpieza de la ultima seleccion
function limpiar_seleccion(tipo_registro, id_registro)
{
    seleccion = tipo_registro + ";" + id_registro;
    if ($("#ultima_seleccion").val() == seleccion) {
	$("#ultima_seleccion").val("");    
    }
}

//Se agrega/elimina un item al orden de seleccion
function actualizar_orden_seleccion(tipo_registro, id_registro, operacion)
{
    registros = "";
    if (operacion == 'add')
    {
	registros = $("#orden_seleccion").val();
	registros = registros + tipo_registro + ";" + id_registro + "-";
	$("#ultima_seleccion").val(tipo_registro + ";" + id_registro);
    }
    else
    {
	registros = $("#orden_seleccion").val();
	registros = registros.replace(tipo_registro + ";" + id_registro + "-", "");
	
	if (registros.trim() != "")
	{
	    datos = registros.split('-');
	    largo = datos.lenght;
	    $("#ultima_seleccion").val(datos[largo - 2]);
	}
    }
    
    $("#orden_seleccion").val(registros);
}

//Se realiza clic sobre alguna categoria unesco
function click_facetas(id_item, nivel, cargado) {
    perfiles = $.trim($("#perfiles_dominio").val());
    dominios = $.trim($("#dominios_especificos").val());
    regiones = $.trim($("#regiones").val());
    provincias = $.trim($("#provincias").val());
    comunas = $.trim($("#comunas").val());
	
    /*    
    if (perfiles == "" && dominios == "" && regiones == "" && provincias == "" && comunas == "")
    {
	$("#texto_libre").val('');
    }
    */
    
    if (nivel == 1) {	//Categoria Unesco
	estado = $("#estado").val();
	estado = "";
	//alert("Estado: " + estado);
	if ($.trim(estado) == '')
	{	    
	    if (!$("#categoria_" + id_item).hasClass("uno_seleccionado"))
	    {	    	    
		$("#dv_perfiles_" + id_item).slideToggle();
		$("#categoria_" + id_item).addClass("uno_seleccionado");
	    }
	    else
	    {
		$("#categoria_" + id_item).removeClass("uno_seleccionado");
		$('#dv_perfiles_' + id_item + ' > p').each(function(index){		    
			id = $(this).attr("id");		    
			if (id.indexOf('perfil_') >= 0)
			{
			    data = id.replace('perfil_', '').split('_');
			    
			    $("p[id^='perfil_" + data[0] + "']").removeClass("dos_seleccionado");
			    
			    //Se eliminan los perfiles seleccionados
			    perfiles = $("#perfiles_dominio").val();
			    perfiles = perfiles.replace(";" + data[0] + ";", "");
			    $("#perfiles_dominio").val(perfiles);
			    
			    actualizar_orden_seleccion('perfil', data[0], 'del');
			    
			    if ($('#dv_dominios_' + data[0]).is(':visible'))
			    {
				$('#dv_dominios_' + data[0]).slideToggle();
			    }
			    
			    //Se eliminan los dominios ya seleccionados
			    $('#dv_dominios_' + data[0] + ' > p').each(function(index){		    
				id_dominio = $(this).attr("id");
				datos = id_dominio.split("_");
				dominios = $("#dominios_especificos").val();
				dominios = perfiles.replace(";" + datos[1] + ";", "");
				$("#dominios_especificos").val(dominios);
				
				actualizar_orden_seleccion('dominio', datos[1], 'del');			    
			    });			    
			}
		});
		$("#dv_perfiles_" + id_item).slideToggle();
		buscar_registros();
	    }
	}
    }else if (nivel == 2) {  //Perfil de Dominio
	estado = $("#estado").val();
	if($.trim(estado) == '')
	{	
	    if (!$("p[id^='perfil_" + id_item + "_']").hasClass("dos_seleccionado"))
	    {
		$('#dv_dominios_' + id_item + ' > p').removeClass("tres_seleccionado");
		$('#dv_dominios_' + id_item + ' > p').each(function(index){
		    id = $(this).attr("id");		    
		    if (id.indexOf('dominio_') >= 0)
		    {
			data = id.replace('dominio_', '').split('_');
			
			$("p[id^='dominio_" + data[0] + "_']").removeClass("tres_seleccionado");		
		
		    }
		});
		
		$("#perfil_" + id_item).addClass("dos_seleccionado");
		
		actualizar_orden_seleccion('perfil', id_item, 'add');
			    
		concatenar_faceta('perfiles_dominio', id_item, 'add');
		
		$("#dv_dominios_" + id_item).slideToggle();
				
		$("#pagina_actual").val('1');		
	    }
	    else
	    {
		limpiar_seleccion("perfil", id_item);
		$("#perfil_" + id_item).removeClass("dos_seleccionado");
		$('#dv_dominios_' + id_item + ' > p').removeClass("tres_seleccionado");
		$('#dv_dominios_' + id_item + ' > p').each(function(index){
		    id = $(this).attr("id");		    
		    if (id.indexOf('dominio_') >= 0)
		    {
			data = id.replace('dominio_', '').split('_');
			
			$("p[id^='dominio_" + data[0] + "_']").removeClass("tres_seleccionado");		
			dominios = $("#dominios_especificos").val();
			dominios = dominios.replace(";" + data[0] + ";", "");
			$("#dominios_especificos").val(dominios);
			
			actualizar_orden_seleccion('dominio', data[0], 'del');			    
		    }
		});
		
		actualizar_orden_seleccion('perfil', id_item, 'del');
		
		concatenar_faceta('perfiles_dominio', id_item, 'del');
		$("#dv_tag_perfil_" + id_item).remove();		
		$("#pagina_actual").val('1');				
		$("#dv_dominios_" + id_item).slideToggle();		
		
	    }
	    buscar_registros();
	}
    }
    else if (nivel == 3){ //Dominio especifico
	estado = $("#estado").val();
	if(($.trim(estado) == '') || ($.trim(estado) == 'carga_dominio'))
	{
	    id = $("p[id^='dominio_" + id_item + "_']").attr("id");
	    data = id.split("_");
	    
	    if (!$("p[id^='dominio_" + id_item + "_']").hasClass("tres_seleccionado") || (cargado == 1))
	    {
		actualizar_orden_seleccion('dominio', id_item, 'add');			    
		$("p[id^='dominio_" + id_item + "_']").addClass("tres_seleccionado");	
		concatenar_faceta('dominios_especificos', id_item, 'add');		
	    }
	    else
	    {
		limpiar_seleccion("dominio", id_item);
		$("p[id^='dominio_" + id_item + "_']").removeClass("tres_seleccionado");
		concatenar_faceta('dominios_especificos', id_item, 'del');
		actualizar_orden_seleccion("dominio", id_item, 'del');
		$("#dv_tag_dominio_" + id_item).remove();				
	    }
	    
	    $("#pagina_actual").val('1');
			    
	    buscar_registros();

	}
    }
    else if (nivel == 4){ //Region
	estado = $("#estado").val();
	if($.trim(estado) == '')
	{
	    if (!$("#region_" + id_item).hasClass("uno_seleccionado"))
	    {
		actualizar_orden_seleccion('region', id_item, 'add');
		$("#region_" + id_item).addClass("uno_seleccionado");
		concatenar_faceta('regiones', id_item, 'add');		    		
		$("#dv_provincias_" + id_item).slideToggle();
		$("#ultima_seleccion").val("region;" + id_item);			    
	    }
	    else
	    {
		limpiar_seleccion("region", id_item);		
		concatenar_faceta('regiones', id_item, 'del');		
	    }
	    
	    $("#pagina_actual").val('1');
	    buscar_registros();
	}
    }
    else if (nivel == 5){ //Provincia
	estado = $("#estado").val();
	if($.trim(estado) == '')
	{
	    if (!$("p[id^='provincia_" + id_item + "_']").hasClass("dos_seleccionado"))
	    {	    	
		$("#dv_comunas_" + id_item).slideToggle();
		actualizar_orden_seleccion('provincia', id_item, 'add');
		$("[id^=provincia_" + id_item + "_]").addClass("dos_seleccionado");
		concatenar_faceta('provincias', id_item, 'add');
		$("#ultima_seleccion").val("provincia;" + id_item);		
	    }
	    else
	    {		
		limpiar_seleccion("provincia", id_item);		
		concatenar_faceta('provincias', id_item, 'del');				
	    }
	    $("#pagina_actual").val('1');
	    buscar_registros();
	}
    }
    else if (nivel == 6){ //Comuna
	estado = $("#estado").val();
	if($.trim(estado) == '')
	{
	    if (!$("p[id^='comuna_" + id_item + "_']").hasClass("tres_seleccionado") || (cargado == 1))
	    {
		actualizar_orden_seleccion('comuna', id_item, 'add');
		$("p[id^='comuna_" + id_item + "_']").addClass("tres_seleccionado");
		concatenar_faceta('comunas', id_item, 'add');
		$("#ultima_seleccion").val("comuna;" + id_item);		
	    }
	    else
	    {
		limpiar_seleccion("comuna", id_item);
				
		concatenar_faceta('comunas', id_item, 'del');	    
	    }
	    $("#pagina_actual").val('1');
	    buscar_registros();
	}
    }
}

//Se realiza una nueva busqueda a partir del concepto especifico (versión modificada que no ncarga los dominios que no son facetas)
//debiera pasarle el nombre del registro

function nueva_busqueda(base_url, tipo_registro, id_registro, nombre_registro) {
    url = base_url + 'buscar/?';
    var nivel=0;
    
    if(tipo_registro=="perfil"){nivel=2} 
    if(tipo_registro=="dominio"){nivel=3} 
    if(tipo_registro=="comuna"){nivel=6}  

	$("#ultima_seleccion").val('');
	$('.tres_seleccionado').removeClass("tres_seleccionado");
	$('.dos_seleccionado').next().slideToggle();
	$('.dos_seleccionado').removeClass("dos_seleccionado");

	$('[id^=region_].uno.uno_seleccionado').next().slideToggle();
	$('[id^=region_].uno.uno_seleccionado').removeClass("uno_seleccionado");

	$('#ultima_seleccion').val('');
	$('#perfiles_dominio').val('');		
	$('#dominios_especificos').val('');
	$('#texto_libre').val('');
	$('#regiones').val('');
	$('#provincias').val('');
	$('#comunas').val('');
	$('#estado').val('');
	$('#descartar').val('');
	$('#pagina_actual').val('1');
	$('#cantidad_registros').val('10');
	$('#orden_seleccion_nombre').val('');

	//aca tomo solo la faceta, si no es dominio = faceta por esto falla

    if(tipo_registro=="dominio")
       	//console.log (url+"dominio_especifico="+nombre_registro);
        window.location.href = url+"dominio_especifico="+nombre_registro;
    else
    	return click_facetas(id_registro,nivel);	

    //debiera hacer un else para el caso en que el dminio no sea faceta, lo "redirija" (buscar termino relacionado?)

} 

//Se realiza una nueva busqueda a partir del concepto especifico (version original no carga la nueva url)
function nueva_busqueda_1(base_url, tipo_registro, id_registro) {
    url = base_url + 'buscar/';
    $.extend(
    {
      	redirectPost: function(location, args)
      	{
      	    var form = '';
      	    $.each( args, function( key, value ) {
      		    //value = value.split('"').join('\"');
      		    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
      	    });
      	    $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
      	}
    });

    $.redirectPost(url, {tipo_registro: tipo_registro, id_registro: id_registro});
}

//Se carga el detalle de un dominio especifico
function detalle_tag(id_item, tipo_item)
{
    if ($("#eliminar_tag").val() == '')
    {
	$(".tag_seleccionado").removeClass("tag_seleccionado");
	if (tipo_item == 'dominio') {	
	    $("#dv_tag_dominio_" + id_item).addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("dominio;" + id_item);
			
	    $.ajax({
		type: "POST",
		url: "/buscar-dominio-especifico-detalle/", 
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       id_dominio: id_item		   	    
		       },
		
		success: function (response) {	    
		    if (response != "ERROR")
		    {
			$("#dv_detalle_tag").html(response);		
		    }
		    else
		    {
			$("#dv_detalle_tag").html('');
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_detalle_tag").html('');
		} 
	    });
	}else if(tipo_item == 'perfil'){
	    $("#dv_tag_perfil_" + id_item).addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("perfil;" + id_item);
		
	    $.ajax({
		type: "POST",
		url: "/buscar-perfil-detalle/", 
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       id_perfil: id_item		   	    
		       },
		
		success: function (response) {	    
		    if (response != "ERROR")
		    {
			$("#dv_detalle_tag").html(response);		
		    }
		    else
		    {
			$("#dv_detalle_tag").html('');
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_detalle_tag").html('');
		} 
	    });	
	}
	else if (tipo_item == 'region'){	    
	    $("#dv_tag_region_" + id_item).addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("region;" + id_item);	    
	    
	    $.ajax({
		type: "POST",
		url: "/buscar-region-detalle/", 
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       id_region: id_item		   	    
		       },
		
		success: function (response) {	    
		    if (response != "ERROR")
		    {
			$("#dv_detalle_tag").html(response);
			$('#img_mapa').maphilight({groupBy: 'title', stroke: true, fill: false, fillColor: 'FFFFFF', strokeColor: '000000',strokeWidth: 2});	    		
		    }
		    else
		    {
			$("#dv_detalle_tag").html('');
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_detalle_tag").html('');
		} 
	    });
	}
	else if (tipo_item == 'provincia'){	    
	    $("#dv_tag_provincia_" + id_item).addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("provincia;" + id_item);	    
	    
	    $.ajax({
		type: "POST",
		url: "/buscar-provincia-detalle/", 
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       id_provincia: id_item		   	    
		       },
		
		success: function (response) {	    
		    if (response != "ERROR")
		    {
			$("#dv_detalle_tag").html(response);
			$('#img_mapa').maphilight({groupBy: 'title', stroke: true, fill: false, fillColor: 'FFFFFF', strokeColor: '000000',strokeWidth: 2});	    		
		    }
		    else
		    {
			$("#dv_detalle_tag").html('');
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_detalle_tag").html('');
		} 
	    });
	}
	else if (tipo_item == 'comuna'){	    
	    $("#dv_tag_comuna_" + id_item).addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("comuna;" + id_item);	    
	    
	    $.ajax({
		type: "POST",
		url: "/buscar-comuna-detalle/", 
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       id_comuna: id_item		   	    
		       },
		
		success: function (response) {	    
		    if (response != "ERROR")
		    {
			$("#dv_detalle_tag").html(response);
			$('#img_mapa').maphilight({groupBy: 'title', stroke: true, fill: false, fillColor: 'FFFFFF', strokeColor: '000000',strokeWidth: 2});	    		
		    }
		    else
		    {
			$("#dv_detalle_tag").html('');
		    }
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_detalle_tag").html('');
		} 
	    });
	}
	else if (tipo_item == 'texto'){	    
	    $("#dv_tag_texto").addClass("tag_seleccionado");
	    $("#ultima_seleccion").val("texto;" + $("#texto_libre").val());
	    $("#dv_detalle_tag").html('');
	}
    }
    else
    {
	$('#eliminar_tag').val('');
    }
}

//Se muestra todo el contenido de la descripcion y antecedentes
function dominio_especifico_detalle_ver(id_dominio, tipo)
{
    $.ajax({
	type: "POST",
	url: "/buscar-dominio-especifico-detalle/", 
	data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       id_dominio: id_dominio
	       },
	
	success: function (response) {	    
	    if (response != "ERROR")
	    {
		ver_mas = "ver m&aacute;s <span class='close'>&#9660;</span>";
		ver_menos = "ver menos <span class='close'>&#9650;</span>";
		nueva_llamada = 0;
		$( "#dv_ver").unbind( "click" );
		$( "#dv_pdf").unbind( "click" );
		if (tipo == 0) {
		    nueva_llamada = 1;
		    $("#dv_ver").html(ver_mas);
		    $("#de_texto").removeClass("perfil_descripcion_ver");
		    $("#de_texto").addClass("perfil_descripcion_inicio");
		}
		else
		{
		    nueva_llamada = 0;
		    $("#dv_ver").html(ver_menos);
		    $("#de_texto").removeClass("perfil_descripcion_inicio");
		    $("#de_texto").addClass("perfil_descripcion_ver");
		}
		$("#dv_ver").click(function(){ dominio_especifico_detalle_ver(id_dominio, nueva_llamada); });
		$("#dv_pdf").click(function(){ dominio_especifico_detalle_pdf(id_dominio, nueva_llamada); });
		
	    }	    
	},
	error: function (xhr, ajaxOptions, thrownError) {	    
	    $("#dv_resultados").html(aviso);
	    $('#dv_aviso_espera').dialog('close');
	} 
    });    
}

//Se muestra todo el contenido de la descripcion y antecedentes
function dominio_especifico_detalle_pdf(id_dominio)
{
    location.href = '/dominio-especifico-pdf/' + id_dominio;
}

//Se muestra todo el contenido de la descripcion y antecedentes
function perfil_detalle_ver(id_perfil, tipo)
{
    $.ajax({
	type: "POST",
	url: "/buscar-perfil-dominio-detalle/", 
	data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       id_perfil: id_perfil
	       },
	
	success: function (response) {	    
	    if (response != "ERROR")
	    {
		ver_mas = "ver m&aacute;s <span class='close'>&#9660;</span>";
		ver_menos = "ver menos <span class='close'>&#9650;</span>";
		nueva_llamada = 0;
		$( "#dv_ver").unbind( "click" );
		$( "#dv_pdf").unbind( "click" );
		if (tipo == 0) {
		    nueva_llamada = 1;
		    $("#dv_ver").html(ver_mas);
		    $("#de_texto").removeClass("perfil_descripcion_ver");
		    $("#de_texto").addClass("perfil_descripcion_inicio");
		}
		else
		{
		    nueva_llamada = 0;
		    $("#dv_ver").html(ver_menos);
		    $("#de_texto").removeClass("perfil_descripcion_inicio");
		    $("#de_texto").addClass("perfil_descripcion_ver");
		}
		$("#dv_ver").click(function(){ perfil_detalle_ver(id_perfil, nueva_llamada); });
		$("#dv_pdf").click(function(){ perfil_detalle_pdf(id_perfil, nueva_llamada); });
		
	    }	    
	},
	error: function (xhr, ajaxOptions, thrownError) {	    
	    $("#dv_resultados").html(aviso);
	    $('#dv_aviso_espera').dialog('close');
	} 
    });    
}

//Se muestra todo el contenido de la descripcion y antecedentes
function perfil_detalle_pdf(id_dominio)
{
    location.href = '/perfil-dominio-pdf/' + id_dominio;
}

//Se cargan los dominios especificos
function cargar_dominios_especificos(id_perfil)
{
    $.ajax({
	type: "POST",
	url: "/buscar-dominio-especifico/", 
	data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       id_perfil: id_perfil,
	       texto: $("#texto_libre").val(),
	       dominios_especificos: $("#dominios_especificos").val(),
	       comunas: $("#comunas").val(),
	       },
	
	success: function (response) {
	    if (response != "ERROR")
	    {		
		$("#dv_dominios_" + id_perfil).html(response);		
	    }
	    else
	    {
		$("#dv_resultados").html(aviso);
		$('#dv_aviso_espera').dialog('close');		
	    }
	},
	error: function (xhr, ajaxOptions, thrownError) {	    
	    $("#dv_resultados").html(aviso);
	    $('#dv_aviso_espera').dialog('close');
	} 
    });
}

//Se realiza la busqueda de acuerdo a los datos ingresados
function buscar_registros()
{
    url = "/buscar/?";
    
    if ($.trim($("#texto_libre").val()) != '')
    {
	url += "texto=" + $("#texto_libre").val();
    }
    
    perfiles_dominio = $.trim($("#perfiles_dominio").val());    
    add_url = '';    
    if (perfiles_dominio != '')
    {
	lista = perfiles_dominio.split(';');
	add_url = '&perfil_dominio=';
	for(i=0;i < lista.length;i++)
	{
		if (lista[i] != '')
		{
			add_url += $("[id^=perfil_" + lista[i] + "_]").html() + ",";
		}
	}
	url += add_url;
    }    
    
    dominios_especificos = $.trim($("#dominios_especificos").val());
    if (dominios_especificos != '')
    {
	lista = dominios_especificos.split(';');
	add_url = '&dominio_especifico=';
	for(i=0;i < lista.length;i++)
	{
		if (lista[i] != '')
		{ 
			add_url += $("[id^=dominio_" + lista[i] + "_]").html() + ",";
		}
	}
	url += add_url;
    }
        
    regiones = $.trim($("#regiones").val());
    if (regiones != '')
    {
	lista = regiones.split(';');
	add_url = '&region=';
	for(i=0;i < lista.length;i++)
	{
		if (lista[i] != '')
		{ 
			add_url += $("#region_" + lista[i]).html() + ",";
		}
	}
	url += add_url;
    }
    
    provincias = $.trim($("#provincias").val());
    if (provincias != '')
    {
	lista = provincias.split(';');
	add_url = '&provincia=';
	for(i=0;i < lista.length;i++)
	{
		if (lista[i] != '')
		{ 
			add_url += $("[id^=provincia_" + lista[i] + "_]").html() + ",";
		}
	}
	url += add_url;
    }
    
    comunas = $.trim($("#comunas").val());
    if (comunas != '')
    {
	lista = comunas.split(';');
	add_url = '&comuna=';
	for(i=0;i < lista.length;i++)
	{
		if (lista[i] != '')
		{ 
			add_url += $("[id^=comuna_" + lista[i] + "_]").html() + ",";
		}
	}
	url += add_url;
    }
    
    $.extend(
    {
      	redirectPost: function(location, args)
      	{
      	   var form = '';
      	   $.each( args, function( key, value ) {
      		   //value = value.split('"').join('\"');
      		   form += '<input type="hidden" name="'+key+'" value="'+value+'">';
      	   });
      	   $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
      	}
    });

    $.redirectPost(url,
	      {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       //texto: $("#texto_libre").val(),
	       //perfiles_dominio: $("#perfiles_dominio").val(),
	       //dominios_especificos: $("#dominios_especificos").val(),
	       //regiones: $("#regiones").val(),
	       //provincias: $("#provincias").val(),
	       //comunas: $("#comunas").val(),
	       ultima_seleccion: $("#ultima_seleccion").val(),
	       orden_seleccion: $("#orden_seleccion").val(),
	       pagina_actual : $("#pagina_actual").val(),
	       cantidad_registros : $("#cantidad_registros").val()		
	});
    
    /*
    $.ajax({
	type: "POST",
	url: "/buscar-resultado/",
	async: true,
	data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
	       texto: $("#texto_libre").val(),
	       perfiles_dominio: $("#perfiles_dominio").val(),
	       dominios_especificos: $("#dominios_especificos").val(),
	       regiones: $("#regiones").val(),
	       provincias: $("#provincias").val(),
	       comunas: $("#comunas").val(),
	       ultima_seleccion: $("#ultima_seleccion").val(),
	       orden_seleccion: $("#orden_seleccion").val(),
	       pagina_actual : $("#pagina_actual").val(),
	       cantidad_registros : $("#cantidad_registros").val()	       
	       },
	
	success: function (response) {
	    if (response != "ERROR"){
	    	$("#dv_resultados").html(response);
			$('#img_mapa').maphilight({groupBy: 'title', stroke: true, fill: false, fillColor: 'FFFFFF', strokeColor: '000000',strokeWidth: 2});
			setTimeout(ReemplazarImagenesRotas, 3000);			
	    }else{
			$("#dv_resultados").html(aviso);			
	    }

	    $("#estado").val('');
	    scroll_resultados($("#ultima_seleccion").val());
	   
	},
	error: function (xhr, ajaxOptions, thrownError) {	    
	    $("#dv_resultados").html(aviso);
	    $("#estado").val('');
	    scroll_resultados($("#ultima_seleccion").val());
	} 
    });
    */
}

//Se realiza la busqueda de registros
function realizar_busqueda()
{
	img = "<img src='media/img/loading.gif' />";
	$('#dv_resultados').html(img);
	
	aviso = "<div id='dv_numero_resultados' class='tx'> >> 0 Resultados</div>";
	
	$.ajax({
		type: "POST",
		url: "/buscar-resultado/",
		async: true,
		data: {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		       texto: $("#texto_libre").val(),
		       perfiles_dominio: $("#perfiles_dominio").val(),
		       dominios_especificos: $("#dominios_especificos").val(),
		       regiones: $("#regiones").val(),
		       provincias: $("#provincias").val(),
		       comunas: $("#comunas").val(),
		       ultima_seleccion: $("#ultima_seleccion").val(),
		       orden_seleccion: $("#orden_seleccion").val(),
		       pagina_actual : $("#pagina_actual").val(),
		       cantidad_registros : $("#cantidad_registros").val()	       
		       },
		success: function (response) {
		    if (response != "ERROR"){
			$("#dv_resultados").html(response);
				$('#img_mapa').maphilight({groupBy: 'title', stroke: true, fill: false, fillColor: 'FFFFFF', strokeColor: '000000',strokeWidth: 2});
				setTimeout(ReemplazarImagenesRotas, 3000);			
		    }else{
				$("#dv_resultados").html(aviso);			
		    }
	
		    $("#estado").val('');
		    scroll_resultados($("#ultima_seleccion").val());
		   
		},
		error: function (xhr, ajaxOptions, thrownError) {	    
		    $("#dv_resultados").html(aviso);
		    $("#estado").val('');
		    scroll_resultados($("#ultima_seleccion").val());
		} 
	});	
}

//Se realiza el cambio de pagina en la Paginacion
function cambiar_pagina(pagina_actual, cantidad_registros)
{
	if (pagina_actual==0){pagina_actual=1;}
	cantidad_registros = $("#cb_registros").val();

	//debiera tratar a ambos como pag y reg virtualmente como un parametros
	var url= location.href;
	
	if (url.indexOf("registros") >= 0)
	{
		//console.log("ya esta");
		if (url.lastIndexOf('&') != -1) 
		{
		   url = url.substr(0, url.lastIndexOf('&')); //elimino registros
			//console.log(url);
		   str = url.substr(0, url.lastIndexOf('&')); //elimino pag
			//console.log(str);
		   str= str+'&pag=' + pagina_actual+'&registros='+cantidad_registros;//ingreso datos nuevos a la URL
		}
	}
	else{
		//console.log("no esta");
		str= location.href+'&pag=' + pagina_actual+'&registros='+cantidad_registros;
	}


    $("#pagina_actual").val(pagina_actual);
    $("#cantidad_registros").val(cantidad_registros);
    $("#cb_pagina").val(pagina_actual);
	$("#cb_registros").val(cantidad_registros);


    window.location.href = str;    
}

//Se realiza el cambio de pagina en la Paginacion
function cambiar_pagina_1(pagina_actual, cantidad_registros)
{

    $("#pagina_actual").val(pagina_actual);
    $("#cantidad_registros").val(cantidad_registros);
    //location.href = +'&pag=' + pagina_actual;

    buscar_registros();  
      
}

/*
//Deprecated method
//Detalle ficha
function ver_ficha(base_url, ficha)
{
    url = base_url + ficha;
    $.extend(
    {
      	redirectPost: function(location, args)
      	{
      	   var form = '';
      	   $.each( args, function( key, value ) {
      		   //value = value.split('"').join('\"');
      		   form += '<input type="hidden" name="'+key+'" value="'+value+'">';
      	   });
      	   $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
      	}
    });

    $.redirectPost(url, {csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
		  volver: true
	});
}
*/

function recargar_busqueda_loc(){
	var region;

    $("p[id^='region_']").each(function(data){    
		id = $(this).attr("id");
		comunas =  $("#"+ id +" + div > .dos + div > .tres_seleccionado");
		if(comunas.length){   
	        data = id.split("_");
	        region = data;
	        $("#region_" + data[1]).addClass("uno_seleccionado");        
	        $("#dv_provincias_" + data[1]).slideToggle();
	    }        
    });
        
    $("#dv_provincias_" + region[1]).each(function(data){
    	id = $(this).attr("id");
        comunas =  $("#" + id + " > div > .tres_seleccionado");
        if(comunas.length){
        	data = comunas.parent().attr("id").split("_");
            $("#provincia_"+ data[2]).addClass("dos_seleccionado");
            $("#dv_comunas_" + data[2]).slideToggle();
        }            
    });
}

//Se cargan las facetas de acuerdo a los items seleccionados
function cargar_facetas()
{
    $(".uno_seleccionado").each(function(index){
	id = $(this).attr("id");
	
	if (id.indexOf('region_') >= 0)
	{
	    data = id.split('_');
	    if (!$('#dv_provincias_' + data[1]).is(':visible')){
		$('#dv_provincias_' + data[1]).slideToggle();
	    }
	}
	else if (id.indexOf("categoria_") >= 0) {
	    data = id.split('_');
	    if (!$('#dv_perfiles_' + data[1]).is(':visible')){
		$('#dv_perfiles_' + data[1]).slideToggle();
	    }
	}
    });
    
    $(".dos_seleccionado").each(function(index){
	id = $(this).attr("id");
	
	if (id.indexOf('provincia_') >= 0)
	{
	    data = id.split('_');
	    if (!$("#region_" + data[2]).hasClass("uno_seleccionado")) {
		$("#region_" + data[2]).addClass("uno_seleccionado");
	    }
	    
	    if (!$('#dv_provincias_' + data[2]).is(':visible')){
		$('#dv_provincias_' + data[2]).slideToggle();
	    }
	    
	    if (!$('#dv_comunas_' + data[1]).is(':visible')){
		$('#dv_comunas_' + data[1]).slideToggle();
	    }
	}
	else if (id.indexOf("perfil_") >= 0) {
	    data = id.split('_');
	    if (!$("#categoria_" + data[2]).hasClass("uno_seleccionado"))
	    {
		$("#categoria_" + data[2]).addClass("uno uno_seleccionado");
	    }
	    
	    if (!$('#dv_dominios_' + data[1]).is(':visible'))
	    {
		$('#dv_dominios_' + data[1]).slideToggle();
	    }
	}
    });
    
    $(".tres_seleccionado").each(function(index){
	id = $(this).attr("id");
	
	if (id.indexOf('comuna_') >= 0)
	{
	    data = id.split('_');
	    if (!$("#region_" + data[3]).hasClass("uno_seleccionado")) {
		$("#region_" + data[3]).addClass("uno_seleccionado")
	    }
	    
	    if (!$('#dv_provincias_' + data[3]).is(':visible')){
		$('#dv_provincias_' + data[3]).slideToggle();
	    }
	    
	    if (!$("#provincia_" + data[2] + "_" + data[3]).hasClass("dos_seleccionado")) {
		$("#provincia_"  + data[2] + "_" + data[3]).addClass("dos_seleccionado")
	    }
	    
	    if (!$('#dv_comunas_' + data[2]).is(':visible')){
		$('#dv_comunas_' + data[2]).slideToggle();
	    }
	}
	else if (id.indexOf("dominio_") >= 0) {
	    data = id.split('_');
	    	    
	    if (!$("#perfil_" + data[2]).hasClass("dos_seleccionado")) {
		$("#perfil_"  + data[2]).addClass("dos_seleccionado")
	    }
	    
	    if (!$('#dv_dominios_' + data[2]).is(':visible')){
		$('#dv_dominios_' + data[2]).slideToggle();
	    }	    	    	    
	}
    });
    
    //buscar_registros();
}