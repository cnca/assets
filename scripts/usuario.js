var style_exito = 'width:405px;';
var style_error = 'border: solid 1px #f00; width:405px;';

//Se realiza la validacion del login
function validar_login()
{
    resultado = true;
    $('#username').addClass('js-username-field email-input').removeClass('signup_error');
    $('#password').addClass('js-password-field').removeClass('signup_error');
    
    if ($.trim($('#username').val()) == "")
    {
        $('#username').removeClass().addClass('signup_error');
        $('#username').attr('title', 'Correo electrónico es obligatorio.');            
        $('#username').tooltip();
        resultado = false;
    }

    if ($.trim($('#password').val()) == "")
    {
        $('#password').removeClass().addClass('signup_error');
        $('#password').attr('title', 'Debe ingresar la contraseña.'); 
        $('#password').tooltip();
        resultado = false;
    }
    
    if (resultado){        
        $.ajax({
            type: "POST",
            url: "/sigpa/login/",
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    username: $('#username').val(),
                    password: $('#password').val()
                    },
            success : function(respuesta){   
                obj = JSON.parse(respuesta);
               
                if (obj.estado== "OK"){
                    mostrar_mensaje_aviso(obj.mensaje, 0, '60%', '25%', '', '');
                    setTimeout("window.location = '" + obj.success_url + "';", 4000);                
                }
                else{                
                    mostrar_mensaje_error(obj.mensaje, 3000, '500px', '180px');                    
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status  + " " + xhr.responseText);
                alert(thrownError);
                }
            });
    }    
}

//Se abre la ventana para recuperar contraseña
function olvide_contrasena(_base_url) {
 $.fancybox({
        'href' : _base_url + "sigpa/olvide-password",
         'width' : '70%',
         'height' : '50%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }          
     });   
}

//Se realiza el proceso de recuperacion de contrasena
function recuperar_contrasena()
{
    resultado = true;
    $('#email').attr('style', 'border: solid 1px #d3d3d3;');            

    if (($.trim($('#email').val()) == "") || (validar_email($('#email').val()) == false))
    {        
        $('#email').attr('style', 'border: solid 1px #f00;');            
        $('#email').attr('title', 'Correo electrónico es obligatorio.');            
        $('#email').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $("#frm_recuperar").submit();
    }    
}

//Se cierra la ventana de Recuperar contrasena
function cerrar_recuperar_contrasena()
{
    parent.$.fancybox.close();
}

//Se realiza la actualizacion de la contraseña
function actualizar_contrasena()
{
    resultado = true;
    $('#new_password1').attr('style', 'border: solid 1px #d3d3d3;');            
    $('#new_password2').attr('style', 'border: solid 1px #d3d3d3;');            

    if (($.trim($('#new_password1').val()) == "") || ($("#new_password1").val().length < 5))
    {        
        $('#new_password1').attr('style', 'border: solid 1px #f00;');            
        $('#new_password1').attr('title', 'Debe ingresar una contraseña, mínimo 5 caracteres.');            
        $('#new_password1').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#new_password2').val()) == "") || ($("#new_password2").val().length < 5))
    {        
        $('#new_password2').attr('style', 'border: solid 1px #f00;');            
        $('#new_password2').attr('title', 'Debe ingresar una contraseña, mínimo 5 caracteres.');            
        $('#new_password2').tooltip();
        resultado = false;
    }
    
    if ((resultado == true) && ($('#new_password1').val() != $("#new_password2").val()))
    {        
        $('#new_password1').attr('style', 'border: solid 1px #f00;');            
        $('#new_password1').attr('title', 'Las contraseñas no coinciden');            
        $('#new_password1').tooltip();
        $('#new_password2').attr('style', 'border: solid 1px #f00;');            
        $('#new_password2').attr('title', 'Las contraseñas no coinciden');            
        $('#new_password2').tooltip();

        resultado = false;
    }
    
    if (resultado)
    {
        $("#frm_actualizar_contrasena").submit();
    }    
}

//Se envia al usuario al home del sitio
function recuperar_contrasena_home()
{
    document.location.href="/";
}

//Se abre la vantana para seleccionar el avatar
function seleccionar_avatar()
{
    $('#fl_avatar').click();
}

//Se realiza la previsualiz
//Se realiza la validacion del formulario de registro
function validar_formulario_registro()
{
    resultado = true;
    $('#registrese').attr('style', '');
    $('#nombres').attr('style', '');
    $('#apellidos').attr('style', '');
    $('#email').attr('style', '');
    $('#pass1').attr('style', '');
    $('#pass2').attr('style', '');
    //$('#dv_avatar').attr('style', '');
    $('#chk_acepto').attr('style', '');
    
    if ($.trim($('#nombres').val()) == "")
    {
        $('#nombres').attr('style', 'border: 1px solid red;');
        $('#nombres').attr('title', 'Debe ingresar sus Nombres.');            
        $('#nombres').tooltip();
        resultado = false;
    }

    if ($.trim($('#apellidos').val()) == "")
    {
        $('#apellidos').attr('style', 'border: 1px solid red;');
        $('#apellidos').attr('title', 'Debe ingresar sus Apellidos.'); 
        $('#apellidos').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#email').val()) == "") || (validar_email($.trim($('#email').val())) == false))
    {
        $('#email').attr('style', 'border: 1px solid red;');
        $('#email').attr('title', 'Debe ingresar su Correo electrónico, revise el formato.'); 
        $('#email').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass1').val()) == "")
    {
        $('#pass1').attr('style', 'border: 1px solid red;');
        $('#pass1').attr('title', 'Debe ingresar su contraseña.'); 
        $('#pass1').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass2').val()) == "")
    {
        $('#pass2').attr('style', 'border: 1px solid red;');
        $('#pass2').attr('title', 'Debe re-ingresar la contraseña.'); 
        $('#pass2').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#pass1').val()) != $.trim($('#pass2').val()))
    {
        $('#pass1, #pass2').attr('style', 'border: 1px solid red;');
        $('#pass1, #pass2').attr('title', 'Las contraseñas no coinciden.'); 
        $('#pass1, #pass2').tooltip();
        resultado = false;
    }
        
    //22-05-2015 Agustin pidio quitar la imagen como requisito
/*    if ($('#fl_avatar').val() == "")
    {    
        $('#dv_avatar').attr('style', 'border: 1px solid red;');
        $('#dv_avatar').attr('title', 'Debe seleccionar su imagen.'); 
        $('#dv_avatar').tooltip();
        resultado = false;
    }*/
    
    
    if (!$('#acepto').is(':checked'))
    {
        $('#chk_acepto').attr('style', 'border: 1px solid red;');
        $('#chk_acepto').attr('title', 'Debe Aceptar las condiciones.'); 
        $('#chk_acepto').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $('form#frm_usuario').submit();
    }   
}

//Se abre la ventana para agregar un cultor individual
function abrir_formulario(tipo_registro)
{
    url = '';
    if (tipo_registro == "cultor_individual")
    {
        url = '/sigpa/mi-cuenta/cultor-individual/agregar/';
    }
    else if (tipo_registro == "cultor_colectivo")
    {
        url = '/sigpa/mi-cuenta/cultor-colectivo/agregar/';
    }
    else if (tipo_registro == "fiestas")
    {
        url = '/sigpa/mi-cuenta/fiestas/agregar/';
    }
    else if (tipo_registro == "gastronomia")
    {
        url = '/sigpa/mi-cuenta/gastronomia/agregar/';
    }
    
    $.fancybox({
        'href' : url,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }        
     });
}

//Se realiza la validación de los datos de una Imagen
function validar_imagen()
{
    resultado = true;
    $('#galeriaTitulo').removeClass('requires');
    $('#galeriaAutor').removeClass('requires');
    //$('#galeriaDescripcion').removeClass('requires');
    //$('#galeriaAnio').removeClass('requires');
    
    if ($.trim($('#galeriaTitulo').val()) == "")
    {
        $('#galeriaTitulo').addClass('requires');
        $('#galeriaTitulo').attr('title', 'Debe ingresar el Titulo');
        $('#galeriaTitulo').tooltip();
        resultado = false;
    }

    if ($.trim($('#galeriaAutor').val()) == "")
    {
        $('#galeriaAutor').addClass('requires');
        $('#galeriaAutor').attr('title', 'Debe inasddgresar el Autor');
        $('#galeriaAutor').tooltip();
        resultado = false;
    }

    if (isNaN($('#galeriaAnio').val()))
    {
        $('#galeriaAnio').addClass('requires');
        $('#galeriaAnio').attr('title', 'El campo puede estar vacio, o solo lleno con un valor numérico');
        $('#galeriaAnio').tooltip();

        resultado = false;
    }
    return resultado;
}

//Se agrega un nuevo boton para agregar una nueva imagen
function nueva_imagen()
{
    id_imagen = parseInt($("#total_imagenes").val());
    
    titulo = $("#galeriaTitulo").val();
    descripcion = $("#galeriaDescripcion").val();
    autor = $("#galeriaAutor").val();
    anio = $("#galeriaAnio").val();
    
    imagen = titulo + "#@#" + descripcion + "#@#" + autor + "#@#" + anio;
    $("#galeriaTitulo").val("");
    $("#galeriaDescripcion").val("");
    $("#galeriaAutor").val("");
    $("#galeriaAnio").val("");
    $("#hf_img_" + id_imagen).val(imagen);
    
    $("#galeriaImagenes").append("<li id='li_img_" + id_imagen +"' onclick='quitar_imagen(" + id_imagen + ");'></li>");

    obj_img = $("<img id='img_" + id_imagen + "' src='' alt='' />");
    obj_img.appendTo("#li_img_" + id_imagen);
    
    id_imagen += 1;
    
    html = "";            
    html += "<div id='dv_img_" + id_imagen + "'>"
    html += "<input type='hidden' id='hf_img_" + id_imagen + "' name='hf_img_";
    html += id_imagen + "' value=''>";
        
    html += "<input id='subirImagen_" + id_imagen + "' name='subirImagen_" + id_imagen + "' class='uploadImage'";
    html += " type='file' onclick='validar_imagen()' accept='image/*'>";
    
    html += "</div>";
    
    $("#dv_imagen").append(html);
    
    $('#subirImagen_' + id_imagen).click(function(e){
            if (!validar_imagen())
            {
                e.preventDefault();
            }
    });

    $("#subirImagen_" + id_imagen).change(function(){
            nueva_imagen(id_imagen);
            preview_imagen(this, 'img_' + id_imagen);
            //$("#img_" + id_img).attr('alt', $('#galeriaTitulo').val());
            $("#dv_img_" + (id_imagen - 1)).hide();                                
    });
    
    $("#total_imagenes").val(id_imagen);
}

//Se realiza la eliminacion de los elementos de una imagen
function quitar_imagen(id_imagen)
{
    $("#img_" + id_imagen).remove();
    $("#hf_img_" + id_imagen).remove();
    $("#uploadImage_" + id_imagen).remove();
    $("#li_img_" + id_imagen).remove();
}

//Se abre el formulario para confirmar eliminacion de registro
function confirmar_eliminacion(tipo_registro, id_registro)
{
    url = '/sigpa/pantalla_confirmacion/' + tipo_registro + '/' + id_registro;
    $.fancybox({
        'href' : url,
         'width' : '550',
         'height' : '100',
         'fitToView' : false,
         'autoSize' : false,
         'closeBtn' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });
}

//Se realiza la aliminacion de un registro
function eliminar_registro(tipo_registro, id_registro)
{
    url_eliminar = '/sigpa/eliminar_registro/' + tipo_registro + '/' + id_registro;
    
    $.ajax({
        type: "POST",
        url: url_eliminar,
        data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value },
        success : function(respuesta)
        {            
            if (respuesta == "OK")
            {
                parent.window.location.reload(); 
            }
            else
            {
                $('#lbl_mensaje').html(respuesta);
            }
        }/*,
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status  + " " + xhr.responseText);
                
        }*/
    });    
}

//Se realiza la edicion de un registro
function editar_registro(tipo_registro, id_registro)
{
    if (tipo_registro == 'cultor_individual')
    {
        tipo_registro = 'cultor-individual';
    }
    else if (tipo_registro == 'cultor_colectivo')
    {
        tipo_registro = 'cultor-colectivo';
    }

    
    url_editar = '/sigpa/mi-cuenta/' + tipo_registro + '/editar/' + id_registro;
    $.fancybox({
        'href' : url_editar,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });  
}

//Se abre la ventana para sugerir revision
function sugerir_revision_ficha(_base_url, tipo_ficha, id_ficha) {
    $.fancybox({
        'href' : _base_url + "sigpa/sugerir-revision/" + tipo_ficha + "/" + id_ficha,
         'width' : '70%',
         'height' : '95%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',         
        helpers : { 
          overlay : {closeClick: false}
        }          
     });   
}

//Se reasliza el registro de una sugerencia de revision
function registrar_sugerencia_revision(_base_url, tipo_ficha, id_ficha)
{
    url_registrar = _base_url + "sigpa/sugerir-revision/" + tipo_ficha + "/" + id_ficha;
    
    resultado = true;
    $('#nombre_usuario').attr('style', '');
    $('#email_usuario').attr('style', '');
    $('#motivo').attr('style', '');
    $('#comentario').attr('style', '');
    
    if ($.trim($('#nombre_usuario').val()) == "")
    {
        $('#nombre_usuario').attr('style', 'border: 1px solid red;');
        $('#nombre_usuario').attr('title', 'Debe ingresar su Nombre.');            
        $('#nombre_usuario').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#email_usuario').val()) == "") || (validar_email($("#email_usuario").val()) == false))
    {
        $('#email_usuario').attr('style', 'border: 1px solid red;');
        $('#email_usuario').attr('title', 'Debe ingresar su Email.');            
        $('#email_usuario').tooltip();
        resultado = false;
    }
    
    if ($('#motivo').val() == "-1")
    {
        $('#motivo').attr('style', 'border: 1px solid red;');
        $('#motivo').attr('title', 'Debe seleccionar un Motivo de sugerencia.');            
        $('#motivo').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#comentario').val()) == "")
    {
        $('#comentario').attr('style', 'border: 1px solid red;');
        $('#comentario').attr('title', 'Debe ingresar el comentario.');            
        $('#comentario').tooltip();
        resultado = false;
    }
    

    if (resultado)
    {
        $.ajax({
            type: "POST",
            url: url_registrar,
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    'nombre_usuario' : $("#nombre_usuario").val(),
                    'email_usuario' : $("#email_usuario").val(),
                    'motivo' : $("#motivo").val(),
                    'comentario' : $("#comentario").val(),
                    'ficha' : $("#ficha").val(),
                    'nombre_ficha' : $("#nombre_ficha").val()},
            success : function(respuesta)
            {            
                if (respuesta == "OK")
                {
                    mostrar_mensaje_exito('usuario-mensaje_exito_sugerencia', 3000, '100%', '25%', '', 'yes');                     
                }
                else
                {
                    $('#lbl_mensaje').html(respuesta);
                }
            }
        });
        
    }
}

//Se abre la ventana para sugerir revision comuna
function sugerir_revision_ficha_comuna(_base_url, id_comuna) {
    $.fancybox({
        'href' : _base_url + "sigpa/sugerir-revision-comuna/" + id_comuna,
         'width' : '70%',
         'height' : '95%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',         
        helpers : { 
          overlay : {closeClick: false}
        }          
     });   
}


//Se reasliza el registro de una sugerencia de revision
function registrar_sugerencia_revision_comuna(_base_url, id_comuna)
{
    url_registrar = _base_url + "sigpa/sugerir-revision-comuna/" + id_comuna,
    resultado = true;
    $('#nombre_usuario').attr('style', '');
    $('#email_usuario').attr('style', '');
    $('#motivo').attr('style', '');
    $('#comentario').attr('style', '');
    
    if ($.trim($('#nombre_usuario').val()) == "")
    {
        $('#nombre_usuario').attr('style', 'border: 1px solid red;');
        $('#nombre_usuario').attr('title', 'Debe ingresar su Nombre.');            
        $('#nombre_usuario').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#email_usuario').val()) == "") || (validar_email($("#email_usuario").val()) == false))
    {
        $('#email_usuario').attr('style', 'border: 1px solid red;');
        $('#email_usuario').attr('title', 'Debe ingresar su Email.');            
        $('#email_usuario').tooltip();
        resultado = false;
    }
    
    if ($('#motivo').val() == "-1")
    {
        $('#motivo').attr('style', 'border: 1px solid red;');
        $('#motivo').attr('title', 'Debe seleccionar un Motivo de sugerencia.');            
        $('#motivo').tooltip();
        resultado = false;
    }
    
    if ($.trim($('#comentario').val()) == "")
    {
        $('#comentario').attr('style', 'border: 1px solid red;');
        $('#comentario').attr('title', 'Debe ingresar el comentario.');            
        $('#comentario').tooltip();
        resultado = false;
    }
  
    if (resultado)
    {
        respuesta="OK";

        $.ajax({
            type: "POST",
            url: url_registrar,
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    'nombre_usuario' : $("#nombre_usuario").val(),
                    'email_usuario' : $("#email_usuario").val(),
                    'motivo' : $("#motivo").val(),
                    'comentario' : $("#comentario").val(),
                    'comuna' : $("#id_comuna").val()
                },
            success : function(respuesta)
            {            

                if (respuesta == "OK")
                {
                    mostrar_mensaje_exito('usuario-mensaje_exito_sugerencia', 3000, '100%', '25%', '', 'yes');                     
                }
                else
                {
                    $('#lbl_mensaje').html(respuesta);
                }
            }
        });
        
    }
}


//Se abre la ventana para Editar Perfil
function ver_editar_perfil(_base_url, id_usuario)
{
    url_perfil = _base_url + "sigpa/editar-perfil/" + id_usuario;
    $.fancybox({
        'href' : url_perfil,
         'width' : '90%',
         'height' : '115%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'scrolling' : 'yes',
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',         
     });      
}

//Se realiza la validacion del formulario de registro
function validar_edicion_perfil()
{
    resultado = true;    
    $('#first_name').attr('style', '');
    $('#last_name').attr('style', '');
    $('#email').attr('style', '');
    $('#pass1').attr('style', '');
    $('#pass2').attr('style', '');
    $('#organizacion').attr('style', '');
    $('#sitio_web').attr('style', '');
    $('#twitter').attr('style', '');
    $('#facebook').attr('style', '');
    $('#youtube').attr('style', '');
    $('#vimeo').attr('style', '');
    $('#red_otra_1').attr('style', '');
    $('#red_otra_2').attr('style', '');
    
    
    if ($.trim($('#first_name').val()) == "")
    {
        $('#first_name').attr('style', 'border: 1px solid red;');
        $('#first_name').attr('title', 'Debe ingresar sus Nombres.');            
        $('#first_name').tooltip();
        resultado = false;
    }

    if ($.trim($('#last_name').val()) == "")
    {
        $('#last_name').attr('style', 'border: 1px solid red;');
        $('#last_name').attr('title', 'Debe ingresar sus Apellidos.'); 
        $('#last_name').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#email').val()) == "") || (validar_email($.trim($('#email').val())) == false))
    {
        $('#email').attr('style', 'border: 1px solid red;');
        $('#email').attr('title', 'Debe ingresar su Correo electrónico, revise el formato.'); 
        $('#email').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#pass1').val()) != "") || ($.trim($('#pass2').val()) != ""))
    {
        if ($.trim($('#pass1').val()) == "")
        {
            $('#pass1').attr('style', 'border: 1px solid red;');
            $('#pass1').attr('title', 'Debe ingresar su contraseña.'); 
            $('#pass1').tooltip();
            resultado = false;
        }
        
        if ($.trim($('#pass2').val()) == "")
        {
            $('#pass2').attr('style', 'border: 1px solid red;');
            $('#pass2').attr('title', 'Debe re-ingresar la contraseña.'); 
            $('#pass2').tooltip();
            resultado = false;
        }
        
        if ($.trim($('#pass1').val()) != $.trim($('#pass2').val()))
        {
            $('#pass1, #pass2').attr('style', 'border: 1px solid red;');
            $('#pass1, #pass2').attr('title', 'Las contraseñas no coinciden.'); 
            $('#pass1, #pass2').tooltip();
            resultado = false;
        }
    }
    
    if ($.trim($('#organizacion').val()) == "")
    {
        $('#organizacion').attr('style', 'border: 1px solid red;');
        $('#organizacion').attr('title', 'Debe ingresar la Organización a la cual pertenece.'); 
        $('#organizacion').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#sitio_web').val()) != "") && (validar_url_simple($('#sitio_web').val()) == false))
    {
        $('#sitio_web').attr('style', 'border: 1px solid red;');
        $('#sitio_web').attr('title', 'Dirección sitio web inválido.'); 
        $('#sitio_web').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#twitter').val()) != "") && (validar_url_simple($('#twitter').val()) == false))
    {
        $('#twitter').attr('style', 'border: 1px solid red;');
        $('#twitter').attr('title', 'Cuenta Twitter inválida. Ej. http://www.twitter.com/cnca'); 
        $('#twitter').tooltip();
        resultado = false;
    }

    if (($.trim($('#facebook').val()) != "") && (validar_url_simple($('#facebook').val()) == false))
    {
        $('#facebook').attr('style', 'border: 1px solid red;');
        $('#facebook').attr('title', 'Cuenta Facebook inválida. Ej. http://www.facebook.com/cnca'); 
        $('#facebook').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#vimeo').val()) != "") && (validar_url_simple($('#vimeo').val()) == false))
    {
        $('#vimeo').attr('style', 'border: 1px solid red;');
        $('#vimeo').attr('title', 'Cuenta Vimeo inválida. Ej. http://www.vimeo.com/cnca'); 
        $('#vimeo').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#youtube').val()) != "") && (validar_url_simple($('#youtube').val()) == false))
    {
        $('#youtube').attr('style', 'border: 1px solid red;');
        $('#youtube').attr('title', 'Cuenta Youtube inválida. Ej. http://www.youtube.com/cnca'); 
        $('#youtube').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#red_otra_1').val()) != "") && (validar_url_simple($('#red_otra_1').val()) == false))
    {
        $('#red_otra_1').attr('style', 'border: 1px solid red;');
        $('#red_otra_1').attr('title', 'Redes Sociales/Otra inválida. Ej. http://www.redsocial.com/cnca'); 
        $('#red_otra_1').tooltip();
        resultado = false;
    }
    
    if (($.trim($('#red_otra_2').val()) != "") && (validar_url_simple($('#red_otra_2').val()) == false))
    {
        $('#red_otra_2').attr('style', 'border: 1px solid red;');
        $('#red_otra_2').attr('title', 'Redes Sociales/Otra inválida. Ej. http://www.redsocial.com/cnca'); 
        $('#red_otra_2').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $('form#frm_usuario').submit();
    }   
}

//Se abre la ventana para enviar mensaje a usuario
function enviar_mensaje(_base_url, id_usuario)
{
    url = _base_url + "sigpa/enviar-mensaje/" + id_usuario;
    
    $.fancybox({
        'href' : url,
         'width' : '65%',
         'height' : '80%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }          
     });
}

function validar_envio_msj()
{
    resultado = true;    
    $('#asunto').attr('style', '');
    $('#mensaje').attr('style', '');
    
    
    if ($.trim($('#asunto').val()) == "")
    {
        $('#asunto').attr('style', 'border: 1px solid red;');
        $('#asunto').attr('title', 'Debe ingresar el asunto del mensaje.');            
        $('#asunto').tooltip();
        resultado = false;
    }

    if ($.trim($('#mensaje').val()) == "")
    {
        $('#mensaje').attr('style', 'border: 1px solid red;');
        $('#mensaje').attr('title', 'El mensaje no puede estar vacío.');            
        $('#mensaje').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $('form#frm_msj').submit();
    }   
}

//Se abre la ventana para enviar mensaje a usuario
function solicitar_token(_base_url, id_usuario)
{
    url = _base_url + "sigpa/solicitar-token/" + id_usuario;
    
    $.fancybox({
        'href' : url,
         'width' : '65%',
         'height' : '80%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }          
     });
}

function validar_solicitud()
{
    resultado = true;    
    $('#razon').attr('style', '');
    

    if ($.trim($('#razon').val()) == "")
    {
        $('#razon').attr('style', 'border: 1px solid red;');
        $('#razon').attr('title', 'El mensaje no puede estar vacío.');            
        $('#razon').tooltip();
        resultado = false;
    }
    
    if (resultado)
    {
        $('form#frm_msj').submit();
    }   
}

//Se realiza la apertura de la ventana de postulacion a THV
function realizar_postulacion_thv(url_thv, tipo_registro, id_registro)
{
    url_thv = url_thv + id_registro;
    if (tipo_registro == 'cultor_individual')
    {
        url_thv = url_thv + "/CI";            
    }
    else if (tipo_registro == 'cultor_colectivo'){
        url_thv = url_thv + "/CC";            
    }
            
    $.fancybox({
        'href' : url_thv,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });  
}

//Se realiza la apertura de la ventana de postulacion a THV
function realizar_postulacion_thv_directa(url_thv, tipo_registro, id_registro)
{
    window.location = url_thv + id_registro + "/" + tipo_registro;   
}



