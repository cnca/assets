(function($) {
	$.fn.formToWizard = function(options) {
		options = $.extend({  
			submitButton: "",
			operacion: "add"
		}, options); 
		
		var element = this;

		var steps = $(element).find("fieldset");
		var count = steps.size();
		var submmitButtonName = "#" + options.submitButton;
		$(submmitButtonName).hide();

		// 2
		$(element).before("<ul id='steps'></ul>");

		steps.each(function(i) {
			$(this).wrap("<div id='step" + i + "'></div>");
			$(this).append("<div class='popup-footer'><div class='inner'><div class='ftr' id='step" + i + "commands'></div><small class='stepCount'>Paso " + (i + 1) + " de 6 </small></div></div>");    
			$('#step5commands').append("<input id='saveForm' type='button' onclick='validar_formulario(" + (i  + 1) + ", \"" + options.operacion + "\")' value='Guardar Ficha' class='btn btn-primary btn-big btn-save' style='display: inline-block;'>");

			// 2
			var name = $(this).find("legend").html();
			$("#steps").append("<li class='stepDesc' id='stepDesc" + i + "'><span class='stpName'>" + name + "</span> <span class='stpNumber'>" + (i + 1) + "</span></li>");

			if (i == 0) {
				createNextButton(i);
				selectStep(i);

			}
			else if (i == count - 1) {
				$("#step" + i).hide();
				createPrevButton(i);
			}
			else {
				$("#step" + i).hide();
				createPrevButton(i);
				createNextButton(i);
			}
		});

		function createPrevButton(i) {
			var stepName = "step" + i;
			$("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev btn btn-big'>VOLVER</a>");

			$("#" + stepName + "Prev").bind("click", function(e) {
				$("#" + stepName).hide();
				$("#step" + (i - 1)).show();
				$(submmitButtonName).hide();
				selectStep(i - 1);

				// console.log("-> +1");
			});
		}

		function createNextButton(i) {
			var stepName = "step" + i;
			
			$("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='next btn btn-big'>SIGUIENTE</a>");

			$("#" + stepName + "Next").bind("click", function(e) {				
				if (validar_formulario(i + 1)) {									
					$("#" + stepName).hide();
					
					$("#step" + (i + 1)).show();
					
					if (i + 2 == count)
						$(submmitButtonName).show();
					selectStep(i + 1);
				}
			});
		}

		function selectStep(i) {
			$("#steps li").removeClass("current");
			$("#stepDesc" + i).addClass("current");
			$("#Form div").removeClass("current");
			$("#step" + i).addClass("current");
		}

	}
})(jQuery); 