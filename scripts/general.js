//Se realiza la regarga del home desde
function reload_home() {
    parent.window.location.reload();
}
//Se abre la ventana para revisar los datos de la gestion cnca
function ver_detalle(id_comuna)
{
  //incluyo la url donde esta el codigo que permite la visualizacion de la gestion del CNCA 
  url = '/sigpa/detalle_gestion_cnca/' + id_comuna;

  $.fancybox({
      'href' : url,
       'width' : '85%',
       'height' : '100%',
       'fitToView' : false,
       'autoSize' : false,
       'autoScale' : false,
       'transitionIn' : 'none',
       'transitionOut' : 'none',
       'type' : 'iframe',
      helpers : { 
        overlay : {closeClick: false}
      }          
   });
}



function ImagenOk(img) {
    if (!img.complete) return false;
    if (typeof img.naturalWidth != "undefined" && img.naturalWidth == 0) return false;
    return true;
}

function ReemplazarImagenesRotas() {
    
  for (var i = 0; i < document.images.length; i++)
  {
  	new_img = "";
  	if (document.images[i].name.indexOf('img_ci_') >= 0)
  	{
  	    new_img = "media/img/no-imagen-cultor-individual.jpg";
  	}
  	else if (document.images[i].name.indexOf('img_cc_') >= 0)
  	{
  	    new_img = "media/img/no-imagen-cultor-colectivo.jpg";
  	}
  	else if (document.images[i].name.indexOf('img_fp_') >= 0)
  	{
  	    new_img = "media/img/no-imagen-fiesta-popular.jpg";
  	}
  	else if (document.images[i].name.indexOf('img_g_') >= 0)
  	{
  	    new_img = "media/img/no-imagen-gastronomia.jpg";
  	}
  	else if (document.images[i].name.indexOf('img_usr_') >= 0)
  	{
  	    new_img = "media/img/avatar-registro.jpg";
  	}
  	
  	
  	if (new_img != "" && !ImagenOk(document.images[i]))
  	{	    
  	    document.images[i].src = new_img;
  	}
  }    
}

// valida entero
function validar_int(valor)
{
  if ($.isNumeric(valor) == false) // si no es valor numerico
    return false;
  else if (parseInt(valor) <= 0) // si es menor o igual a 0
    return false;
  else if (/^[0-9]+$/.test(valor) == false) // si el valor no tiene solo digitos
    return false;
  else if (valor.charAt(0) == '0') // si el primer valor del string es 0
    return false;
  else // si nada de lo anterior ocurre
    return true;
}

//valida rut
function validar_rut(texto)
{   
   var tmpstr = "";   
   for ( i=0; i < texto.length ; i++ )      
      if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
	 tmpstr = tmpstr + texto.charAt(i);   
   texto = tmpstr;   
   largo = texto.length;   

   if ( largo < 2 )   
   {      
      return false;   
   }   

   for (i=0; i < largo ; i++ )   
   {         
      if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
      {         
	 return false;      
      }   
   }   

   var invertido = "";   
   for ( i=(largo-1),j=0; i>=0; i--,j++ )      
      invertido = invertido + texto.charAt(i);   
   var dtexto = "";   
   dtexto = dtexto + invertido.charAt(0);   
   dtexto = dtexto + '-';   
   cnt = 0;   

   for ( i=1,j=2; i<largo; i++,j++ )   
   {      
      if ( cnt == 3 )      
      {         
	 dtexto = dtexto + '.';         
	 j++;         
	 dtexto = dtexto + invertido.charAt(i);         
	 cnt = 1;      
      }      
      else      
      {            
	 dtexto = dtexto + invertido.charAt(i);         
	 cnt++;      
      }   
   }   

   invertido = "";   
   for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )      
      invertido = invertido + dtexto.charAt(i);   

   if ( revisarDigito(texto) )      
      return true;   

   return false;
}

function revisarDigito(componente)
{   
   var crut =  componente
   largo = crut.length;   
   if ( largo < 2 )   
   {      
      return false;   
   }   
   if ( largo > 2 )      
      rut = crut.substring(0, largo - 1);   
   else      
   rut = crut.charAt(0);   
   dv = crut.charAt(largo-1);   
   
   if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')   
   {      
      return false;   
   }      

   if ( rut == null || dv == null )
      return 0   

   var dvr = '0'   
   suma = 0   
   mul  = 2   

   for (i= rut.length -1 ; i >= 0; i--)   
   {   
      suma = suma + rut.charAt(i) * mul      
      if (mul == 7)         
	 mul = 2      
      else             
	 mul++   
   }   
   res = suma % 11   
   if (res==1)      
      dvr = 'k'   
   else if (res==0)      
      dvr = '0'   
   else   
   {      
      dvi = 11-res      
      dvr = dvi + ""   
   }
   if ( dvr != dv)   
   {      
      return false   
   }

   return true
}
    
//Se realiza la validacion d email
function validar_email(valor)
{
    // creamos nuestra regla con expresiones regulares.
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    // utilizamos test para comprobar si el parametro valor cumple la regla
    if(filter.test(valor))
            return true;
    else
	return false;
}

//Se realiza la validacion de una url
function validar_url(url)
{
    url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if(!url_validate.test(url)){
    return false;
    }
    else{
       return true;
    }
}

//Se realiza la validacion de una url
function validar_url_simple(url){
    if(url.indexOf('http://') < 0)
    {	
	if (url.indexOf('www.') < 0)
	{
	    return false;
	}
	url = "http://" + url;	
    }
    
    temp = url.replace('http://www.', '');    
    if (temp.indexOf('.') < 0)
    {
	return false;
    }
    else
    {
	data = temp.split('.');
	if (data.length < 2) {
	    return false;
	}
	else
	{
	    if (data[1] == '')
	    {
		return false;
	    }
	}
    }
    
    url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if(!url_validate.test(url)){
	return false;
    }
    else{
       return true;
    }
}


//Se abre una pagina nueva
function ir_pagina(pagina)
{
    window.location = pagina;
}

//Se realiza el preview de una imagen para ser cargada
function preview_imagen(input, id_img)
{    
    if (input.files && input.files[0])
    {
	var reader = new FileReader();	
	reader.onload = function (e) {	    
	    $('#' + id_img).attr('src', e.target.result);
	}

	reader.readAsDataURL(input.files[0]);	
    }
}
    
//Se muestra un mensaje de error
function mostrar_mensaje_aviso(mensaje, tiempo, ancho, alto, redirect, refresh_parent)
{
    if (alto == ""){
	alto = '50%';
    } 

    if (ancho == ""){
	ancho = '50%';
    }
    url = '/sigpa/mensaje_aviso/' + mensaje;
        
    $.fancybox({
        'href' : url,
         'width' : ancho,
         'height' : alto,
	 'fitToView' : false,
         'autoSize' : false,
         'autoScale' : true,
	 //'scrolling': 'no',	 
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });
    
    //Verifico si debe cerrarse automaticamente
    if (tiempo > 0)
    {
	if (redirect != "")
	{
	    setTimeout('$.fancybox.close();', tiempo);
	    setTimeout("window.location = '" + redirect + "';", tiempo);	
	}
	else
	{
	    if (refresh_parent != "")
	    { 
		setTimeout('parent.window.location.reload();', tiempo);	    
	    }
	    else
	    {
		setTimeout('$.fancybox.close();', tiempo);
	    }
	}	
    }
}

//Se muestra un mensaje de error
function mostrar_mensaje_aviso_thv(mensaje, tipo_registro, id_ficha, ancho, alto)
{
    if (alto == ""){
	alto = '50%';
    } 

    if (ancho == ""){
	ancho = '50%';
    }
    url = '/sigpa/mensaje_aviso_thv/' + mensaje + '/' + tipo_registro + '/' + id_ficha;
        
    $.fancybox({
        'href' : url,
         'width' : ancho,
         'height' : alto,
	 'fitToView' : false,
         'autoSize' : false,
         'autoScale' : true,
	 //'scrolling': 'no',	 
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
	 afterClose : function() {
	    parent.reload_home();
	}
     });
}

//Se muestra un mensaje de error
function mostrar_mensaje_exito(mensaje, tiempo, ancho, alto, redirect, refresh_parent)
{
    if (alto == ""){
	alto = '50%';
    } 

    if (ancho == ""){
	ancho = '50%';
    }
    url = '/sigpa/mensaje_aviso/' + mensaje;
        
    $.fancybox({
        'href' : url,
         'width' : ancho,
         'height' : alto,
	 'fitToView' : false,
         'autoSize' : false,
         'autoScale' : true,
	 //'scrolling': 'no',	 
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
	 'closeBtn':false,
	 helpers : { 
          overlay : {closeClick: false}
        }
     });
    
    //Verifico si debe cerrarse automaticamente
    if (tiempo > 0)
    {
	if (redirect != "")
	{
	    if (refresh_parent == "")
	    {
		setTimeout('$.fancybox.close();', tiempo);
		setTimeout("window.location = '" + redirect + "';", tiempo);
	    }
	    else
	    {
		setTimeout('$.fancybox.close();', tiempo);
		setTimeout("parent.window.location = '" + redirect + "';", tiempo);
	    }
	}
	else
	{
	    if (refresh_parent != "")
	    { 
		setTimeout('parent.window.location.reload();', tiempo);	    
	    }
	    else
	    {
		setTimeout('$.fancybox.close();', tiempo);
	    }
	}	
    }
}

//Se muestra un mensaje de error
function mostrar_mensaje_error(mensaje, tiempo, ancho, alto)
{
    if (alto == ""){
	alto = '50%';
    } 

    if (ancho == ""){
	ancho = '50%';
    }
    url = '/sigpa/mensaje_error/' + mensaje;
    
    $.fancybox({
        'href' : url,
         'width' : ancho,
         'height' : alto,
	 'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });
    //Verifico si debe cerrarse automaticamente
    if (tiempo > 0)
    {
	setTimeout('$.fancybox.close();', tiempo);
    }    
}

//Se muestra la galkeria de imagenes de un registro
function galeria_imagenes(tipo_registro, id_registro, id_imagen)
{
    url = '/galeria/' + tipo_registro + '/' + id_registro + '/' + id_imagen;
    
    //alert("url: " + url);
    $.fancybox({
	'padding': 0,
        'href' : url,
         'width' : '100%',
         'height' : '100%',
	 'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe'
     });
}

//Se realiza la inicializacion de un mapa
function inicializar_mapa(address, id_mapa) {
  var myMarkerIsDraggable = true;
  var myCoordsLenght = 6;

  // I create a new google maps object to handle the request and we pass the address to it
  var geoCoder = new google.maps.Geocoder(address)
  // a new object for the request I called "request" , you can put there other parameters to specify a better search (check google api doc for details) ,
  // on this example im going to add just the address
  var request = {address:address};

  // I make the request
  geoCoder.geocode(request, function(result, status){
    // as a result i get two parameters , result and status.
    // results is an  array tha contenis objects with the results founds for the search made it.
    // to simplify the example i take only the first result "result[0]" but you can use more that one if you want

    // So , using the first result I need to create a  latlng object to be pass later to the map
    var latlng = new google.maps.LatLng(result[0].geometry.location.lat(), result[0].geometry.location.lng());
    document.getElementById('localizacion-lat').value = result[0].geometry.location.lat();
    document.getElementById('localizacion-lng').value = result[0].geometry.location.lng();

    // some initial values to the map
    var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    // the map is created with all the information
    var map = new google.maps.Map(document.getElementById(id_mapa),myOptions);

    // an extra step is need it to add the mark pointing to the place selected.
    var my_marker = new google.maps.Marker({position:latlng, map:map, title:'Mapa', draggable: myMarkerIsDraggable});

    // adds a listener to the marker
    // gets the coords when drag event ends
    // then updates the input with the new coords
    google.maps.event.addListener(my_marker, 'dragend', function(evt){
      document.getElementById('localizacion-lat').value = evt.latLng.lat().toFixed(myCoordsLenght);
      document.getElementById('localizacion-lng').value = evt.latLng.lng().toFixed(myCoordsLenght);
    });

    // centers the map on markers coords
    map.setCenter(my_marker.position);
    // adds the marker on the map
    my_marker.setMap(map);
  })
}

//Se realiza la inicializacion de un mapa
function mapa_inicio(id_mapa) {
  var myMarkerIsDraggable = true;
  var myCoordsLenght = 6;

  // So , using the first result I need to create a  latlng object to be pass later to the map
  var latlng = new google.maps.LatLng(-33.038796, -71.629055);
  document.getElementById('localizacion-lat').value = -33.038796;
  document.getElementById('localizacion-lng').value = -71.629055;

  // some initial values to the map
  var myOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  // the map is created with all the information
  var map = new google.maps.Map(document.getElementById(id_mapa),myOptions);

  // an extra step is need it to add the mark pointing to the place selected.
  var my_marker = new google.maps.Marker({position:latlng, map:map, title:'Mapa', draggable: myMarkerIsDraggable});

  // adds a listener to the marker
  // gets the coords when drag event ends
  // then updates the input with the new coords
  google.maps.event.addListener(my_marker, 'dragend', function(evt){	    
    document.getElementById('localizacion-lat').value = evt.latLng.lat().toFixed(myCoordsLenght);
    document.getElementById('localizacion-lng').value = evt.latLng.lng().toFixed(myCoordsLenght);
  });

  // centers the map on markers coords
  map.setCenter(my_marker.position);

  // adds the marker on the map
  my_marker.setMap(map);

  var geocoder = new google.maps.Geocoder();
  var infowindow = new google.maps.InfoWindow();

  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
        map.fitBounds(results[0].geometry.viewport);
        my_marker.setMap(map);
        my_marker.setPosition(latlng);
        // $("#localizacion-comuna").val(3);
        // $("#localizacion-comuna").multiselect("refresh");
        // $('#localizacion-ciudad').val('Valparaíso');
        // $('#localizacion-calle_avenida').val('Sotomayor');
        // $('#localizacion-numero').val('233');
      }
    }
  });
}

//Se realiza la busqueda en el Mapa
function buscar_mapa(id_mapa)
{
  var style_exito = 'width:421px;';
  var style_error = 'border: solid 1px #f00; width:421px;';

  $('#btn_localizacion_comuna').attr('style', style_exito);
  $('#localizacion-ciudad').removeClass('requires');
  $('#localizacion-calle_avenida').removeClass('requires');
  $('#localizacion-numero').removeClass('requires');

  comuna = $("#localizacion-comuna option:selected").text();
  ciudad = $.trim($('#localizacion-ciudad').val());
  calle = $.trim($('#localizacion-calle_avenida').val());
  numero = $.trim($('#localizacion-numero').val());

  html = "<button type='button' onclick=\"buscar_mapa('map_canvas');\">";
  html += "<p id='geo'>Localízame en el mapa</p>";
  html += "</button>";
  html += "<div id='mapholder'></div>";

  $('#' + id_mapa).html(html);
  if ((comuna != 'Comuna') && (ciudad != "") && (calle != "")) {
    $('#' + id_mapa).html(html);
    direccion = comuna + ', ' + ciudad + ', ' + calle + ' ' + numero;
    inicializar_mapa(direccion, id_mapa);
  }
  else {
    if (comuna == "Comuna") {
      $('#btn_localizacion_comuna').attr('style', style_error);
      $('#localizacion-comuna').attr('title', 'Debe seleccionar la Comuna');
      //$('#localizacion-comuna').tooltip();
    }

    if (ciudad == "") {
      $('#localizacion-ciudad').addClass('requires');
      $('#localizacion-ciudad').attr('title', 'Debe ingresar la Ciudad');
      //$('#localizacion-ciudad').tooltip();
    }

    if (calle == "") {
      $('#localizacion-calle_avenida').addClass('requires');
      $('#localizacion-calle_avenida').attr('title', 'Debe ingresar la Calle');
      //$('#localizacion-calle_avenida').tooltip();
    }
  }
}

//Se realiza la busqueda en el Mapa
function buscar_mapa_nuevo(id_mapa)
{
  comuna = $("#localizacion-comuna option:selected").text();
  ciudad = $.trim($('#localizacion-ciudad').val());
  calle = $.trim($('#localizacion-calle_avenida').val());
  numero = $.trim($('#localizacion-numero').val());

  direccion = 'Sotomayor 233, Valparaíso, Región de Valparaíso';

  if ((comuna != 'Selecciona...') && (ciudad != "") && (calle != "")) {
    direccion = comuna + ', ' + ciudad + ', ' + calle + ' ' + numero;
  }
  //$('#' + id_mapa).html(html);
  inicializar_mapa(direccion, id_mapa);
}

function errores(err) {
  /*Controlamos los posibles errores */
  if (err.code == 0) {
    alert("Oops! Algo ha salido mal");
  }
  if (err.code == 1) {
    alert("Oops! No has aceptado compartir tu posición");
  }
  if (err.code == 2) {
    alert("Oops! No se puede obtener la posición actual");
  }
  if (err.code == 3) {
    alert("Oops! Hemos superado el tiempo de espera");
  }
}
	
//Se dibuja el mapa
function dibujar_mapa(latitud, longitud) {
  var latlon = new google.maps.LatLng(latitud,longitud); /* Creamos un punto con nuestras coordenadas */
  var myOptions = {
    zoom: 17,
    center: latlon, /* Definimos la posicion del mapa con el punto */
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };/*Configuramos una serie de opciones como el zoom del mapa y el tipo.*/

  map = new google.maps.Map($("#map_canvas").get(0), myOptions); /*Creamos el mapa y lo situamos en su capa */
  var coorMarcador = new google.maps.LatLng(latitud,longitud); /*Un nuevo punto con nuestras coordenadas para el marcador (flecha) */
  
  var marcador = new google.maps.Marker({
    /*Creamos un marcador*/
    position: coorMarcador, /*Lo situamos en nuestro punto */
    map: map, /* Lo vinculamos a nuestro mapa */
    title: "Dónde estoy?" 
  });
}
    
//Se obtienen las coordenadas con el navegador
function coordenadas(position) {
  latitud = position.coords.latitude; /*Guardamos nuestra latitud*/
  longitud = position.coords.longitude; /*Guardamos nuestra longitud*/
  dibujar_mapa(latitud, longitud);
}

//Se realiza la localizacion IP del usuario
function localizacion_ip() {
  if (navigator.geolocation) { /* Si el navegador tiene geolocalizacion */
    navigator.geolocation.getCurrentPosition(coordenadas, errores);
  } else {
    alert('Oops! Tu navegador no soporta geolocalización. Bájate Chrome, que es gratis!');
  }
}

//Se realiza el proceso de eliminacion de un multimedia
function eliminar_multimedia(tipo_registro, id_registro)
{
    $.ajax({
	type: "POST",
	url: "/sigpa/eliminar-archivo/" + tipo_registro + "/" + id_registro,
	data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value},
	success : function(respuesta)
	{
	    if (respuesta == "OK")
	    {
		$("#img_" + id_registro).remove();
		$("#li_img_" + id_registro).remove();
	    }
	    else
	    {                    
		$("#li_img_" + id_registro).addClass("requires");		
	    }
	}
	,
	error: function (xhr, ajaxOptions, thrownError) {
		alert(xhr.status  + " " + xhr.responseText);
		alert(thrownError);
    
	}
    });    
}

//Se realiza la funcion de agregar a Favoritos
function agregar_favoritos()
{
    //e.preventDefault(); 
    var bookmarkUrl = window.location.href;
    var bookmarkTitle = window.document.title;

    if(window.sidebar){ 
	window.sidebar.addPanel(bookmarkTitle, bookmarkUrl,"");
    } else if(document.all){ 
	window.external.AddFavorite( bookmarkUrl, bookmarkTitle);
    } else if(window.opera && window.print){ 
	alert('Pulsa las teclas ctrl + D para añadir (Comando + D para Mac), después hacer clic en Aceptar'); 
    } else if(window.chrome){ 
	alert('Pulsa las teclas ctrl + D para añadir (Comando + D para Mac), después hacer clic en Aceptar');
    } else { 
	alert('Su navegador no soporta la opción de añadir a favoritos/marcadores');
    }
}
// Numeric only control handler
jQuery.fn.SoloNumeros =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

//Se realiza la operacion de compartir en twitter
function compartir_twitter()
{    
    _url = "http://twitter.com/home?status=";
    titulo = $("#titulo_registro").html();
    _href = window.location.href;
    
    _url = _url + titulo + "+" + _href;
    $("#lnk_twitter").attr("href", _url);
    $("#lnk_twitter").click();    
}

//Se realiza la operacion de compartir en facebook
function compartir_facebook()
{
    _url = "http://www.facebook.com/sharer.php?u=";
    titulo = $("#titulo_registro").html();
    _href = window.location.href;
    
    _url = _url + _href + "&t=" + titulo;
    $("#lnk_facebook").attr("href", _url);
    $("#lnk_facebook").click();
}


//Se muestra la ficha en pdf
function ficha_pdf(slug)
{
    location.href = '/ficha-pdf:' + slug + '.pdf';
}

function copiar_iframe()
{
  $('#iframe-input').select();
  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
  } catch (err) {
    console.log('Oops, unable to copy');
  }
}

function dibujar_graficos(js_data){
  google.load("visualization", "1", {packages:["corechart"]});
      
      function drawTipos() {
        var data = google.visualization.arrayToDataTable([
          ['Manifestación', 'Cantidad'],
          ['Cultores individuales', js_data.individuales],
          ['Cultores colectivos', js_data.colectivos],
          ['Fiestas populares',js_data.fiestas],
          ['Lugares gastronómicos',js_data.gastronomias],
        ]);

        var options = {
          is3D: true,
          chartArea: {width: '100%', height: '100%'},
          legend: {position: 'right'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_tipos'));
        chart.draw(data, options);
        
      }

      function drawA() {
        var data = google.visualization.arrayToDataTable([
          ['Composición', 'Cantidad'],
          ['Hombres', js_data.hombres],
          ['Mujeres', js_data.mujeres],
        ]);

        var options = {
          is3D: true,
          chartArea: {width: '100%', height: '100%'},
          legend: {position: 'right'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
        
      }
      function drawB() {
        var data = google.visualization.arrayToDataTable(js_data.individuales_reg);

        var options = {
          chartArea: {width: '100%', height: '100%'},
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      
      function drawC() {
        var data = google.visualization.arrayToDataTable([
          ['Género', 'Cantidad'],
          ['Compuestos sólo por hombres', js_data.chombres],
          ['Compuestos sólo por Mujeres', js_data.cmujeres],
          ['Compuestos por hombres y mujeres', js_data.cmixta]
        ]);

        var options = {
          //title: 'Cultores colectivos registrados en SIGPA a nivel nacional',
          is3D: true,
          chartArea: {width: '100%', height: '100%'},
          legend: {position: 'right'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3dc'));
        chart.draw(data, options);
      }
      
      function drawD() {
        var data = google.visualization.arrayToDataTable(js_data.colectivos_reg);

        var options = {
          chartArea: {width: '100%', height: '100%'},
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_divd'));
        chart.draw(data, options);
      }
      
      function drawAll() {
        drawTipos();
        drawA();
        drawB();
        drawC();
        drawD();
      }
      
      google.setOnLoadCallback(drawAll);
}

//Se realiza el copiado del link al portapapeles
function copyToClipboard(texto) {
  var $temp = $("<input>")
  $("body").append($temp);
  $temp.val(texto).select();
  document.execCommand("copy");
  $temp.remove();
}