//Se realiza la validacion del login
function validar_login(){
    resultado = true;

   if ($.trim($('#username').val()) == ""){
        $('#username').attr('style', 'border: 1px solid red;');
        $('#username').attr('title', 'Correo electrónico es obligatorio');            
        $('#username').tooltip();
        resultado = false;
    }

    if (resultado){        
        $.ajax({
            type: "POST",
            url: "/acreditacion/acreditacion/",
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    username: $('#username').val(),
                },
            success : function(respuesta){
                if (!isNaN(respuesta)){
                    window.location = '/acreditacion/certificado/'+respuesta;           
                }
                else{                  
                    div = '<p style="color: red;">El correo ingresado no se encuentra en nuestra base de datos</p>';
                    $('#errors').html(div);                  
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status  + " " + xhr.responseText);
                alert(thrownError);
                }
        });
    }    
}

function validacion(){
    resultado = true;
    
    if ($.trim($('#nro_cert').val()) == ""){
        $('#nro_cert').attr('style', 'border: 1px solid red;');
        $('#nro_cert').attr('title', 'El número de certificado es obligatorio');            
        $('#nro_cert').tooltip();
        resultado = false;
    }
    if ($.trim($('#cod_ver').val()) == ""){
        $('#cod_ver').attr('style', 'border: 1px solid red;');
        $('#cod_ver').attr('title', 'El código de verificación es obligatorio');            
        $('#cod_ver').tooltip();
        resultado = false;
    }

    nro_cert= $('#nro_cert').val();
    cod_ver= $('#cod_ver').val();

    if(resultado){
        $.ajax({
            type: "POST",
            url: "/acreditacion/validacion",
            data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    nro_cert: nro_cert,
                    cod_ver: cod_ver
            },
            success : function(respuesta){
                if (respuesta == 'True'){
                    window.location = '/acreditacion/confirmacion/valido/'+nro_cert; 
                }
                else{
                    window.location = '/acreditacion/confirmacion/invalido/'+nro_cert;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status  + " " + xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}

function logout(){
    location.href = '/acreditacion/logout';
}

function validar_registro(){
    nombres= $('#nombres').val();
    apellidos= $('#apellidos').val();
    email= $('#email').val();
    organizacion= $('#organizacion').val();
    telefono= $('#telefono').val();
    etnografico= $('#etnografico').is(':checked')
    sigpa= $('#sigpa').is(':checked');
    conferencias= $('#conferencias').is(':checked')
    simposios= $('#simposios').is(':checked')

    $.ajax({
        type: "POST",
        url: "/acreditacion/registro",
        data: { csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            nombres: nombres,
            apellidos: apellidos,
            email: email,
            organizacion: organizacion,
            telefono: telefono,
            etnografico: etnografico,
            sigpa: sigpa,
            conferencias: conferencias,
            simposios: simposios
        },
        success : function(respuesta){
            if (respuesta.includes('Error')){
                div = '<p style="color: red;">'+respuesta+'</p>';
                $('#errors').html(div); 
            }
            else{
                div = '<p>'+respuesta+'</p>';
                $('#errors').html(div);
                $('#nombres').val('');
                $('#apellidos').val('');     
                $('#organizacion').val('');
                $('#telefono').val('');
                $('#email').val('');
                $( "#etnografico" ).prop( "checked", false );
                $( "#sigpa" ).prop( "checked", false );
                $( "#conferencias" ).prop( "checked", false );
                $( "#simposios" ).prop( "checked", false );
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status  + " " + xhr.responseText);
            console.log(thrownError);
        }
    });
}

function exportar_cert(){

    html2canvas($("#certificado"), {
        onrendered: function(canvas) {
            var ctx = canvas.getContext('2d');
            ctx.mozImageSmoothingEnabled = false;
            ctx.imageSmoothingEnabled = false;
            var myImage = canvas.toDataURL("image/png");   
            $.ajax({
                type: "POST",
                url: "/acreditacion/exportar-cert",
                data: {
                    csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    image: myImage,
                    correo: $('#correo').val()
                },
                success : function(respuesta){
                    if (respuesta == 'Ok'){
                       console.log("ok");
                    }
                    else{
                        console.log("error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status  + " " + xhr.responseText);
                    console.log(thrownError);
                }
            });
        },
    });
     
}

function certificado_pdf(id_user){
    location.href = '/acreditacion/certificado-pdf/'+ id_user;
}
