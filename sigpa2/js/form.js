// Pasos del formulario
function ShowTerm() {
    $("#terminos").show();
    $("#formulario").hide();
    $(window).scrollTop(0);
};
function ShowForm() {
    $("#terminos").hide();
    $("#formulario").show();
    $(window).scrollTop(0);
};

// Campos que permiten solo numeros.
function OnlyNumbers(e) {
	// Allow: backspace, delete, tab, escape and enter
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
	// Allow: Ctrl+A, Command+A
	(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	// Allow: home, end, left, right, down, up
	(e.keyCode >= 35 && e.keyCode <= 40) ||
	// Allow: k and K
	(e.keyCode === 75)) {
	// let it happen, don't do anything
		return;
	}
	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

// Anade puntos y guion a los campos rut
function FormatoRut(input) {
	var rut = input.val().replace(/\./g, "").replace(/-/g, "");
    var rut_split = rut.split("");
    if (rut_split.length > 1)
    	rut_split.splice(-1, 0 , "-");
    if (rut_split.length > 5)
    	rut_split.splice(-5, 0 , ".");
    if (rut_split.length > 9)
    	rut_split.splice(-9, 0 , ".");
    input.val(rut_split.join(""));
};

// Agrega o elimina propiedad required al marcar casilla de personalidad juridica
// Muestra u oculta formulario de personalidad juridica
function RequiredProp() {
	if ($("#cultorcolectivo-personalidad_juridica").is(':checked')){
		$("#personalidadjuridica").show();
		$('#cultorcolectivo-rut').prop('required', true);
		$('#cultorcolectivo-fecha_constitucion').prop('required', true);
		$('#cultorcolectivo-representante_legal').prop('required', true);
	}
	else {
		$("#personalidadjuridica").hide();
		$('#cultorcolectivo-rut').removeAttr('required');
		$('#cultorcolectivo-fecha_constitucion').removeAttr('required');
		$('#cultorcolectivo-representante_legal').removeAttr('required');
	}
};

// Agregar miembro cultor colectivo
function AddMiembro() {
	nombre = $("#mc_nombre").val().replace(/'/g,"&#39;");
	telefono = $("#mc_telefono").val();
	rut = $("#mc_rut").val();

	$("#mc_nombre").removeClass('required');
	$("#mc_telefono").removeClass('required');
	$("#mc_rut").removeClass('required');

	if (nombre == '' || telefono == '' || telefono.length < 6 || telefono.length > 11 || rut == '' || rut.length < 11 || rut > 12) {
		if (nombre == '')
			$("#mc_nombre").addClass('required');
		if (telefono == '' || telefono.length < 6 || telefono.length > 11)
			$("#mc_telefono").addClass('required');
		if (rut == '' || rut.length < 11 || rut > 12)
			$("#mc_rut").addClass('required');
	}
	else {
		html = "<div class='disabled'><label class='f30'><input type='text' id='miembrocolectivo-mc_nombre' name='miembrocolectivo-mc_nombre[]' value='" + nombre +"' readonly /></label><label class='f30'><input type='text' id='miembrocolectivo-mc_telefono' name='miembrocolectivo-mc_telefono[]' value='" + telefono + "' readonly /></label><label class='f30'><input type='text' id='miembrocolectivo-mc_rut' name='miembrocolectivo-mc_rut[]' value='" + rut + "' readonly /></label><input type='hidden' id='miembrocolectivo-colectivo' name='miembrocolectivo-colectivo[]' value='' /><span class='icon'><img id='edit-miembro' style='display: none;' src='media/sigpa2/images/form-edit.png' /><img id='delete-miembro' src='media/sigpa2/images/form-delete.png' onclick='$(this).parent().parent().remove()' /></span></div>"
		$("#lista-miembros").append(html)
		$("#mc_nombre").val('')
		$("#mc_telefono").val('')
		$("#mc_rut").val('')
	}
};

// Validacion de formularios Fancybox
function ValidateFormCC() {
	// Informacion base
	var val_nombre = $('#cultorcolectivo-nombre')[0].checkValidity();
	var val_canth = $('#cultorcolectivo-cantidad_hombres')[0].checkValidity();
	var val_cantm = $('#cultorcolectivo-cantidad_mujeres')[0].checkValidity();
	var val_telefono = $('#cultorcolectivo-telefono')[0].checkValidity();
	var val_region = $('#cultorcolectivo-region')[0].checkValidity();
	var val_comuna = $('#cultorcolectivo-comuna')[0].checkValidity();
	var val_direccion = $('#cultorcolectivo-direccion')[0].checkValidity();
	// Informacion personalidad juridica
	var val_rut = $('#cultorcolectivo-rut')[0].checkValidity();
	var val_fecha = $('#cultorcolectivo-fecha_constitucion')[0].checkValidity();
	var val_representante = $('#cultorcolectivo-representante_legal')[0].checkValidity();
	// Informacion miembros colectivo
	var val_mc_nombre = $('#mc_nombre')[0].checkValidity();
	var val_mc_telefono = $('#mc_telefono')[0].checkValidity()
	var val_mc_rut = $('#mc_rut')[0].checkValidity()

	// Recoge info para enviar a parent
	var nombre = $('#cultorcolectivo-nombre').val().replace(/'/g,"&#39;");
	var cantidad_hombres = $('#cultorcolectivo-cantidad_hombres').val();
	var cantidad_mujeres = $('#cultorcolectivo-cantidad_mujeres').val();
	var telefono = $('#cultorcolectivo-telefono').val();
	var email = $('#cultorcolectivo-email').val();
	var region = $('#cultorcolectivo-region option:selected').text().replace(/'/g,"&#39;");
	var comuna = $('#cultorcolectivo-comuna option:selected').text().replace(/'/g,"&#39;");
	var localidad = $('#cultorcolectivo-localidad').val().replace(/'/g,"&#39;");
	var direccion = $('#cultorcolectivo-direccion').val().replace(/'/g,"&#39;");
	// Informacion de los miembros del colectivo
	var miembros = ""
	var miembros_aux = $('#lista-miembros').html();
	if (miembros_aux == "") {
		miembros = "<span class=no-info>No se registran miembros del colectivo</span>"
	}
	else {
		$('input[name="miembrocolectivo-colectivo[]"]').each(function() {
			$(this).val(nombre);
		});
		miembros = $('#lista-miembros').html();
	}

	if ($('#cultorcolectivo-personalidad_juridica').is(':checked')) {
		var personalidad_juridica = "Si";
		var rut = $('#cultorcolectivo-rut').val();
		var fecha_constitucion = $('#cultorcolectivo-fecha_constitucion').val();
		var tipo_persona = $('#cultorcolectivo-tipo_persona').val().replace(/'/g,"&#39;");
		var institucion_otorgante = $('#cultorcolectivo-institucion_otorgante').val().replace(/'/g,"&#39;");
		var representante_legal = $('#cultorcolectivo-representante_legal').val().replace(/'/g,"&#39;");
		html = "<div class='info'><div class='row'><span>Nombre:</span><span><input type='text' id='cultorcolectivo-nombre' name='cultorcolectivo-nombre[]' value='" + nombre + "' readonly /></span><span class='icon'><img id='edit-cc' style='display: none;' src='media/sigpa2/images/form-edit.png' /><img id='delete-archivo' src='media/sigpa2/images/form-delete.png' onclick='EliminarCC($(this));' /></span></div><div class='row'><span>Cantidad hombres:</span><span><input type='text' id='cultorcolectivo-cantidad_hombres' name='cultorcolectivo-cantidad_hombres[]' value='" + cantidad_hombres + "' readonly /></span></div><div class='row'><span>Cantidad mujeres:</span><span><input type='text' id='cultorcolectivo-cantidad_mujeres' name='cultorcolectivo-cantidad_mujeres[]' value='" + cantidad_mujeres + "' readonly /></span></div><div class='row'><span>Teléfono:</span><span><input type='text' id='cultorcolectivo-telefono' name='cultorcolectivo-telefono[]' value='" + telefono + "' readonly /></span></div><div class='row'><span>Correo electrónico:</span><span><input type='text' id='cultorcolectivo-email' name='cultorcolectivo-email[]' value='" + email + "' readonly /></span></div><div class='row'><span>Región:</span><span><input type='text' id='cultorcolectivo-region' name='cultorcolectivo-region[]' value='" + region + "' readonly /></span></div><div class='row'><span>Comuna:</span><span><input type='text' id='cultorcolectivo-comuna' name='cultorcolectivo-comuna[]' value='" + comuna + "' readonly /></span></div><div class='row'><span>Localidad:</span><span><input type='text' id='cultorcolectivo-localidad' name='cultorcolectivo-localidad[]' value='" + localidad + "' readonly /></span></div><div class='row'><span>Dirección:</span><span><input type='text' id='cultorcolectivo-direccion' name='cultorcolectivo-direccion[]' value='" + direccion + "' readonly /></span></div><div class='row'><span>Personalidad jurídica:</span><span><input type='text' id='cultorcolectivo-personalidad_juridica' name='cultorcolectivo-personalidad_juridica[]' value='" + personalidad_juridica + "' readonly /></span></div><div class='row'><span>Rut:</span><span><input type='text' id='cultorcolectivo-rut' name='cultorcolectivo-rut[]' value='" + rut + "' readonly /></span></div><div class='row'><span>Fecha constitución:</span><span><input type='text' id='cultorcolectivo-fecha_constitucion' name='cultorcolectivo-fecha_constitucion[]' value='" + fecha_constitucion + "' readonly /></span></div><div class='row'><span>Tipo persona:</span><span><input type='text' id='cultorcolectivo-tipo_persona' name='cultorcolectivo-tipo_persona[]' value='" + tipo_persona + "' readonly /></span></div><div class='row'><span>Institución otorgante:</span><span><input type='text' id='cultorcolectivo-institucion_otorgante' name='cultorcolectivo-institucion_otorgante[]' value='" + institucion_otorgante + "' readonly /></span></div><div class='row'><span>Representante legal:</span><span><input type='text' id='cultorcolectivo-representante_legal' name='cultorcolectivo-representante_legal[]' value='" + representante_legal + "' readonly /></span></div><div><span>Miembros del colectivo:</span>" + miembros + "</div><input type='hidden' name='cultorcolectivo-miembros[]' value='" + miembros + "'></div>"
	}

	else {
		var personalidad_juridica = "No";
		html = "<div class='info'><div class='row'><span>Nombre:</span><span><input type='text' id='cultorcolectivo-nombre' name='cultorcolectivo-nombre[]' value='" + nombre + "' readonly /></span><span class='icon'><img id='edit-cc' style='display: none;' src='media/sigpa2/images/form-edit.png' /><img id='delete-archivo' src='media/sigpa2/images/form-delete.png' onclick='EliminarCC($(this));' /></span></div><div class='row'><span>Cantidad hombres:</span><span><input type='text' id='cultorcolectivo-cantidad_hombres' name='cultorcolectivo-cantidad_hombres[]' value='" + cantidad_hombres + "' readonly /></span></div><div class='row'><span>Cantidad mujeres:</span><span><input type='text' id='cultorcolectivo-cantidad_mujeres' name='cultorcolectivo-cantidad_mujeres[]' value='" + cantidad_mujeres + "' readonly /></span></div><div class='row'><span>Teléfono:</span><span><input type='text' id='cultorcolectivo-telefono' name='cultorcolectivo-telefono[]' value='" + telefono + "' readonly /></span></div><div class='row'><span>Correo electrónico:</span><span><input type='text' id='cultorcolectivo-email' name='cultorcolectivo-email[]' value='" + email + "' readonly /></span></div><div class='row'><span>Región:</span><span><input type='text' id='cultorcolectivo-region' name='cultorcolectivo-region[]' value='" + region + "' readonly /></span></div><div class='row'><span>Comuna:</span><span><input type='text' id='cultorcolectivo-comuna' name='cultorcolectivo-comuna[]' value='" + comuna + "' readonly /></span></div><div class='row'><span>Localidad:</span><span><input type='text' id='cultorcolectivo-localidad' name='cultorcolectivo-localidad[]' value='" + localidad + "' readonly /></span></div><div class='row'><span>Dirección:</span><span><input type='text' id='cultorcolectivo-direccion' name='cultorcolectivo-direccion[]' value='" + direccion + "' readonly /></span></div><div class='row'><span>Personalidad jurídica:</span><span><input type='text' id='cultorcolectivo-personalidad_juridica' name='cultorcolectivo-personalidad_juridica[]' value='" + personalidad_juridica + "' readonly /></span></div><input type='hidden' id='cultorcolectivo-rut' name='cultorcolectivo-rut[]' value='' /><input type='hidden' id='cultorcolectivo-fecha_constitucion' name='cultorcolectivo-fecha_constitucion[]' value='' /><input type='hidden' id='cultorcolectivo-tipo_persona' name='cultorcolectivo-tipo_persona[]' value='' /><input type='hidden' id='cultorcolectivo-institucion_otorgante' name='cultorcolectivo-institucion_otorgante[]' value='' /><input type='hidden' id='cultorcolectivo-representante_legal' name='cultorcolectivo-representante_legal[]' value='' /><div><span>Miembros del colectivo:</span>" + miembros + "</div><input type='hidden' name='cultorcolectivo-miembros[]' value='" + miembros + "'></div>"
	}

	if (val_nombre && val_canth && val_cantm && val_telefono && val_region && val_comuna && val_direccion) {
		if ($("#cultorcolectivo-personalidad_juridica").is(':checked')) {
			if (val_rut && val_fecha && val_representante) {
				parent.AddCC(html);
				parent.$.fancybox.close();
			}
		}
		else {
			parent.AddCC(html);
			parent.$.fancybox.close();
		}
	}
};

function ValidateFormCI() {
	var val_nombres = $('#cultorindividual-nombres')[0].checkValidity();
	var val_apellido_paterno = $('#cultorindividual-apellido_paterno')[0].checkValidity();
	var val_rut = $('#cultorindividual-rut')[0].checkValidity();
	var val_telefono = $('#cultorindividual-telefono')[0].checkValidity();
	var val_region = $('#cultorindividual-region')[0].checkValidity();
	var val_comuna = $('#cultorindividual-comuna')[0].checkValidity();
	var val_direccion = $('#cultorindividual-direccion')[0].checkValidity();

	if (val_nombres && val_apellido_paterno && val_rut && val_telefono && val_region && val_comuna && val_direccion) {

		var nombres = $("#cultorindividual-nombres").val().replace(/'/g,"&#39;");
		var apellido_paterno = $("#cultorindividual-apellido_paterno").val().replace(/'/g,"&#39;");
		var apellido_materno = $("#cultorindividual-apellido_materno").val().replace(/'/g,"&#39;");
		var rut = $("#cultorindividual-rut").val();
		var telefono = $("#cultorindividual-telefono").val();
		var email = $("#cultorindividual-email").val();
		var region = $("#cultorindividual-region option:selected").text().replace(/'/g,"&#39;");
		var comuna = $("#cultorindividual-comuna option:selected").text().replace(/'/g,"&#39;");
		var localidad = $("#cultorindividual-localidad").val().replace(/'/g,"&#39;");
		var direccion = $("#cultorindividual-direccion").val().replace(/'/g,"&#39;");

		html = "<div class='info'><div class='row'><span>Nombres:</span><span><input type='text' id='cultorindividual-nombres' name='cultorindividual-nombres[]' value='" + nombres + "' readonly /></span><span class='icon'><img id='edit-ci' style='display: none;' src='media/sigpa2/images/form-edit.png' /><img id='delete-archivo' src='media/sigpa2/images/form-delete.png' onclick='EliminarCI($(this));' /></span></div><div class='row'><span>Apellido paterno:</span><span><input type='text' id='cultorindividual-apellido_paterno' name='cultorindividual-apellido_paterno[]' value='" + apellido_paterno + "' readonly /></span></div><div class='row'><span>Apellido materno:</span><span><input type='text' id='cultorindividual-apellido_materno' name='cultorindividual-apellido_materno[]' value='" + apellido_materno + "' readonly /></span></div><div class='row'><span>Rut:</span><span><input type='text' id='cultorindividual-rut' name='cultorindividual-rut[]' value='" + rut + "' readonly /></span></div><div class='row'><span>Teléfono:</span><span><input type='text' id='cultorindividual-telefono' name='cultorindividual-telefono[]' value='" + telefono + "' readonly /></span></div><div class='row'><span>Correo electrónico:</span><span><input type='text' id='cultorindividual-email' name='cultorindividual-email[]' value='" + email + "' readonly /></span></div><div class='row'><span>Región:</span><span><input type='text' id='cultorindividual-region' name='cultorindividual-region[]' value='" + region + "' readonly /></span></div><div class='row'><span>Comuna:</span><span><input type='text' id='cultorindividual-comuna' name='cultorindividual-comuna[]' value='" + comuna + "' readonly /></span></div><div class='row'><span>Localidad:</span><span><input type='text' id='cultorindividual-localidad' name='cultorindividual-localidad[]' value='" + localidad + "' readonly /></span></div><div class='row'><span>Dirección:</span><span><input type='text' id='cultorindividual-direccion' name='cultorindividual-direccion[]' value='" + direccion + "' readonly /></span></div></div>"
		parent.AddCI(html);
		parent.$.fancybox.close();
	}
};

function ValidateFormEnlace() {
	var val_titulo = $('#link-titulo')[0].checkValidity();
	var val_tipo = $('#link-tipo')[0].checkValidity();
	var val_url = $('#link-url')[0].checkValidity();

	if (val_titulo && val_tipo && val_url) {
		// Informacion ingresada en los campos
		var titulo = $("#link-titulo").val().replace(/'/g,"&#39;");
		var tipo = $("#link-tipo").val();
		var descripcion = $("textarea#link-descripcion").val().replace(/'/g,"&#39;");
		var url = $("#link-url").val().replace(/'/g,"&#39;");

		html = "<div class='info'><div class='row'><span>Título:</span><span><input type='text' id='enlace-titulo' name='enlace-titulo[]' value='" + titulo + "' readonly /></span><span class='icon'><img id='edit-enlace' style='display: none;' src='media/sigpa2/images/form-edit.png' /><img id='delete-enlace' src='media/sigpa2/images/form-delete.png' onclick='EliminarEnlace($(this));' /></span></div><div class='row'><span>Tipo:</span><span><input type='text' id='enlace-tipo' name='enlace-tipo[]' value='" + tipo + "' readonly /></span></div><div class='row'><span>Enlace:</span><span><input type='text' id='enlace-url' name='enlace-url[]' value='" + url + "' readonly /></span></div><div class='row'><span>Descripción:</span><span><textarea id='enlace-descripcion' name='enlace-descripcion[]' readonly>" + descripcion + "</textarea></span></div></div>"
		parent.AddEnlace(html);
		parent.$.fancybox.close();
	}
};

function ValidateFormArchivo(html) {
	var html2 = $("<div />").html(html).text();
	parent.AddArchivo(html2);
	parent.$.fancybox.close();
}

function ValidateFormImagen(html) {
	var html2 = $("<div />").html(html).text();
	parent.AddImagen(html2);
	parent.$.fancybox.close();
}

function ValidateFormContacto(html) {
	var html2 = $("<div />").html(html).text();
	parent.AddContacto(html2);
	parent.$.fancybox.close();
}

function ValidateFormIntegrante(html) {
	var html2 = $("<div />").html(html).text();
	parent.AddIntegrante(html2);
	parent.$.fancybox.close();
}

function MaxSize(file) {
	var fileSize = file.get(0).files[0].size;
		if (fileSize > 2097152){
			alert("El archivo supera el tamaño permitido de 2 MB.");
			file.val('');
		}
}


function CountWords(textarea, max_length) {
	var text = textarea.val()
	var words = text.match(/\S+/g);
	if (text == '') {
		return 0;
	}
	else {
		if (words.length > max_length) {
			var str = text.match(/\S+/g).slice(0,max_length).join(' ');
			textarea.val(str);
			words = str.match(/\S+/g);
		}
		return words.length;
	}
}

//Se valida la cantidad de caracteres de un textarea
function CountCharacter(textarea, limite) {
	var text = textarea.val();
	if (text.length > limite) {
		var str = text.substring(0, limite);
		textarea.val(str);
	}

	return text.length;

}

function SaveForm() {
	$.ajax({ 
		url: '/solicitud-ciudadana/guardar/', 
		type: 'POST', 
		data: $('#formulario').find('select, input, textarea').serialize(),
		success: function (data) {
			alert("¡Solicitud guardada con éxito!");
			document.location.href="/mi-perfil/";
		},
		error: function(xhr, status, error) {
			alert("Problemas al guardar la solicitud. PÃ³ngase en contacto con el administrador del sitio.");
		}
	});
	return false;
}

function SaveFormCultor(tipo) {
	datos = $('#frm_registro').find('select, input, textarea').serializeArray();
	console.log($("#localizacion-region").val());
	datos.push({name: "localizacion-region", value: $("#localizacion-region").val()});
	datos.push({name: "localizacion-comuna", value: $("#localizacion-comuna").val()});
	
	$.ajax({ 
		url: '/solicitud-ciudadana/formulario-cultor-guardar/' + tipo, 
		type: 'POST', 
		data: datos,
		success: function (data) {
			alert("¡Solicitud guardada con éxito!");
			//document.location.href="/mi-perfil/";
		},
		error: function(xhr, status, error) {
			alert("Problemas al guardar la solicitud. Póngase en contacto con el administrador del sitio.");
		}
	});
	return false;
}

function EliminarSolicitud(solicitud_id) {
	if (confirm('¿Seguro que desea eliminar la solicitud?')) {
		$.ajax({ 
			url: '/solicitud-ciudadana/eliminar/', 
			type: 'POST', 
			data: {'solicitud_id': solicitud_id},
			success: function (data) {
				alert(data);
				location.reload();
			},
			error: function(xhr, status, error) {
				return false;
			}
		});
	} else {
		return false;
	}
}

function EliminarSolicitudRegistro(registro_id) {
	if (confirm('¿Seguro que desea eliminar este registro ?')) {
		$.ajax({ 
			url: '/solicitud-ciudadana/formulario-cultor/eliminar/', 
			type: 'POST', 
			data: {'registro_id': registro_id},
			success: function (data) {
				location.reload();
			},
			error: function(xhr, status, error) {
				return false;
			}
		});
	} else {
		return false;
	}
}

function EliminarCC(html) {
	if (confirm('¿Seguro que desea eliminar?')) {
		html.parent().parent().parent().remove();
	} else {
		return false;
	}
}

function EliminarCI(html) {
	if (confirm('¿Seguro que desea eliminar?')) {
		html.parent().parent().parent().remove();
	} else {
		return false;
	}
}

function EliminarArchivo(html) {
	if (confirm('¿Seguro que desea eliminar?')) {
		html.parent().parent().parent().remove();
	} else {
		return false;
	}
}

function EliminarEnlace(html) {
	if (confirm('¿Seguro que desea eliminar?')) {
		html.parent().parent().parent().remove();
	} else {
		return false;
	}
}

//Se realiza la impresion de la pagina, pero primero se ocultar los elementos innecesarios
function imprimir_pagina()
{
	$('.header,.menu,.footer,.boton').hide();
	window.print();
	$('.header,.menu,.footer,.boton').show();
}

//Se valida la publicacion de una solicitud
function publicar_solicitud(tipo, solicitud_id) {
	if (confirm('¿Seguro que desea publicar esta solicitud?')) {
		$.ajax({ 
			url: '/solicitud-ciudadana/publicar/' + tipo + "/" + solicitud_id, 
			type: 'POST', 
			data: {},
			success: function (data) {
				window.location = '/mi-perfil';
			},
			error: function(xhr, status, error) {
				return false;
			}
		});
	} else {
		return false;
	}
}

//Se realiza el registro de un comentario
function agregar_comentario(tipo_registro, id_registro, campo)
{
	$.fancybox({
        'href' : '/solicitud-ciudadana/comentarios/' + tipo_registro + "/" + id_registro + "/" + campo,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }        
     });
}

//Se realiza el registro de un comentario
function ver_cambios(tipo_registro, id_registro, id_cambio)
{
	$.fancybox({
        'href' : '/solicitud-ciudadana/cambios-registro/' + tipo_registro + "/" + id_registro + "/" + id_cambio,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }        
     });
}