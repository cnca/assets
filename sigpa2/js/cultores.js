//Se abre la ventana para agregar un cultor individual
function agregar_cultor(id_elemento, tipo_registro)
{
	url = '';
    if (tipo_registro == "cultor_individual")
    {
        url = '/sigpa/mi-cuenta/cultor-individual/agregar/';
    }
    else if (tipo_registro == "cultor_colectivo")
    {
        url = '/sigpa/mi-cuenta/cultor-colectivo/agregar/';
    }
    else if (tipo_registro == "fiestas")
    {
        url = '/sigpa/mi-cuenta/fiestas/agregar/';
    }
    else if (tipo_registro == "gastronomia")
    {
        url = '/sigpa/mi-cuenta/gastronomia/agregar/';
    }
    
    $.fancybox({
        'href' : url,
         'width' : '85%',
         'height' : '100%',
         'fitToView' : false,
         'autoSize' : false,
         'autoScale' : false,
         'transitionIn' : 'none',
         'transitionOut' : 'none',
         'type' : 'iframe',
        helpers : { 
          overlay : {closeClick: false}
        }        
     });
}