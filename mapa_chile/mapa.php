<?php
if(isset($_REQUEST["op"]) && ($_REQUEST["op"] == "cargar"))
{
    
}
if ($gestor = opendir('/home/codigo_abierto/codigoabierto.cl/mapa_chile/'))
{ 
    /* Esta es la forma correcta de iterar sobre el directorio. */
    $imagenes = array();
    while (false !== ($entrada = readdir($gestor))) {        
        if(strpos($entrada, ".jpg") > 0)
        {
            $imagenes[] = array($entrada, str_replace(".jpg", "", $entrada));
        }
    }
  
    closedir($gestor);
    
}
?>
<html>
    <head>
        <script src="jquery-1.10.2.min.js"></script>
        <script>
            function obtener_posicion(id_imagen)
            {
                var pos = $("#" + id_imagen).position();
                coordenada = pos.top + ', ' + pos.left;
                $("#txt_" + id_imagen).val(coordenada);
                
            }
            
            $(document).ready(function(){
                $('.img_mapa').click(function(e) {
                    /*var offset_t = $(this).offset().top - $(window).scrollTop();
                    var offset_l = $(this).offset().left - $(window).scrollLeft();
                   
                    var posX = Math.round( (e.clientX - offset_l) );
                    var posY = Math.round( (e.clientY - offset_t) );
                    */
                    
                    var offset = $(this).offset();
                    id = $(this).attr("id");
                    posX = e.pageX - offset.left;
                    posY = e.pageY - offset.top;
                    coordenadas = posX + ", " + posY;
                    $('#txt_' + id).val(coordenadas);
                     });                
                });
        </script>
    </head>
    <body>
        <form name="frm_mapa" id="frm_mapa" method="post" action="?op=cargar" >
            <table>
                <?php
                $i=0;
                for($i=0;$i < count($imagenes);$i++){
                ?>
                <tr>
                    <td>
                        <img src="<?= $imagenes[$i][0];?>" class="img_mapa" id="<?= $imagenes[$i][1];?>" alt="" border="0" onclick="obtener_posicion('<?= $imagenes[$i][1];?>')" />
                    </td>
                    <td>
                        <input type="text" name="txt_<?= $imagenes[$i][1];?>" id="txt_<?= $imagenes[$i][1];?>" value="" />
                    </td>
                </tr>
                <?php
                }
                ?>
                <tr>
                    <td>                        
                    </td>
                    <td>
                        <input type="submit" name="cmd_guardar" value="Guardar" />
                    </td>
                </tr>                
            </table>
        </form>
    </body>
</html>